﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManagementSystem.Models
{
    public class DashboardSearchActionModel
    {
        public Guid camp_guid { get; set; }
        public String startDate_str { get; set;}
        public String endDate_str { get; set; }

    }

    public class DashboardActionModel
    {
        public DashboardSearchActionModel dtoCriteria { get; set; }
        public List<SelectListItem> lstSel_Campaign { get; set; }
        public ActivityModel dtoUserActivity { get; set; }
        public List<DateActivityModel> lstDateAct { get; set; }
        public List<TotalByStoreAndCate> lstByStore { get; set; }
        public List<TotalByStoreAndCate> lstByStoreCate { get; set; }
    }

    public class ActivityModel
    {
        public int totalCust { get; set; }
        public int totalRec { get; set; }
        public double avgBill { get; set; }
        public double totalGainPoint { get; set; }
        
        public int totalPlayGameByRec { get; set; }
        
        public double perPlayGameByRec { get; set; }
        public String perPlayGameByRec_str { get; set; }

        public int totalStat_appv { get; set; }
        public int totalStat_rejt { get; set; }
        public int totalStat_wait { get; set; }
        
        public int totalWin { get; set; }
        public int totalPremium { get; set; }
        public decimal percTotalPrem { get; set; }
    }

    public class DateActivityModel
    {
        public DateTime actionDate_dt { get; set; }
        public String actionDate_str { get; set; }
        public int totalCust { get; set; }
        public int totalRec { get; set; }
        public double totalGainPoint { get; set; }
        public int totalWin { get; set; }
    }

    public class TotalByStoreAndCate
    {
        public String storeCateName { get; set; }
        public String storeName { get; set; }
        public Guid cust_guid { get; set; }
        public int totalRec { get; set; }
        public int totalCust { get; set; }
    }
}