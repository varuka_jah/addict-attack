﻿using ManagementSystem.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManagementSystem.Models
{
    public class CustomerSearchModel : DataTableAjaxPostModel
    {
        public bool continueProcess { get; set; }
        public Guid cust_guid { get; set; }
        public String cust_fullname { get; set; }
        public String cust_firstname { get; set; }
        public String cust_lastname { get; set; }
        public String cust_email { get; set; }
        public String cust_tel { get; set; }
        public bool status_act { get; set; }
        public bool status_ina { get; set; }
    }

    public class CustomerActionModel
    {
        public CustomerSearchModel dtoCriteria { get; set; }
        public CustomerModel dtoFocus { get; set; }
        public List<CustomerModel> lstResult { get; set; }

        public List<AddrProviceModel> lstProvince { get; set; }
        public List<SelectListItem> lstSel_Province { get; set; }

        public List<AddrDistrictModel> lstDistrict { get; set; }
        public List<SelectListItem> lstSel_District { get; set; }

        public List<AddrDistrictModel> lstSubDistrict { get; set; }
        public List<SelectListItem> lstSel_SubDistrict { get; set; }
    }

    public class CustomerModel
    {
        public Guid cust_guid { get; set; }
        public String cust_fullname { get; set; }
        public String cust_firstname { get; set; }
        public String cust_lastname { get; set; }
        public String cust_email { get; set; }
        public String cust_tel { get; set; }
        public String cust_password { get; set; }
        public String cust_addr1 { get; set; }
        public String cust_addr2 { get; set; }
        

        #region Province
        public long provinceId { get; set; }
        public String provinceCode { get; set; }
        public String provinceNameTh { get; set; }
        public String provinceNameEn { get; set; }
        #endregion

        #region District
        public long distId { get; set; }
        public String distCode { get; set; }
        public String distNameTh { get; set; }
        public String distNameEn { get; set; }
        #endregion

        #region Sub District
        public long subDistId { get; set; }
        public String subDistNameTh { get; set; }
        public String subDistNameEn { get; set; }
        public String subDistZipcode { get; set; }
        #endregion


        #region Common Property
        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }

        public String prop_remark { get; set; }

        public DateTime prop_create_date_dt { get; set; }
        public String prop_create_date_str { get; set; }

        public Guid prop_create_by { get; set; }
        public String prop_create_by_name { get; set; }

        public DateTime prop_update_date_dt { get; set; }
        public String prop_update_date_str { get; set; }

        public Guid prop_update_by { get; set; }
        public String prop_update_by_name { get; set; }

        public String prop_errorMsg { get; set; }
        #endregion
    }

    public class AddrProviceModel
    {
        public long provinceId { get; set; }
        public String provinceCode { get; set; }
        public String provinceNameTh { get; set; }
        public String provinceNameEn { get; set; }
    }
    public class AddrDistrictModel
    {
        #region Province
        public long provinceId { get; set; }
        public String provinceCode { get; set; }
        public String provinceNameTh { get; set; }
        public String provinceNameEn { get; set; }
        #endregion

        public long distId { get; set; }
        public String distCode { get; set; }
        public String distNameTh { get; set; }
        public String distNameEn { get; set; }
    }
    public class AddrSubDistrictModel
    {
        #region Province
        public long provinceId { get; set; }
        public String provinceCode { get; set; }
        public String provinceNameTh { get; set; }
        public String provinceNameEn { get; set; }
        #endregion

        #region District
        public long distId { get; set; }
        public String distCode { get; set; }
        public String distNameTh { get; set; }
        public String distNameEn { get; set; }
        #endregion

        public long subDistId { get; set; }
        public String subDistNameTh { get; set; }
        public String subDistNameEn { get; set; }
        public String subDistZipcode { get; set; }
    }
}