﻿using ManagementSystem.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagementSystem.Models
{
    public class UploadModel
    {
        public bool resultSuccess { get; set; }
        public bool resultError { get; set; }
        public String message { get; set; }
        public bool alreadyUpload { get; set; }
        public bool valid { get; set; }
        public bool notValid { get; set; }
        public int totalRecord { get; set; }
        public bool continueProcess { get; set; }

        public HttpPostedFileBase File { get; set; }

        public SMSUploadModel dtoData { get; set; }
        public List<SMSUploadModel> lstData { get; set; }

        public List<CouponUploadModel> lstCouponData { get; set; }
        public Boolean isValid { get; set; }
    }

    public class UploadPagingModel : DataTableAjaxPostModel
    {
        public bool resultSuccess { get; set; }
        public bool resultError { get; set; }
        public String message { get; set; }
        public bool alreadyUpload { get; set; }
        public bool valid { get; set; }
        public bool notValid { get; set; }
        public int totalRecord { get; set; }
        public bool continueProcess { get; set; }

        public HttpPostedFileBase File { get; set; }

        public SMSUploadModel dtoData { get; set; }
        public List<SMSUploadModel> lstData { get; set; }
        public List<CouponUploadModel> lstCouponData { get; set; }
        
        public Boolean isValid { get; set; }
    }

    public class SMSUploadModel
    {
        public string campName { get; set; }
        public string createTime { get; set; }
        public string updateTime { get; set; }
        public string shortCode { get; set; }
        public string campSenderName { get; set; }
        public string Ooperator { get; set; }
        public string msisdn { get; set; }
        public string message { get; set; }
        public string keyword { get; set; }
        public string context { get; set; }
        public string replyMessage { get; set; }
        public string messageStatus { get; set; }
        public string errorMsg { get; set; }
        public string campaignId { get; set; }
        public string company { get; set; }
        public string department { get; set; }
        public string brand { get; set; }

        public bool prop_flagstat { get; set; }
        public String prop_errorMsg { get; set; }
    }

    public class CouponUploadModel
    {
        public string campaignId { get; set; }
        public string createTime { get; set; }
        public string tel { get; set; }

        public string firstname { get; set; }
        public string lastname { get; set; }
        public string recNo { get; set; }


        public bool prop_flagstat { get; set; }
        public String prop_errorMsg { get; set; }
    }
}