﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagementSystem.Models
{
    public class MsgSearchModel
    {
        public Guid msg_guid { get; set; }
        public String msg_value { get; set; }
        public String msg_code { get; set; }
        public bool status_act { get; set; }
        public bool status_ina { get; set; }
    }

    public class MsgActionModel
    {
        public MsgSearchModel dtoCriteria { get; set; }
        public MsgModel dtoFocus { get; set; }
        public List<MsgModel> lstResult { get; set; }
    }

    public class MsgModel
    {
        public Guid msg_guid { get; set; }
        public String msg_value { get; set; }
        public String msg_code { get; set; }
      
        #region Common Property
        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }

        public String prop_remark { get; set; }

        public DateTime prop_create_date_dt { get; set; }
        public String prop_create_date_str { get; set; }

        public Guid prop_create_by { get; set; }
        public String prop_create_by_name { get; set; }

        public DateTime prop_update_date_dt { get; set; }
        public String prop_update_date_str { get; set; }

        public Guid prop_update_by { get; set; }
        public String prop_update_by_name { get; set; }

        public String prop_errorMsg { get; set; }
        #endregion
    }
}