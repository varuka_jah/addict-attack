﻿using ManagementSystem.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManagementSystem.Models
{
    public class ReportByCustomer
    {
        public CustomerModel dtoCustomer { get; set; }
        public CustomerReceiptModel dtoData { get; set; }
        public List<CustomerReceiptModel> lstData { get; set; }
        public List<SelectListItem> lstSel_RecStatus { get; set; }
        
    }

    public class CustomerReceiptModel
    {
        public Guid cust_guid { get; set; }
        public String cust_firstName { get; set; }
        public String cust_lastName { get; set; }
        public String cust_fullName { get; set; }
        public String cust_tel { get; set; }
        public String custRec_pointAnnounce { get; set; }
        public System.Guid custRec_guid { get; set; }
        public System.Guid custRec_campGuid { get; set; }
        public System.Guid custRec_storeGuid { get; set; }
        public System.Guid custRec_custGuid { get; set; }
        public string custRec_branchName { get; set; }
        public string custRec_recpImgUrl { get; set; }
        public string custRec_recpNo { get; set; }
        public double custRec_totalGainPoint { get; set; }
        public string custRec_gameResult { get; set; }
        public string custRec_gameResult_disp { get; set; }
        public string custRec_status { get; set; }
        public string custRec_remark { get; set; }
        public System.Guid custRec_createBy { get; set; }
        public System.DateTime custRec_createDate { get; set; }
        public string custRec_createDate_str { get; set; }
        public System.Guid custRec_updateBy { get; set; }
        public System.DateTime custRec_updateDate { get; set; }
        public String custRec_updateDate_str { get; set; }
        public String custRec_imgUrl { get; set; }
        public String custRec_fullPath { get; set; }

        public CustomerModel dtoCustomer { get; set; }


        #region Campaign
        public Guid camp_guid { get; set; }
        public String camp_code { get; set; }
        public String camp_name { get; set; }
        public String camp_desc { get; set; }
        public DateTime camp_startDate_dt { get; set; }
        public String camp_startDate_str { get; set; }
        public DateTime camp_endDate_dt { get; set; }
        public String camp_endDate_str { get; set; }
        #endregion

        #region Store
        public Guid store_guid { get; set; }
        public String store_name { get; set; }
        #endregion

        #region Premium
        public String custPrem_name { get; set; }
        public String custPrem_fullPath { get; set; }
        public String custPrem_textNameDisp { get; set; }

        public Guid custPrem_guid { get; set; }
        public Guid custPrem_premGuid { get; set; }
        #endregion

        #region Common Property
        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }

        public String prop_remark { get; set; }

        public DateTime prop_create_date_dt { get; set; }
        public String prop_create_date_str { get; set; }

        public Guid prop_create_by { get; set; }
        public String prop_create_by_name { get; set; }

        public DateTime prop_update_date_dt { get; set; }
        public String prop_update_date_str { get; set; }

        public Guid prop_update_by { get; set; }
        public String prop_update_by_name { get; set; }

        public String prop_errorMsg { get; set; }
        #endregion
    }

    public class ReportByCamp
    {
        public int totalJoin { get; set; }
        public CampReportSearchModel dtoCriteria_int { get; set; }
        public CampReportSearchModel dtoCriteria_resv { get; set; }
        public CampReportSearchModel dtoCriteria_act { get; set; }
        public CampReportSearchModel dtoCriteria_succ { get; set; }
        public CustomerReceiptModel dtoData { get; set; }
        public CampaignModel dtoCamp { get; set; }

        public List<CustomerReceiptModel> lstData_int { get; set; }
        public List<CustomerReceiptModel> lstData_resv { get; set; }
        public List<CustomerReceiptModel> lstData_act { get; set; }
        public List<CustomerReceiptModel> lstData_succ { get; set; }
        public List<SelectListItem> lstSel_RecStatus { get; set; }
    }
    public class CampReportSearchModel : DataTableAjaxPostModel
    {
        public bool continueProcess { get; set; }
        public Guid camp_guid { get; set; }
        public String custRec_status { get; set; }
        public String custRec_gameResult { get; set; }
        //public bool custRec_status_init { get; set; }
        //public bool custRec_status_resv { get; set; }
        //public bool custRec_status_act { get; set; }
        //public bool custRec_status_succ { get; set; }

    }

}