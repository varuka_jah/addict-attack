﻿
using ManagementSystem.Data;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;

namespace ManagementSystem.Models.Session
{
    public class UserDetailSession
    {
        public Guid erpGUID { get; set; }
        public M_User loginUserDetail { get; set; }
        public M_Role loginUserRole { get; set; }
        public Guid UserID { get; set; }
        public string Username { get; set; }
        //public EmpActionModel employee { get; set; }

        public UserDetailSession(M_User user, M_Role role)
        {
            this.loginUserDetail = user;
            this.loginUserRole = role;
            this.UserID = user.user_guid;
            this.Username = user.user_name;
        }

        /// <summary>
        /// Log off the current user.
        /// </summary>
        /// <param name="httpSession">The current HTTP session.</param>
        public static void LogOff(HttpSessionStateBase httpSession)
        {
            //
            // Write in the event log the message about the user's Log Off.
            // Note that could be situations that this code was invoked from "Error" page 
            // after the current user session has expired, or before the user to login!
            //
            UserDetailSession userInfoSession = (httpSession["UserInfoSession"] == null ? null : (UserDetailSession)httpSession["UserInfoSession"]);
           
            FormsAuthentication.SignOut();
            httpSession["userInfoSession"] = null;
        }

    }
}