﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManagementSystem.Models
{

    #region User Role
    public class RoleSearchModel
    {
        public Guid role_guid { get; set; }
        public String role_name { get; set; }
        public bool status_act { get; set; }
        public bool status_ina { get; set; }
    }

    public class RoleActionModel
    {
        public RoleSearchModel dtoCriteria { get; set; }
        public RoleModel dtoFocus { get; set; }
        public List<RoleModel> lstResult { get; set; }
    }

    public class RoleModel
    {
        public Guid role_guid { get; set; }
        public String role_isAdmin { get; set; }
        public bool role_isAdmin_flg { get; set; }
        public String role_name { get; set; }

        #region Common Property
        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }

        public String prop_remark { get; set; }

        public DateTime prop_create_date_dt { get; set; }
        public String prop_create_date_str { get; set; }

        public Guid prop_create_by { get; set; }
        public String prop_create_by_name { get; set; }

        public DateTime prop_update_date_dt { get; set; }
        public String prop_update_date_str { get; set; }

        public Guid prop_update_by { get; set; }
        public String prop_update_by_name { get; set; }

        public String prop_errorMsg { get; set; }
        #endregion
    }
    #endregion

    #region User

    public class UserSearchModel
    {
        public Guid role_guid { get; set; }
        public String role_name { get; set; }
        public Guid user_guid { get; set; }
        public String user_name { get; set; }
        public String user_username { get; set; }
        public String user_email { get; set; }

        public bool status_act { get; set; }
        public bool status_ina { get; set; }
    }

    public class UserActionModel
    {
        public UserSearchModel dtoCriteria { get; set; }
        public UserModel dtoFocus { get; set; }
        public List<UserModel> lstResult { get; set; }

        public List<SelectListItem> lstSelRole { get; set; }
    }

    public class UserModel
    {
        public Guid user_guid { get; set; }
        public String user_name { get; set; }
        public String user_username { get; set; }
        public String user_password { get; set; }
        public String user_email { get; set; }

        #region Role
        public Guid role_guid { get; set; }
        public String role_isAdmin { get; set; }
        public bool role_isAdmin_flg { get; set; }
        public String role_name { get; set; }
        #endregion

        #region Common Property
        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }

        public String prop_remark { get; set; }

        public DateTime prop_create_date_dt { get; set; }
        public String prop_create_date_str { get; set; }

        public Guid prop_create_by { get; set; }
        public String prop_create_by_name { get; set; }

        public DateTime prop_update_date_dt { get; set; }
        public String prop_update_date_str { get; set; }

        public Guid prop_update_by { get; set; }
        public String prop_update_by_name { get; set; }

        public String prop_errorMsg { get; set; }
        #endregion
    }

    #endregion
}