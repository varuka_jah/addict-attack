﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManagementSystem.Models
{
    public class UserLoginModel
    {
        [Display(Name = "ชื่อผู้ใช้งาน")]
        [Required(ErrorMessage = "The username is required")]
        public string UserName { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "รหัสผ่าน")]
        [Required(ErrorMessage = "The password is required")]
        public string Password { get; set; }



        [Display(Name = "Forgot Password?")]
        public bool ForgotPass { get; set; }

        [HiddenInput]
        public string ReturnUrl { get; set; }
    }
}