﻿using ManagementSystem.ConstUtil;
using ManagementSystem.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagementSystem.Models.Service
{
    public class MainConfigService
    {
        public MainConfigModel getMainConfig(dbEntities db)
        {
            try
            {
                MainConfigModel dtoMC = new MainConfigModel();

                var lstMainConfig = (from m in db.M_MainConfig
                                     select m).ToList();


                dtoMC.baseUploadFolder = (from m in lstMainConfig
                                          where m.configCode.Equals(ConstValue.MainConfig.baseUploadFolder)
                                          select m.configValue).SingleOrDefault();

                return dtoMC;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
    }
}