﻿using AutoMapper;
using ManagementSystem.Data;
using ManagementSystem.Models.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagementSystem.Models.Service
{
    public class ProductService
    {
        public void create(dbEntities db, UserDetailSession userInfo, ProductModel model)
        {
            try
            {
                var prod = (from s in db.M_Product
                            where s.prod_code == model.prod_code.Trim().ToUpper()
                            select s).SingleOrDefault();
                if (prod != null)
                {
                    throw new Exception(ConstUtil.MsgConst.ProdCodeDuplicate);
                }

                prod = (from s in db.M_Product
                        where s.prod_name == model.prod_name.Trim()
                        select s).SingleOrDefault();
                if (prod != null)
                {
                    throw new Exception(ConstUtil.MsgConst.ProdNameDuplicate);
                }


                prod = new M_Product();
                prod.prod_code = model.prod_code.Trim();
                prod.prod_createBy = userInfo.loginUserDetail.user_guid; 
                prod.prod_createDate = DateTime.Now;
                prod.prod_guid = Guid.NewGuid();
                prod.prod_imgUrl = model.prod_imgUrl.Trim();
                prod.prod_name = model.prod_name.Trim();
                prod.prod_pricePerUnit = model.prod_pricePerUnit;
                prod.prod_remark = (model.prop_remark == null ? "" : model.prop_remark.Trim());
                prod.prod_status = ConstUtil.ConstValue.Status.active;
                prod.prod_updateBy = userInfo.loginUserDetail.user_guid;
                prod.prod_updateDate = DateTime.Now;
                db.M_Product.Add(prod);

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public void update(dbEntities db, UserDetailSession userInfo, ProductModel model)
        {
            try
            {
                var prod = (from s in db.M_Product
                            where s.prod_guid == model.prod_guid
                            select s).SingleOrDefault();

                if (prod == null)
                {
                    throw new Exception("ไม่พบรายการที่ต้องการแก้ไขค่ะ");
                }

                var checkDup = (from s in db.M_Product
                                where s.prod_code == model.prod_code.Trim().ToUpper()
                                    && s.prod_guid != model.prod_guid
                                select s).SingleOrDefault();
                if (checkDup != null)
                {
                    throw new Exception(ConstUtil.MsgConst.ProdCodeDuplicate);
                }

                checkDup = (from s in db.M_Product
                            where s.prod_name == model.prod_name.Trim()
                                && s.prod_guid != model.prod_guid
                            select s).SingleOrDefault();
                if (checkDup != null)
                {
                    throw new Exception(ConstUtil.MsgConst.ProdNameDuplicate);
                }

                prod.prod_code = model.prod_code.Trim();
                prod.prod_imgUrl = model.prod_imgUrl.Trim();
                prod.prod_name = model.prod_name.Trim();
                prod.prod_pricePerUnit = model.prod_pricePerUnit;
                prod.prod_remark = (model.prop_remark == null ? "" : model.prop_remark.Trim());
                prod.prod_status = model.prop_activeStat;
                prod.prod_updateBy = userInfo.loginUserDetail.user_guid;
                prod.prod_updateDate = DateTime.Now;

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<ProductModel> searchByCriteria(dbEntities db, ProductSearchModel model)
        {
            try
            {
                var data = (from s in db.M_Product
                            join create in db.M_User on s.prod_createBy equals create.user_guid
                            join update in db.M_User on s.prod_updateBy equals update.user_guid
                            select new ProductModel()
                            {
                                prod_code = s.prod_code,
                                prod_guid = s.prod_guid,
                                prod_imgUrl = s.prod_imgUrl,
                                prod_fullPath = ConstUtil.ConstValue.UploadPath.product + s.prod_imgUrl,
                                prod_name = s.prod_name,
                                prod_pricePerUnit = s.prod_pricePerUnit, 
                                prop_activeStat = s.prod_status,
                                prop_create_by = s.prod_createBy,
                                prop_create_by_name = create.user_name,
                                prop_create_date_dt = s.prod_createDate,
                                prop_remark = s.prod_remark,
                                prop_update_by = s.prod_updateBy,
                                prop_update_by_name = update.user_name,
                                prop_update_date_dt = s.prod_updateDate,
                            });
                if (model.prod_code != null && !model.prod_code.Trim().Equals(""))
                {
                    data = data.Where(s => s.prod_code.Contains(model.prod_code.Trim()));
                }

                if (model.prod_name != null && !model.prod_name.Trim().Equals(""))
                {
                    data = data.Where(s => s.prod_name.Contains(model.prod_name.Trim()));
                }

                if (model.prod_guid != null && !model.prod_guid.Equals(Guid.Empty))
                {
                    data = data.Where(s => s.prod_guid.Equals(model.prod_guid));
                }

                #region Find by checkbox => Status
                List<String> lstStat = new List<string>();
                if (model.status_act)
                {
                    lstStat.Add(ConstUtil.ConstValue.Status.active);
                }
                if (model.status_ina)
                {
                    lstStat.Add(ConstUtil.ConstValue.Status.inactive);
                }

                if (lstStat.Count() > 0)
                {
                    data = data.Where(s => lstStat.Contains(s.prop_activeStat));
                }
                #endregion

                List<ProductModel> lstResult = data.OrderBy(s => s.prod_code).ToList<ProductModel>();
                
                lstResult = Mapper.Map<List<ProductModel>, List<ProductModel>>(lstResult);

                return lstResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

    }
}