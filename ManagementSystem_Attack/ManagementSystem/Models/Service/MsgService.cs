﻿using ManagementSystem.Data;
using ManagementSystem.Models.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagementSystem.Models.Service
{
    public class MsgService
    {
        public void create(dbEntities db, UserDetailSession userInfo, MsgModel model)
        {
            try
            {
                var msg = (from s in db.M_Message
                            where s.message_code == model.msg_code.Trim().ToUpper()
                            select s).SingleOrDefault();
                if (msg != null)
                {
                    throw new Exception(ConstUtil.MsgConst.MsgCodeDuplicate);
                }


                msg = new M_Message();
                msg.message_code = model.msg_code.Trim();
                msg.message_guid = Guid.NewGuid();
                msg.message_value = model.msg_value.Trim();
                msg.message_remark = (model.prop_remark == null ? "" : model.prop_remark.Trim());
                db.M_Message.Add(msg);

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public void update(dbEntities db, UserDetailSession userInfo, MsgModel model)
        {
            try
            {
                var msg = (from s in db.M_Message
                            where s.message_guid == model.msg_guid
                            select s).SingleOrDefault();

                if (msg == null)
                {
                    throw new Exception("ไม่พบรายการที่ต้องการแก้ไขค่ะ");
                }

                var checkDup = (from s in db.M_Message
                                where s.message_code == model.msg_code.Trim().ToUpper()
                                    && s.message_guid != model.msg_guid
                                select s).SingleOrDefault();
                if (checkDup != null)
                {
                    throw new Exception(ConstUtil.MsgConst.MsgCodeDuplicate);
                }

                msg.message_code = model.msg_code.Trim();
                msg.message_value = model.msg_value.Trim();
                msg.message_remark = (model.prop_remark == null ? "" : model.prop_remark.Trim());

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<MsgModel> searchByCriteria(dbEntities db, MsgSearchModel model)
        {
            try
            {
                var data = (from s in db.M_Message
                            select new MsgModel()
                            {
                                msg_code = s.message_code,
                                msg_guid = s.message_guid,
                                prop_remark = s.message_remark,
                                msg_value = s.message_value,
                              
                            });
                if (model.msg_code != null && !model.msg_code.Trim().Equals(""))
                {
                    data = data.Where(s => s.msg_code.Contains(model.msg_code.Trim()));
                }


                if (model.msg_guid != null && !model.msg_guid.Equals(Guid.Empty))
                {
                    data = data.Where(s => s.msg_guid.Equals(model.msg_guid));
                }

                List<MsgModel> lstResult = data.OrderBy(s => s.msg_code).ToList<MsgModel>();

                return lstResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

    }
}