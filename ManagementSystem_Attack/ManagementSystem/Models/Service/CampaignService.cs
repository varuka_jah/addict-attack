﻿using AutoMapper;
using ManagementSystem.ConstUtil;
using ManagementSystem.Data;
using ManagementSystem.Models.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagementSystem.Models.Service
{
    public class CampaignService
    {
        public void create(dbEntities db, UserDetailSession userInfo, CampaignModel model)
        {
            try
            {
                var camp = (from s in db.T_Campaign
                           where s.camp_code == model.camp_code.Trim().ToUpper()
                           select s).SingleOrDefault();
                if (camp != null)
                {
                    throw new Exception(ConstUtil.MsgConst.CampCodeDuplicate);
                }

                camp = (from s in db.T_Campaign
                            where s.camp_name == model.camp_name.Trim().ToUpper()
                            select s).SingleOrDefault();
                if (camp != null)
                {
                    throw new Exception(ConstUtil.MsgConst.CampNameDuplicate);
                }


                camp = new T_Campaign();
                camp.camp_budgetAmt = model.camp_budgetAmt;
                camp.camp_code = model.camp_code.Trim();
                camp.camp_createBy = userInfo.loginUserDetail.user_guid;
                camp.camp_createDate = DateTime.Now;
                camp.camp_desc = (model.camp_desc == null ? "" :  model.camp_desc.Trim());

                #region Date
                DateTime dtResult;
                ConstUtil.ConvertUtil.convertFromString_MDY(model.camp_endDate_str, out dtResult);
                camp.camp_endDate = dtResult;

                ConstUtil.ConvertUtil.convertFromString_MDY(model.camp_startDate_str, out dtResult);
                camp.camp_startDate = dtResult;
                #endregion

                camp.camp_guid = Guid.NewGuid();
                camp.camp_imgUrl = model.camp_imgUrl;
                camp.camp_name = model.camp_name.Trim();
                camp.camp_receiptMinAmt = model.camp_receiptMinAmt;
                camp.camp_remark = (model.prop_remark == null ? "" : model.prop_remark.Trim());
                
                camp.camp_status = ConstValue.Status.active;
                camp.camp_updateBy = userInfo.loginUserDetail.user_guid;
                camp.camp_updateDate = DateTime.Now;

                foreach (var item in model.lstPremium)
                {
                    T_CampaignPremium prem = new T_CampaignPremium();
                    prem.campPrem_campGuid =camp.camp_guid;
                    prem.campPrem_createBy = userInfo.loginUserDetail.user_guid;
                    prem.campPrem_createDate = DateTime.Now;
                    prem.campPrem_guid = Guid.NewGuid();
                    prem.campPrem_outstanding = item.prem_outstandingQty;
                    prem.campPrem_premGuid = item.prem_guid;
                    prem.campPrem_pricePerUnit = item.prem_pricePerUnit;
                    prem.campPrem_remaining = item.prem_outstandingQty;
                    prem.campPrem_remark = "";
                    prem.campPrem_status = ConstValue.Status.active;
                    prem.campPrem_updateBy = userInfo.loginUserDetail.user_guid;
                    prem.campPrem_updateDate = DateTime.Now;

                    #region Date
                    ConstUtil.ConvertUtil.convertFromString_MDY(item.prem_endDate_str, out dtResult);
                    prem.campPrem_endDt = dtResult;

                    ConstUtil.ConvertUtil.convertFromString_MDY(item.prem_startDate_str, out dtResult);
                    prem.campPrem_startDt = dtResult;
                    #endregion

                    db.T_CampaignPremium.Add(prem);
                }

                foreach (var item in model.lstProduct)
                {
                    T_CampaignProduct prod = new T_CampaignProduct();
                    prod.campProd_campGuid = camp.camp_guid;
                    prod.campProd_createBy = userInfo.loginUserDetail.user_guid;
                    prod.campProd_createDate = DateTime.Now;
                    prod.campProd_guid = Guid.NewGuid();
                    prod.campProd_prodGuid = item.prod_guid;
                    prod.campProd_prodPoint = item.prod_point;
                    prod.campProd_prodPricePerUnit = item.prod_pricePerUnit;
                    prod.campProd_remark = "";
                    prod.campProd_status = ConstValue.Status.active;
                    prod.campProd_updateBy = userInfo.loginUserDetail.user_guid;
                    prod.campProd_updateDate = DateTime.Now;
                    db.T_CampaignProduct.Add(prod);
                }

                db.T_Campaign.Add(camp);
                model.camp_guid = camp.camp_guid;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<CampaignSearchResultModel> searchByCriteria (dbEntities db, CampaignSearchModel model)
        {
            try
            {
                var data = (from ca in db.T_Campaign
                            join create in db.M_User on ca.camp_createBy equals create.user_guid
                            join update in db.M_User on ca.camp_updateBy equals update.user_guid
                            select new CampaignSearchResultModel()
                            {
                                prop_create_by = ca.camp_createBy,
                                prop_create_by_name = create.user_name,
                                prop_create_date_dt = create.user_createDate,
                                prop_update_by = ca.camp_updateBy,
                                prop_update_by_name = update.user_name,
                                prop_update_date_dt = ca.camp_updateDate, 
                                prop_activeStat = ca.camp_status,
                                camp_budgetAmt = ca.camp_budgetAmt,
                                camp_code = ca.camp_code,
                                camp_desc = ca.camp_desc,
                                camp_endDate_dt = ca.camp_endDate,
                                camp_fullPath = ConstUtil.ConstValue.UploadPath.campaign + ca.camp_imgUrl,
                                camp_guid = ca.camp_guid,
                                camp_imgUrl = ca.camp_imgUrl,
                                camp_name = ca.camp_name,
                                camp_receiptMinAmt = ca.camp_receiptMinAmt,
                                camp_startDate_dt = ca.camp_startDate
                            });

                if (model.camp_code != null && !model.camp_code.Trim().Equals(""))
                {
                    data = data.Where(s => s.camp_code.Contains(model.camp_code.Trim()));
                }

                if (model.camp_name != null && !model.camp_name.Trim().Equals(""))
                {
                    data = data.Where(s => s.camp_name.Contains(model.camp_name.Trim()));
                }

                if (model.camp_guid != null && !model.camp_guid.Equals(Guid.Empty))
                {
                    data = data.Where(s => s.camp_guid.Equals(model.camp_guid));
                }

                #region Find by checkbox => Status
                List<String> lstStat = new List<string>();
                if (model.status_act)
                {
                    lstStat.Add(ConstUtil.ConstValue.Status.active);
                }
                if (model.status_ina)
                {
                    lstStat.Add(ConstUtil.ConstValue.Status.inactive);
                }

                if (lstStat.Count() > 0)
                {
                    data = data.Where(s => lstStat.Contains(s.prop_activeStat));
                }
                #endregion

                List<CampaignSearchResultModel> lstResult = data.OrderByDescending(s => s.camp_startDate_dt).ToList<CampaignSearchResultModel>();

                lstResult = Mapper.Map<List<CampaignSearchResultModel>, List<CampaignSearchResultModel>>(lstResult);

                return lstResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        
        public CampaignModel getCampaignByGuId(dbEntities db, Guid campGuid)
        {
            try
            {
                var data = (from ca in db.T_Campaign
                            join create in db.M_User on ca.camp_createBy equals create.user_guid
                            join update in db.M_User on ca.camp_updateBy equals update.user_guid
                            where ca.camp_guid.Equals(campGuid)
                            select new CampaignModel()
                            {
                                prop_activeStat = ca.camp_status,
                                camp_budgetAmt = ca.camp_budgetAmt,
                                camp_code = ca.camp_code,
                                camp_desc = ca.camp_desc,
                                camp_endDate_dt = ca.camp_endDate,
                                camp_fullPath = ConstUtil.ConstValue.UploadPath.campaign + ca.camp_imgUrl,
                                camp_guid = ca.camp_guid,
                                camp_imgUrl = ca.camp_imgUrl,
                                camp_name = ca.camp_name,
                                camp_receiptMinAmt = ca.camp_receiptMinAmt,
                                camp_startDate_dt = ca.camp_startDate,
                                prop_remark = ca.camp_remark
                            }).SingleOrDefault();

                data = Mapper.Map<CampaignModel, CampaignModel>(data);
                data.lstPremium = (from c in db.T_CampaignPremium
                            join pr in db.M_Premium on c.campPrem_premGuid equals pr.prem_guid
                            where c.campPrem_campGuid.Equals(data.camp_guid)
                            select new CampaignPremiumModel()
                            {
                                camp_guid = c.campPrem_campGuid,
                                prop_activeStat = c.campPrem_status,
                                campPrem_guid = c.campPrem_guid,
                                prem_code = pr.prem_code,
                                prem_fullPath = ConstUtil.ConstValue.UploadPath.premium + pr.prem_imgUrl,
                                prem_guid = pr.prem_guid,
                                prem_imgUrl = pr.prem_imgUrl,
                                prem_name = pr.prem_name,
                                prem_outstandingQty = c.campPrem_outstanding,
                                prem_pricePerUnit = c.campPrem_pricePerUnit,
                                prem_remainingQty = c.campPrem_remaining,
                                prem_startDate_dt = c.campPrem_startDt, 
                                prem_endDate_dt = c.campPrem_endDt
                            }).OrderBy(s=>s.prem_startDate_dt).ThenBy(s=>s.prem_endDate_dt).ToList();
                data.lstPremium = Mapper.Map<List<CampaignPremiumModel>, List<CampaignPremiumModel>>(data.lstPremium);

                data.lstProduct = (from c in db.T_CampaignProduct
                            join pd in db.M_Product on c.campProd_prodGuid equals pd.prod_guid
                            where c.campProd_campGuid.Equals(data.camp_guid)
                            select new CampaignProductModel()
                            {
                               camp_guid = c.campProd_campGuid,
                                prop_activeStat = c.campProd_status,
                                campProd_guid = c.campProd_guid,
                                prod_code = pd.prod_code,
                                prod_fullPath = ConstUtil.ConstValue.UploadPath.product + pd.prod_imgUrl,
                                prod_guid = pd.prod_guid,
                                prod_imgUrl = pd.prod_imgUrl,
                                prod_name = pd.prod_name,
                                prod_point = c.campProd_prodPoint,
                                prod_pricePerUnit = c.campProd_prodPricePerUnit
                            }).OrderBy(s=>s.prod_name).ToList();
                return data;

            } catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public int getNoByCamp(dbEntities db, Guid campGuid)
        {
            try
            {
                var data = (from ca in db.T_CustomerReceipt
                            where ca.custRec_campGuid.Equals(campGuid)
                            select ca).ToList();
                if (data == null )
                {
                    data = new List<T_CustomerReceipt>();
                }
                return data.Count();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }


        public void update(dbEntities db, UserDetailSession userInfo, CampaignModel model)
        {
            try
            {
                var camp = (from s in db.T_Campaign
                            where s.camp_guid == model.camp_guid
                            select s).SingleOrDefault();

                if (camp == null)
                {
                    throw new Exception("ไม่พบรายการที่ต้องการแก้ไขค่ะ");
                }

                var checkDup = (from s in db.T_Campaign
                                where s.camp_code == model.camp_code.Trim().ToUpper()
                                    && s.camp_guid != model.camp_guid
                                select s).SingleOrDefault();
                if (checkDup != null)
                {
                    throw new Exception(ConstUtil.MsgConst.CampCodeDuplicate);
                }

                checkDup = (from s in db.T_Campaign
                            where s.camp_name == model.camp_name.Trim()
                                && s.camp_guid != model.camp_guid
                            select s).SingleOrDefault();
                if (checkDup != null)
                {
                    throw new Exception(ConstUtil.MsgConst.CampNameDuplicate);
                }

                camp.camp_budgetAmt = model.camp_budgetAmt;
                camp.camp_code = model.camp_code.Trim();
                camp.camp_desc = (model.camp_desc == null ? "" : model.camp_desc.Trim());

                #region Date
                DateTime dtResult;
                ConstUtil.ConvertUtil.convertFromString_MDY(model.camp_endDate_str, out dtResult);
                camp.camp_endDate = dtResult;

                ConstUtil.ConvertUtil.convertFromString_MDY(model.camp_startDate_str, out dtResult);
                camp.camp_startDate = dtResult;
                #endregion

                camp.camp_imgUrl = model.camp_imgUrl;
                camp.camp_name = model.camp_name.Trim();
                camp.camp_receiptMinAmt = model.camp_receiptMinAmt;
                camp.camp_remark = (model.prop_remark == null ? "" : model.prop_remark.Trim());
                camp.camp_status = model.prop_activeStat;
                camp.camp_updateBy = userInfo.loginUserDetail.user_guid;
                camp.camp_updateDate = DateTime.Now;


                foreach (var item in model.lstPremium)
                {
                    if (item.campPrem_guid == null || item.campPrem_guid.Equals(Guid.Empty))
                    {
                        T_CampaignPremium prem = new T_CampaignPremium();
                        prem.campPrem_campGuid = camp.camp_guid;
                        prem.campPrem_createBy = userInfo.loginUserDetail.user_guid;
                        prem.campPrem_createDate = DateTime.Now;
                        prem.campPrem_guid = Guid.NewGuid();
                        prem.campPrem_outstanding = item.prem_outstandingQty;
                        prem.campPrem_premGuid = item.prem_guid;
                        prem.campPrem_pricePerUnit = item.prem_pricePerUnit;
                        prem.campPrem_remaining = item.prem_outstandingQty;
                        prem.campPrem_remark = "";
                        prem.campPrem_status = ConstValue.Status.active;
                        prem.campPrem_updateBy = userInfo.loginUserDetail.user_guid;
                        prem.campPrem_updateDate = DateTime.Now;

                        #region Date
                        ConstUtil.ConvertUtil.convertFromString_MDY(item.prem_endDate_str, out dtResult);
                        prem.campPrem_endDt = dtResult;

                        ConstUtil.ConvertUtil.convertFromString_MDY(item.prem_startDate_str, out dtResult);
                        prem.campPrem_startDt = dtResult;
                        #endregion

                        db.T_CampaignPremium.Add(prem);
                    } else
                    {
                        var campPrem = (from pre in db.T_CampaignPremium
                                        where pre.campPrem_guid.Equals(item.campPrem_guid)
                                        select pre).Single();

                        campPrem.campPrem_outstanding = item.prem_outstandingQty;
                        campPrem.campPrem_premGuid = item.prem_guid;
                        campPrem.campPrem_pricePerUnit = item.prem_pricePerUnit;
                        campPrem.campPrem_remaining = item.prem_remainingQty;
                        campPrem.campPrem_remark = "";
                        campPrem.campPrem_status = item.prop_activeStat;
                        campPrem.campPrem_updateBy = userInfo.loginUserDetail.user_guid;
                        campPrem.campPrem_updateDate = DateTime.Now;

                        #region Date
                        ConstUtil.ConvertUtil.convertFromString_MDY(item.prem_endDate_str, out dtResult);
                        campPrem.campPrem_endDt = dtResult;

                        ConstUtil.ConvertUtil.convertFromString_MDY(item.prem_startDate_str, out dtResult);
                        campPrem.campPrem_startDt = dtResult;
                        #endregion
                    }


                }

                foreach (var item in model.lstProduct)
                {
                    if (item.campProd_guid == null || item.campProd_guid.Equals(Guid.Empty))
                    {
                        T_CampaignProduct prod = new T_CampaignProduct();
                        prod.campProd_campGuid = camp.camp_guid;
                        prod.campProd_createBy = userInfo.loginUserDetail.user_guid;
                        prod.campProd_createDate = DateTime.Now;
                        prod.campProd_guid = Guid.NewGuid();
                        prod.campProd_prodGuid = item.prod_guid;
                        prod.campProd_prodPoint = item.prod_point;
                        prod.campProd_prodPricePerUnit = item.prod_pricePerUnit;
                        prod.campProd_remark = "";
                        prod.campProd_status = ConstValue.Status.active;
                        prod.campProd_updateBy = userInfo.loginUserDetail.user_guid;
                        prod.campProd_updateDate = DateTime.Now;
                        db.T_CampaignProduct.Add(prod);
                    } else
                    {
                        var prod = (from pre in db.T_CampaignProduct
                                        where pre.campProd_guid.Equals(item.campProd_guid)
                                        select pre).Single();

                        prod.campProd_prodGuid = item.prod_guid;
                        prod.campProd_prodPoint = item.prod_point;
                        prod.campProd_prodPricePerUnit = item.prod_pricePerUnit;
                        prod.campProd_remark = "";
                        prod.campProd_status = ConstValue.Status.active;
                        prod.campProd_updateBy = userInfo.loginUserDetail.user_guid;
                        prod.campProd_updateDate = DateTime.Now;
                    }
                        
                }

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
    }
}