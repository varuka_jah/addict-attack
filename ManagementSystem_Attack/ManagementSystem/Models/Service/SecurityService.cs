﻿using AutoMapper;
using ManagementSystem.ConstUtil;
using ManagementSystem.Data;
using ManagementSystem.Models.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagementSystem.Models.Service
{
    public class SecurityService
    {
        public void roleCreate(dbEntities db, UserDetailSession userInfo, RoleModel model)
        {
            try
            {
                var role = (from s in db.M_Role
                           where s.roleName == model.role_name.Trim().ToUpper()
                           select s).SingleOrDefault();
                if (role != null)
                {
                    throw new Exception(ConstUtil.MsgConst.RoleNameDuplicate);
                }


                role = new M_Role();
                role.roleCreateBy = userInfo.loginUserDetail.user_guid;
                role.roleCreateDate = DateTime.Now;
                role.roleGuid = Guid.NewGuid();
                role.roleIsAdmin = model.role_isAdmin;
                role.roleName = model.role_name.Trim();
                role.roleStatus = ConstValue.Status.active;
                role.roleUpdateBy = userInfo.loginUserDetail.user_guid;
                role.roleUpdateDate = DateTime.Now;
                db.M_Role.Add(role);

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public void roleUpdate(dbEntities db, UserDetailSession userInfo, RoleModel model)
        {
            try
            {
                var role = (from s in db.M_Role
                           where s.roleGuid == model.role_guid
                           select s).SingleOrDefault();

                if (role == null)
                {
                    throw new Exception("ไม่พบรายการที่ต้องการแก้ไขค่ะ");
                }

                var checkDup = (from s in db.M_Role
                                where s.roleName == model.role_name.Trim().ToUpper()
                                    && s.roleGuid != model.role_guid
                                select s).SingleOrDefault();
                if (checkDup != null)
                {
                    throw new Exception(ConstUtil.MsgConst.RoleNameDuplicate);
                }

                role.roleIsAdmin = model.role_isAdmin;
                role.roleName = model.role_name.Trim();
                role.roleStatus = model.prop_activeStat;
                role.roleUpdateBy = userInfo.loginUserDetail.user_guid;
                role.roleUpdateDate = DateTime.Now;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<RoleModel> role_searchByCriteria(dbEntities db, RoleSearchModel model)
        {
            try
            {
                var data = (from s in db.M_Role
                            join create in db.M_User on s.roleCreateBy equals create.user_guid
                            join update in db.M_User on s.roleUpdateBy equals update.user_guid
                            select new RoleModel()
                            {
                                role_guid = s.roleGuid,
                                role_isAdmin = s.roleIsAdmin,
                                role_isAdmin_flg = (s.roleIsAdmin.Equals(ConstValue.Flag.yes) ? true : false),
                                role_name = s.roleName,
                                prop_activeStat = s.roleStatus,
                                prop_create_by = s.roleCreateBy,
                                prop_create_by_name = create.user_name,
                                prop_create_date_dt = s.roleCreateDate,
                                prop_update_by = s.roleUpdateBy,
                                prop_update_by_name = update.user_name,
                                prop_update_date_dt = s.roleUpdateDate,

                            });
                if (model.role_name != null && !model.role_name.Trim().Equals(""))
                {
                    data = data.Where(s => s.role_name.Contains(model.role_name.Trim()));
                }


                if (model.role_guid != null && !model.role_guid.Equals(Guid.Empty))
                {
                    data = data.Where(s => s.role_guid.Equals(model.role_guid));
                }

                List<RoleModel> lstResult = data.OrderBy(s => s.role_name).ToList<RoleModel>();
                lstResult = Mapper.Map<List<RoleModel>, List<RoleModel>>(lstResult);
                return lstResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public void userCreate(dbEntities db, UserDetailSession userInfo, UserModel model)
        {
            try
            {
                var data = (from s in db.M_User
                            where s.user_userName == model.user_username.Trim().ToUpper()
                            || s.user_email == model.user_email.Trim().ToUpper()
                            select s).SingleOrDefault();
                if (data != null)
                {
                    throw new Exception(ConstUtil.MsgConst.UserDuplicate);
                }


                data = new M_User();
                data.user_createBy = userInfo.loginUserDetail.user_guid;
                data.user_createDate = DateTime.Now;
                data.user_email = model.user_email.Trim();
                data.user_guid = Guid.NewGuid();
                data.user_name =model.user_name.Trim();
                data.user_password = ConvertUtil.GetMyEncryptPassword(model.user_password.Trim());
                data.user_roleGuid = model.role_guid;
                data.user_status = ConstValue.Status.active;
                data.user_updateBy = userInfo.loginUserDetail.user_guid;
                data.user_updateDate = DateTime.Now;
                data.user_userName = model.user_username.Trim();

                db.M_User.Add(data);

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public void userUpdate(dbEntities db, UserDetailSession userInfo, UserModel model)
        {
            try
            {
                var data = (from s in db.M_User
                            where s.user_guid == model.user_guid
                            select s).SingleOrDefault();

                if (data == null)
                {
                    throw new Exception("ไม่พบรายการที่ต้องการแก้ไขค่ะ");
                }

                var checkDup = (from s in db.M_User
                                where (s.user_userName == model.user_username.Trim().ToUpper()
                                    || s.user_email == model.user_email.Trim().ToUpper())
                                    && s.user_guid != model.user_guid
                                select s).SingleOrDefault();
                if (checkDup != null)
                {
                    throw new Exception(ConstUtil.MsgConst.UserDuplicate);
                }

                data.user_email = model.user_email.Trim();
                data.user_name = model.user_name.Trim();
                data.user_password = ConvertUtil.GetMyEncryptPassword(model.user_password.Trim());
                data.user_roleGuid = model.role_guid;
                data.user_status = model.prop_activeStat;
                data.user_updateBy = userInfo.loginUserDetail.user_guid;
                data.user_updateDate = DateTime.Now;
                data.user_userName = model.user_username.Trim();
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<UserModel> user_searchByCriteria(dbEntities db, UserSearchModel model)
        {
            try
            {
                var data = (from s in db.M_User 
                            join role in db.M_Role on s.user_roleGuid equals role.roleGuid
                            join create in db.M_User on s.user_createBy equals create.user_guid
                            join update in db.M_User on s.user_updateBy equals update.user_guid
                            select new UserModel()
                            {
                                user_email = s.user_email,
                                user_guid = s.user_guid,
                                user_name = s.user_name,
                                user_password = s.user_password,
                                user_username = s.user_userName,
                                role_guid = role.roleGuid,
                                role_isAdmin = role.roleIsAdmin,
                                role_isAdmin_flg = (role.roleIsAdmin.Equals(ConstValue.Flag.yes) ? true : false),
                                role_name = role.roleName,
                                prop_activeStat = s.user_status,
                                prop_create_by = s.user_createBy,
                                prop_create_by_name = create.user_name,
                                prop_create_date_dt = s.user_createDate,
                                prop_update_by = s.user_updateBy,
                                prop_update_by_name = update.user_name,
                                prop_update_date_dt = s.user_updateDate,

                            });

                if (model.user_name != null && !model.user_name.Trim().Equals(""))
                {
                    data = data.Where(s => s.user_name.Contains(model.user_name.Trim()));
                }
                if (model.user_username != null && !model.user_username.Trim().Equals(""))
                {
                    data = data.Where(s => s.user_username.Contains(model.user_username.Trim()));
                }

                if (model.user_guid != null && !model.user_guid.Equals(Guid.Empty))
                {
                    data = data.Where(s => s.user_guid.Equals(model.user_guid));
                }

                if (model.role_name != null && !model.role_name.Trim().Equals(""))
                {
                    data = data.Where(s => s.role_name.Contains(model.role_name.Trim()));
                }


                if (model.role_guid != null && !model.role_guid.Equals(Guid.Empty))
                {
                    data = data.Where(s => s.role_guid.Equals(model.role_guid));
                }

                List<UserModel> lstResult = data.OrderBy(s => s.user_name).ThenBy(s=>s.user_username).ToList<UserModel>();
                lstResult = Mapper.Map<List<UserModel>, List<UserModel>>(lstResult);
                return lstResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
    }
}