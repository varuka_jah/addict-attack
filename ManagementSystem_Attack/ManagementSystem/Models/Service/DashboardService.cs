﻿using ManagementSystem.ConstUtil;
using ManagementSystem.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using static ManagementSystem.ConstUtil.ConstValue;

namespace ManagementSystem.Models.Service
{
    public class DashboardService
    {
        public DashboardActionModel getData(dbEntities db, Guid camp_guid)
        {
            try
            {
                DashboardActionModel model = new DashboardActionModel();
                model.lstDateAct = new List<DateActivityModel>();
                model.dtoUserActivity = new ActivityModel();
                model.lstByStore = new List<TotalByStoreAndCate>();
                model.lstByStoreCate = new List<TotalByStoreAndCate>();
                model.dtoUserActivity = initialUserActivity(model.dtoUserActivity);
                
                var lstCustRec = (from rec in db.T_CustomerReceipt
                               where rec.custRec_campGuid.Equals(camp_guid)
                               select rec).ToList();

                var lstCampPre = (from prem in db.T_CampaignPremium
                                  where prem.campPrem_campGuid.Equals(camp_guid)
                                  && prem.campPrem_status.Equals(ConstValue.Status.active)
                                  select prem).ToList();

                NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
                nfi.PercentDecimalDigits = 2;
                nfi.PercentPositivePattern = 1;
                nfi.PercentNegativePattern = 1;

                if (lstCustRec != null && lstCustRec.Count() > 0)
                {
                    Dictionary<String, DateActivityModel> dicDateAct = new Dictionary<string, DateActivityModel>();

                    Dictionary<Guid, Guid> dicCustGuid = new Dictionary<Guid, Guid>();
                    Dictionary<String, Guid> dicCustDate = new Dictionary<string, Guid>();
                    foreach (var item in lstCustRec)
                    {
                        dicDateAct = assignUserActivity(model, dicDateAct, dicCustGuid, dicCustDate, item);
                    }

                    model.dtoUserActivity.totalCust = dicCustGuid.Count();
                    model.dtoUserActivity.avgBill = Convert.ToDouble(model.dtoUserActivity.totalRec) / Convert.ToDouble(model.dtoUserActivity.totalCust);
                    model.dtoUserActivity.perPlayGameByRec = (Convert.ToDouble(model.dtoUserActivity.totalPlayGameByRec) / Convert.ToDouble(model.dtoUserActivity.totalRec));
                    model.dtoUserActivity.perPlayGameByRec_str = model.dtoUserActivity.perPlayGameByRec.ToString("P", nfi);
                    foreach (var item in dicDateAct)
                    {
                        model.lstDateAct.Add(item.Value);
                    }
                    model.lstDateAct= model.lstDateAct.OrderBy(s => s.actionDate_dt).ToList();

                    foreach (var item in lstCampPre)
                    {
                        model.dtoUserActivity.totalPremium = model.dtoUserActivity.totalPremium + item.campPrem_outstanding;
                    }
                    model.dtoUserActivity.percTotalPrem = (Convert.ToDecimal(model.dtoUserActivity.totalWin) / Convert.ToDecimal(model.dtoUserActivity.totalPremium))*100;
                }


                // by store category
                var lstStoreCate = (from rec in db.T_CustomerReceipt
                                    join st in db.M_Store on rec.custRec_storeGuid equals st.store_guid
                                    join cate in db.M_StoreCategory on st.store_cateGuid equals cate.storeCate_guid
                                    select new TotalByStoreAndCate
                                    {
                                        storeCateName = cate.storeCate_name,
                                        storeName = st.store_name,
                                        totalCust = 0, 
                                        totalRec = 1,
                                        cust_guid = rec.custRec_custGuid
                                    }).ToList();
                if (lstStoreCate != null && lstStoreCate.Count() > 0)
                {

                    Dictionary<String, TotalByStoreAndCate> dicStore = new Dictionary<String, TotalByStoreAndCate>();
                    Dictionary<String, TotalByStoreAndCate> dicStoreCate = new Dictionary<String, TotalByStoreAndCate>();
                    Dictionary<String, Guid> dicStoreCust = new Dictionary<string, Guid>();
                    foreach (var item in lstStoreCate)
                    {
                        TotalByStoreAndCate dtoStoreCate = new TotalByStoreAndCate();
                        dicStore = assignByStore(dicStore, dicStoreCust, item);
                        dicStoreCate = assignByCate(dicStoreCate, item);
                        
                    }
                    foreach (var item in dicStore)
                    {
                        model.lstByStore.Add(item.Value);
                    }
                    foreach (var item in dicStoreCate)
                    {
                        model.lstByStoreCate.Add(item.Value);
                    }
                    model.lstByStoreCate = model.lstByStoreCate.OrderByDescending(s => s.totalRec).ToList();
                    model.lstByStore = model.lstByStore.OrderByDescending(s => s.totalRec).Take(15).ToList();
                }
                
                return model;
            } catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        private Dictionary<String, TotalByStoreAndCate> assignByCate(Dictionary<String, TotalByStoreAndCate> dicStoreCate, TotalByStoreAndCate item)
        {
            String key = item.storeCateName;
            TotalByStoreAndCate dtoStoreCate;
            if (dicStoreCate.ContainsKey(key))
            {
                dtoStoreCate = dicStoreCate[key];
                dtoStoreCate.totalRec = dtoStoreCate.totalRec + 1;
            } else
            {
                dtoStoreCate = new TotalByStoreAndCate();
                dtoStoreCate.cust_guid = item.cust_guid;
                dtoStoreCate.storeCateName = item.storeCateName;
                dtoStoreCate.storeName = "";
                dtoStoreCate.totalRec = 1;

                dicStoreCate.Add(key, dtoStoreCate);
            }
            

            return dicStoreCate;
        }

        private Dictionary<String, TotalByStoreAndCate> assignByStore(Dictionary<String, TotalByStoreAndCate> dicStore, Dictionary<string, Guid> dicStoreCust, TotalByStoreAndCate item)
        {
            TotalByStoreAndCate dtoStore;
            String key = item.storeName;
            String storeKey = item.storeName + item.cust_guid.ToString();
            if (dicStore.ContainsKey(key))
            {
                dtoStore = dicStore[key];
                dtoStore.totalRec = dtoStore.totalRec + 1;
                if (!dicStoreCust.ContainsKey(storeKey))
                {
                    dtoStore.totalCust = dtoStore.totalCust + 1;
                    dicStoreCust.Add(storeKey, item.cust_guid);
                }
            } else
            {
                
                dtoStore = new TotalByStoreAndCate();
                dtoStore.cust_guid = item.cust_guid;
                dtoStore.storeCateName = "";
                dtoStore.storeName = item.storeName;
                dtoStore.totalCust = dtoStore.totalCust + 1;
                dicStoreCust.Add(storeKey, item.cust_guid);
                dtoStore.totalRec = 1;

                dicStore.Add(key, dtoStore);
            }
           

            return dicStore;
        }

        private Dictionary<string, DateActivityModel> assignUserActivity(DashboardActionModel model, Dictionary<string, DateActivityModel> dicDateAct, Dictionary<Guid, Guid> dicCustGuid, Dictionary<String, Guid> dicCustDate, T_CustomerReceipt item)
        {
            model.dtoUserActivity.totalRec = model.dtoUserActivity.totalRec + 1;

            if (!dicCustGuid.ContainsKey(item.custRec_custGuid))
            {
                dicCustGuid.Add(item.custRec_custGuid, item.custRec_custGuid);
            }

            if (item.custRec_status.Equals(ConstUtil.ConstValue.Status.success))
            {
                model.dtoUserActivity.totalStat_appv = model.dtoUserActivity.totalStat_appv + 1;
            }
            else if (item.custRec_status.Equals(ConstUtil.ConstValue.Status.reject))
            {
                model.dtoUserActivity.totalStat_rejt = model.dtoUserActivity.totalStat_rejt + 1;
            }
            else
            {
                model.dtoUserActivity.totalStat_wait = model.dtoUserActivity.totalStat_wait + 1;
            }

            model.dtoUserActivity = assignActivityData(item, model.dtoUserActivity);
            dicDateAct = assignDateActivityData(dicDateAct, dicCustDate, item);
            return dicDateAct;
        }

        public Dictionary<String, DateActivityModel> assignDateActivityData(Dictionary<String, DateActivityModel> dicDateAct, Dictionary<String, Guid> dicCustDate, T_CustomerReceipt item)
        {
            try
            {
                String key = item.custRec_createDate.ToString("yyyyMMdd");
                String keyCustDate = key + item.custRec_custGuid.ToString();
                if (dicDateAct.ContainsKey(key))
                {
                    DateActivityModel dtoData = dicDateAct[key];
                    dtoData.totalRec = dtoData.totalRec+1;
                    if (!dicCustDate.ContainsKey(keyCustDate))
                    {
                        dtoData.totalCust = dtoData.totalCust + 1;
                        dicCustDate.Add(keyCustDate, item.custRec_custGuid);
                    }
                    dtoData.totalGainPoint = dtoData.totalGainPoint + item.custRec_totalGainPoint;
                    if (item.custRec_gameResult.Equals(ConstValue.Status.gameResult_win))
                    {
                        dtoData.totalWin = dtoData.totalWin+1;
                    }
                }
                else
                {
                    DateActivityModel dtoData = new DateActivityModel();
                    dtoData.actionDate_dt = item.custRec_createDate;
                    dtoData.actionDate_str = dtoData.actionDate_dt.ToString("dd/MM/yyyy");
                    dtoData.totalGainPoint = item.custRec_totalGainPoint;
                    dtoData.totalRec = 1;

                    dicCustDate.Add(keyCustDate, item.custRec_custGuid);
                    dtoData.totalCust = 1;
                    if (item.custRec_gameResult.Equals(ConstValue.Status.gameResult_win))
                    {
                        dtoData.totalWin  =  1;
                    }
                    else
                    {
                        dtoData.totalWin = 0;
                    }
                    dicDateAct.Add(key, dtoData);
                }
                return dicDateAct;
            } catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        public ActivityModel assignActivityData(T_CustomerReceipt item, ActivityModel dtoAct)
        {
            try
            {
                if (!item.custRec_status.Equals(Status.reject))
                {
                    dtoAct.totalGainPoint = dtoAct.totalGainPoint + item.custRec_totalGainPoint;
                }

                if (!item.custRec_gameResult.Equals(Status.gameResult_none))
                {
                    dtoAct.totalPlayGameByRec = dtoAct.totalPlayGameByRec + 1;
                }

                if (item.custRec_gameResult.Equals(ConstValue.Status.gameResult_win))
                {
                    dtoAct.totalWin = dtoAct.totalWin + 1;
                }

                return dtoAct;
            } catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        public ActivityModel initialUserActivity(ActivityModel dtoAct)
        {
            dtoAct = new ActivityModel();
            dtoAct.avgBill = 0;
            dtoAct.percTotalPrem = 0;
            dtoAct.perPlayGameByRec = 0;
            dtoAct.totalCust = 0;
            dtoAct.totalGainPoint = 0;
            dtoAct.totalPlayGameByRec = 0;
            dtoAct.totalPremium = 0;
            dtoAct.totalRec = 0;
            dtoAct.totalStat_appv = 0;
            dtoAct.totalStat_rejt = 0;
            dtoAct.totalStat_wait = 0;
            dtoAct.totalWin = 0;
            

            return dtoAct;
        }
    }
}