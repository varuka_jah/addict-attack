﻿using AutoMapper;
using ManagementSystem.Data;
using ManagementSystem.Models.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagementSystem.Models.Service
{
    public class CustomerService
    {
        //public void create(dbEntities db, UserDetailSession userInfo, CustomerModel model)
        //{
        //    try
        //    {
        //        var msg = (from s in db.M_Customer
        //                   where s.cust_tel == model.cust_tel.Trim().ToUpper()
        //                   select s).SingleOrDefault();
        //        if (msg != null)
        //        {
        //            throw new Exception(ConstUtil.MsgConst.MsgCodeDuplicate);
        //        }


        //        msg = new M_Message();
        //        msg.message_code = model.msg_code.Trim();
        //        msg.message_guid = Guid.NewGuid();
        //        msg.message_value = model.msg_value.Trim();
        //        msg.message_remark = (model.prop_remark == null ? "" : model.prop_remark.Trim());
        //        db.M_Message.Add(msg);

        //        db.SaveChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message.ToString());
        //    }
        //}

        //public void update(dbEntities db, UserDetailSession userInfo, MsgModel model)
        //{
        //    try
        //    {
        //        var msg = (from s in db.M_Message
        //                   where s.message_guid == model.msg_guid
        //                   select s).SingleOrDefault();

        //        if (msg == null)
        //        {
        //            throw new Exception("ไม่พบรายการที่ต้องการแก้ไขค่ะ");
        //        }

        //        var checkDup = (from s in db.M_Message
        //                        where s.message_code == model.msg_code.Trim().ToUpper()
        //                            && s.message_guid != model.msg_guid
        //                        select s).SingleOrDefault();
        //        if (checkDup != null)
        //        {
        //            throw new Exception(ConstUtil.MsgConst.MsgCodeDuplicate);
        //        }

        //        msg.message_code = model.msg_code.Trim();
        //        msg.message_value = model.msg_value.Trim();
        //        msg.message_remark = (model.prop_remark == null ? "" : model.prop_remark.Trim());

        //        db.SaveChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message.ToString());
        //    }
        //}

        public void update(dbEntities ctx, UserDetailSession userInfo, CustomerModel customer)
        {
            try
            {
                var m_customer = ctx.M_Customer.Find(customer.cust_guid);
                if (m_customer == null)
                {
                    throw new Exception("ไม่พบข้อมูลลูกค้าค่ะ");
                }

                m_customer.cust_firstName = customer.cust_firstname.Trim();
                m_customer.cust_lastName = customer.cust_lastname.Trim();
                m_customer.cust_tel = customer.cust_tel.Trim();
                m_customer.cust_addr1 = (customer.cust_addr1 == null ? "" : customer.cust_addr1.Trim());
                m_customer.cust_addr2 = (customer.cust_addr2 == null ? "" : customer.cust_addr2.Trim()) ;
                m_customer.cust_provId = customer.provinceId ;
                m_customer.cust_distId = customer.distId;
                m_customer.cust_subId = customer.subDistId;
                m_customer.cust_updateBy = userInfo.UserID;
                m_customer.cust_updateDate = DateTime.Now;

                ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        public List<CustomerModel> searchByCriteria(dbEntities db, CustomerSearchModel model, out int filterRecords)
        {
            try
            {
                var data = (from cust in db.M_Customer
                            select new CustomerModel()
                            {
                                provinceId = cust.cust_provId, 
                                distId = cust.cust_distId, 
                                subDistId = cust.cust_subId,
                                cust_addr1 = cust.cust_addr1,
                                cust_tel = cust.cust_tel,
                                cust_addr2 = cust.cust_addr2,
                                cust_email = cust.cust_email,
                                cust_firstname = cust.cust_firstName,
                                cust_fullname = cust.cust_firstName + " " + cust.cust_lastName,
                                cust_guid = cust.cust_guid,
                                cust_lastname = cust.cust_lastName,
                                cust_password = cust.cust_password,
                                prop_activeStat = cust.cust_status,
                                prop_create_by = cust.cust_createBy,
                                prop_create_date_dt = cust.cust_createDate,
                                prop_remark = cust.cust_remark,
                                prop_update_by = cust.cust_updateBy,
                                prop_update_date_dt = cust.cust_updateDate
                            });
                if (model.cust_tel != null && !model.cust_tel.Trim().Equals(""))
                {
                    data = data.Where(s => s.cust_tel.Contains(model.cust_tel.Trim()));
                }

                if (model.cust_fullname != null && !model.cust_fullname.Trim().Equals(""))
                {
                    data = data.Where(s => s.cust_fullname.Contains(model.cust_fullname.Trim()));
                }

                if (model.cust_guid != null && !model.cust_guid.Equals(Guid.Empty))
                {
                    data = data.Where(s => s.cust_guid.Equals(model.cust_guid));
                }

                #region Find by checkbox => Status
                List<String> lstStat = new List<string>();
                if (model.status_act)
                {
                    lstStat.Add(ConstUtil.ConstValue.Status.active);
                }
                if (model.status_ina)
                {
                    lstStat.Add(ConstUtil.ConstValue.Status.inactive);
                }

                if (lstStat.Count() > 0)
                {
                    data = data.Where(s => lstStat.Contains(s.prop_activeStat));
                }
                #endregion

                List<CustomerModel> lstResult = data.OrderBy(s => s.cust_fullname).Skip(model.start).Take(model.length).ToList<CustomerModel>();
                filterRecords = data.Count();
                lstResult = Mapper.Map<List<CustomerModel>, List<CustomerModel>>(lstResult);

                for (int i = 0; lstResult != null && i < lstResult.Count(); i++)
                {
                    if (lstResult[i].provinceId > 0)
                    {
                        long provId = lstResult[i].provinceId;
                        var prov = (from us in db.M_Province
                                    where us.provId == provId
                                    select us).SingleOrDefault();


                        lstResult[i].provinceId = prov.provId;
                        lstResult[i].provinceCode = prov.provCode;
                        lstResult[i].provinceNameEn = prov.provNameEn;
                        lstResult[i].provinceNameTh = prov.provNameTh;
                    }

                    if (lstResult[i].distId > 0)
                    {
                        long distId = lstResult[i].distId;
                        var dist = (from us in db.M_District
                                    where us.distId == distId
                                    select us).SingleOrDefault();


                        lstResult[i].distCode = dist.distCode;
                        lstResult[i].distId = dist.distId;
                        lstResult[i].distNameEn = dist.name_en;
                        lstResult[i].distNameTh = dist.name_th;
                    }

                    if (lstResult[i].subDistId > 0)
                    {
                        long subId = lstResult[i].subDistId;
                        var subD = (from us in db.M_SubDistrict
                                    where us.subDistId == subId
                                    select us).SingleOrDefault();

                        lstResult[i].subDistId = subD.subDistId;
                        lstResult[i].subDistNameEn = subD.subDistNameEn;
                        lstResult[i].subDistNameTh = subD.subDistNameTh;
                        lstResult[i].subDistZipcode = subD.subDistZipCode;
                    }
                }
                return lstResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<CustomerModel> searchByCriteria(dbEntities db, CustomerSearchModel model)
        {
            try
            {
                var data = (from cust in db.M_Customer
                            select new CustomerModel()
                            {
                                provinceId = cust.cust_provId,
                                distId = cust.cust_distId,
                                subDistId = cust.cust_subId,
                                cust_addr1 = cust.cust_addr1,
                                cust_tel = cust.cust_tel,
                                cust_addr2 = cust.cust_addr2,
                                cust_email = cust.cust_email,
                                cust_firstname = cust.cust_firstName,
                                cust_fullname = cust.cust_firstName + " " + cust.cust_lastName,
                                cust_guid = cust.cust_guid,
                                cust_lastname = cust.cust_lastName,
                                cust_password = cust.cust_password,
                                prop_activeStat = cust.cust_status,
                                prop_create_by = cust.cust_createBy,
                                prop_create_date_dt = cust.cust_createDate,
                                prop_remark = cust.cust_remark,
                                prop_update_by = cust.cust_updateBy,
                                prop_update_date_dt = cust.cust_updateDate
                            });
                if (model.cust_tel != null && !model.cust_tel.Trim().Equals(""))
                {
                    data = data.Where(s => s.cust_tel.Contains(model.cust_tel.Trim()));
                }

                if (model.cust_fullname != null && !model.cust_fullname.Trim().Equals(""))
                {
                    data = data.Where(s => s.cust_fullname.Contains(model.cust_fullname.Trim()));
                }

                if (model.cust_guid != null && !model.cust_guid.Equals(Guid.Empty))
                {
                    data = data.Where(s => s.cust_guid.Equals(model.cust_guid));
                }

                #region Find by checkbox => Status
                List<String> lstStat = new List<string>();
                if (model.status_act)
                {
                    lstStat.Add(ConstUtil.ConstValue.Status.active);
                }
                if (model.status_ina)
                {
                    lstStat.Add(ConstUtil.ConstValue.Status.inactive);
                }

                if (lstStat.Count() > 0)
                {
                    data = data.Where(s => lstStat.Contains(s.prop_activeStat));
                }
                #endregion
                List<CustomerModel> lstResult = data.OrderBy(s => s.cust_fullname).ToList<CustomerModel>();
             
                lstResult = Mapper.Map<List<CustomerModel>, List<CustomerModel>>(lstResult);
                for (int i = 0; lstResult != null && i < lstResult.Count(); i++)
                {
                    if (lstResult[i].provinceId > 0)
                    {
                        long provId = lstResult[i].provinceId;
                        var prov = (from us in db.M_Province
                                    where us.provId == provId
                                    select us).SingleOrDefault();


                        lstResult[i].provinceId = prov.provId;
                        lstResult[i].provinceCode = prov.provCode;
                        lstResult[i].provinceNameEn = prov.provNameEn;
                        lstResult[i].provinceNameTh = prov.provNameTh;
                    }

                    if (lstResult[i].distId > 0)
                    {
                        long distId = lstResult[i].distId;
                        var dist = (from us in db.M_District
                                    where us.distId == distId
                                    select us).SingleOrDefault();


                        lstResult[i].distCode = dist.distCode;
                        lstResult[i].distId = dist.distId;
                        lstResult[i].distNameEn = dist.name_en;
                        lstResult[i].distNameTh = dist.name_th;
                    }

                    if (lstResult[i].subDistId > 0)
                    {
                        long subId = lstResult[i].subDistId;
                        var subD = (from us in db.M_SubDistrict
                                    where us.subDistId == subId
                                    select us).SingleOrDefault();

                        lstResult[i].subDistId = subD.subDistId;
                        lstResult[i].subDistNameEn = subD.subDistNameEn;
                        lstResult[i].subDistNameTh = subD.subDistNameTh;
                        lstResult[i].subDistZipcode = subD.subDistZipCode;
                    }
                }
                return lstResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public CustomerModel getCustomerModelInfoByID(dbEntities db, Guid custGuid)
        {
            try
            {
                CustomerModel custModel  = (from cust in db.M_Customer
                                  where cust.cust_guid.Equals(custGuid)
                                select new CustomerModel()
                                {
                                    cust_addr1 = cust.cust_addr1,
                                    cust_tel = cust.cust_tel,
                                    cust_addr2 = cust.cust_addr2,
                                    cust_email = cust.cust_email,
                                    cust_firstname = cust.cust_firstName,
                                    cust_fullname = cust.cust_firstName + " " + cust.cust_lastName,
                                    cust_guid = cust.cust_guid,
                                    cust_lastname = cust.cust_lastName,
                                    cust_password = cust.cust_password,
                                    prop_activeStat = cust.cust_status,
                                    prop_create_by = cust.cust_createBy,
                                    prop_create_date_dt = cust.cust_createDate,
                                    prop_remark = cust.cust_remark,
                                    prop_update_by = cust.cust_updateBy,
                                    prop_update_date_dt = cust.cust_updateDate
                                }).SingleOrDefault();


                custModel.provinceCode = "";
                custModel.provinceNameEn = "";
                custModel.provinceNameTh = "";
                custModel.distCode = "";
                custModel.distNameEn = "";
                custModel.distNameTh = "";
                custModel.subDistNameEn = "";
                custModel.subDistNameTh = "";
                custModel.subDistZipcode = "";
                if (custModel.provinceId > 0)
                {
                    var province = (from us in db.M_Province
                                    where us.provId == custModel.provinceId
                                    select us).SingleOrDefault();
                    custModel.provinceCode = province.provCode;
                    custModel.provinceId = province.provId;
                    custModel.provinceNameEn = province.provNameEn;
                    custModel.provinceNameTh = province.provNameTh;
                }

                if (custModel.distId > 0)
                {
                    var district = (from us in db.M_District
                                    where us.distId == custModel.distId
                                    select us).SingleOrDefault();
                    custModel.distCode = district.distCode;
                    custModel.distId = district.distId;
                    custModel.distNameEn = district.name_en;
                    custModel.distNameTh = district.name_th;
                }

                if (custModel.subDistId > 0)
                {
                    var subDist = (from us in db.M_SubDistrict
                                    where us.subDistId == custModel.subDistId
                                   select us).SingleOrDefault();

                    custModel.subDistId = subDist.subDistId;
                    custModel.subDistNameEn = subDist.subDistNameEn;
                    custModel.subDistNameTh = subDist.subDistNameTh;
                    custModel.subDistZipcode = subDist.subDistZipCode;
                }

                return custModel;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<AddrProviceModel> getProvince(dbEntities db)
        {
            try
            {
                var prov = (from m in db.M_Province
                            select new AddrProviceModel()
                            {
                                provinceCode = m.provCode,
                                provinceId = m.provId,
                                provinceNameEn = m.provNameEn,
                                provinceNameTh = m.provNameTh
                            });


                List<AddrProviceModel> lstResult = prov.OrderBy(s => s.provinceNameTh).ToList<AddrProviceModel>();
                return lstResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<AddrDistrictModel> getDistrictByProv(long provinceId)
        {
            try
            {
                using (dbEntities ctx = new dbEntities())
                {
                    var prov = (from a in ctx.M_District
                                join m in ctx.M_Province on a.province_id equals m.provId
                                where m.provId == provinceId
                                select new AddrDistrictModel()
                                {
                                    distCode = a.distCode,
                                    distId = a.distId,
                                    distNameEn = a.name_en,
                                    distNameTh = a.name_th,
                                    provinceCode = m.provCode,
                                    provinceId = m.provId,
                                    provinceNameEn = m.provNameEn,
                                    provinceNameTh = m.provNameTh
                                });
                    List<AddrDistrictModel> lstResult = prov.OrderBy(s => s.distNameTh).ToList<AddrDistrictModel>();
                    return lstResult;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<AddrSubDistrictModel> getSubDistrictByDistrict(long districtId)
        {
            try
            {
                using (dbEntities ctx = new dbEntities())
                {
                    var prov = (from b in ctx.M_SubDistrict
                                join a in ctx.M_District on b.subDistDistId equals a.distId
                                join m in ctx.M_Province on a.province_id equals m.provId
                                where b.subDistDistId == districtId
                                select new AddrSubDistrictModel()
                                {
                                    subDistId = b.subDistId,
                                    subDistNameEn = b.subDistNameEn,
                                    subDistNameTh = b.subDistNameTh,
                                    subDistZipcode = b.subDistZipCode,
                                    distCode = a.distCode,
                                    distId = a.distId,
                                    distNameEn = a.name_en,
                                    distNameTh = a.name_th,
                                    provinceCode = m.provCode,
                                    provinceId = m.provId,
                                    provinceNameEn = m.provNameEn,
                                    provinceNameTh = m.provNameTh
                                });


                    List<AddrSubDistrictModel> lstResult = prov.OrderBy(s => s.subDistNameTh).ToList<AddrSubDistrictModel>();
                    return lstResult;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
    }
}