﻿using ManagementSystem.ConstUtil;
using ManagementSystem.Data;
using ManagementSystem.Models.Session;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ManagementSystem.Models.Service
{



    public class SMSUpload
    {
        public List<SMSUploadModel> readSmsFromFile(dbEntities db, HttpPostedFileBase file, out bool isValid)
        {
            //DivSearchModel model = new DivSearchModel();
            // = new List<DivActionModel>();
            try
            {
                
                isValid = true;
                ExcelUtil excel = new ExcelUtil(file);
                List<DataRow> lstRow = excel.GetData(ConstValue.ExcelSheetName.sms, true);

                List<SMSUploadModel> lstResult = (from row in lstRow
                                                  select new SMSUploadModel()
                                                  {
                                                      campName = (row[0] == null ? "" : row[0].ToString().ToString()),
                                                      createTime = (row[1] == null ? "" : row[1].ToString().ToString()),
                                                      updateTime = (row[2] == null ? "" : row[2].ToString().ToString()),
                                                      shortCode = (row[3] == null ? "" : row[3].ToString().ToString()),
                                                      campSenderName = (row[4] == null ? "" : row[4].ToString().ToString()),
                                                      Ooperator = (row[5] == null ? "" : row[5].ToString().ToString()),
                                                      msisdn = (row[6] == null ? "" : row[6].ToString().ToString()),
                                                      message = (row[7] == null ? "" : row[7].ToString().ToString()),
                                                      keyword = (row[8] == null ? "" : row[8].ToString().ToString()),
                                                      context = (row[9] == null ? "" : row[9].ToString().ToString()),
                                                      replyMessage = (row[10] == null ? "" : row[10].ToString().ToString()),
                                                      messageStatus = (row[11] == null ? "" : row[11].ToString().ToString()),
                                                      errorMsg = (row[12] == null ? "" : row[12].ToString().ToString()),
                                                      campaignId = (row[13] == null ? "" : row[13].ToString().ToString()),
                                                      company = (row[14] == null ? "" : row[14].ToString().ToString()),
                                                      department = (row[15] == null ? "" : row[15].ToString().ToString()),
                                                      brand = (row[16] == null ? "" : row[16].ToString().ToString()),
                                                      prop_errorMsg = "",
                                                      prop_flagstat = true
                                                 }).ToList();
                if (lstResult != null && lstResult.Count() > 0)
                {
                    foreach (var item in lstResult)
                    {
                        if (!item.messageStatus.ToUpper().Equals("SUCCESS"))
                        {
                            item.prop_errorMsg = "ไม่นำรายการเข้าระบบ";
                            item.prop_flagstat = false;
                        }
                    }
                    lstResult = lstResult.OrderByDescending(s => s.prop_flagstat).ThenByDescending(s => s.createTime).ToList();
                }
                

                return lstResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public void uploadSMSData(dbEntities db, UserDetailSession userInfo, List<SMSUploadModel> lstData)
        {
                try
                {
                   
                List<T_CustomerReceipt> lstInsertRec = new List<T_CustomerReceipt>();
                List<T_CustomerProduct> lstInsertCustProd = new List<T_CustomerProduct>();
                List<M_Customer> lstInsertCust = new List<M_Customer>();
                List<T_SmsUpload> lstInsertSMS = new List<T_SmsUpload>();

                if (lstData != null && lstData.Count() > 0)
                {
                    Guid storeGuid = getConstantForStore(db, "SMS");
                    Guid prodGuid = getConstantForProd(db, "SMS");

                    List<M_Customer> lstAllCust = (from cust in db.M_Customer
                                                   select cust).ToList();

                    Guid uploadKeyGroup = Guid.NewGuid();

                    foreach (var item in lstData)
                    {
                        // generate log
                        T_SmsUpload dtoSMS = generateSMSUpload(uploadKeyGroup, item);

                        if (!item.messageStatus.ToUpper().Equals("SUCCESS"))
                        {
                            dtoSMS.uploadMsg = "ไม่นำรายการเข้าระบบ:: sms failure";
                            lstInsertSMS.Add(dtoSMS);
                            continue;
                        }
                        else
                        {
                            Guid campGuid = getCampGuidByCampCode(db, item.campaignId.Trim().ToUpper());

                            T_CustomerReceipt dtoRec = new T_CustomerReceipt();
                            String[] arr = item.context.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries);

                            String tel = "0" + item.msisdn.Trim().Substring(2);
                            Guid custGuid = generateNewCustomer(userInfo, lstInsertCust, lstAllCust, arr[1].Trim(), arr[2].Trim(), tel);


                            dtoRec.custRec_recpNo = arr[0].ToUpper();
                            // check exist rec no 
                            bool isExist = false;
                            isExist = checkIsExistRecNo(db, lstInsertRec, dtoRec, isExist);

                            if (isExist)
                            {
                                dtoSMS.uploadMsg = "ไม่นำรายการเข้าระบบ:: เลขที่ใบเสร็จซ้ำ";
                                lstInsertSMS.Add(dtoSMS);
                                continue;
                            }

                            dtoRec = generatePropForCustRec(userInfo, dtoRec, "SMS.png");
                            dtoRec.custRec_campGuid = campGuid;
                            dtoRec.custRec_custGuid = custGuid;
                            dtoRec.custRec_storeGuid = storeGuid;
                            dtoRec.custRec_createDate = convertUploadDateFromStringToDate(item.createTime);
                            dtoRec.custRec_updateDate = convertUploadDateFromStringToDate(item.updateTime);

                            lstInsertRec.Add(dtoRec);

                            T_CustomerProduct dtoProd = generateNewCustProd(userInfo, prodGuid, dtoRec);
                            lstInsertCustProd.Add(dtoProd);

                            dtoSMS.uploadMsg = "Import Success";
                            lstInsertSMS.Add(dtoSMS);
                        }
                    }
                }

                db.T_CustomerProduct.AddRange(lstInsertCustProd);
                db.T_CustomerReceipt.AddRange(lstInsertRec);
                db.M_Customer.AddRange(lstInsertCust);
                db.T_SmsUpload.AddRange(lstInsertSMS);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        private DateTime convertUploadDateFromStringToDate(String strDateTime)
        {
            DateTime dt = new DateTime(Convert.ToInt32(strDateTime.Substring(0, 4)),
                Convert.ToInt32(strDateTime.Substring(4, 2)),
                Convert.ToInt32(strDateTime.Substring(6, 2)),
                Convert.ToInt32(strDateTime.Substring(8, 2)),
                Convert.ToInt32(strDateTime.Substring(10, 2)),
                Convert.ToInt32(strDateTime.Substring(12, 2)));

            return dt;
        }

        private DateTime convertUploadOnlyDateFromStringToDate(String strDateTime)
        {
            DateTime dt = new DateTime(Convert.ToInt32(strDateTime.Substring(0, 4)),
                Convert.ToInt32(strDateTime.Substring(4, 2)),
                Convert.ToInt32(strDateTime.Substring(6, 2)),0,0,0);

            return dt;
        }


        private static T_SmsUpload generateSMSUpload(Guid uploadKeyGroup, SMSUploadModel item)
        {
            T_SmsUpload dtoSMS = new T_SmsUpload();
            dtoSMS.brand = item.brand;
            dtoSMS.campaignId = item.campaignId;
            dtoSMS.campName = item.campName;
            dtoSMS.campSender = item.campSenderName;
            dtoSMS.company = item.company;
            dtoSMS.context = item.context;
            dtoSMS.createTime = item.createTime;
            dtoSMS.department = item.department;
            dtoSMS.keyGroup = uploadKeyGroup;
            dtoSMS.keyword = item.keyword;
            dtoSMS.message = item.message;
            dtoSMS.messageStatus = item.messageStatus;
            dtoSMS.msisdn = item.msisdn;
            dtoSMS.@operator = item.Ooperator;
            dtoSMS.replyMessage = item.replyMessage;
            dtoSMS.shortCode = item.shortCode;
            dtoSMS.updateTime = item.updateTime;
            return dtoSMS;
        }

        private static Guid getCampGuidByCampCode(dbEntities db, String campCode)
        {
            return (from camp in db.T_Campaign
                    where camp.camp_code.ToUpper().Equals(campCode)
                    select camp.camp_guid).Single();
        }

        private static Guid getConstantForProd(dbEntities db, String prodType)
        {
            return (from prod in db.M_Product
                    where prod.prod_code.ToUpper().Equals(prodType)
                    select prod.prod_guid).Single();
        }

        private static Guid getConstantForStore(dbEntities db, String storeType)
        {
            return (from store in db.M_Store
                    where store.store_name.ToUpper().Equals(storeType)
                    select store.store_guid).Single();
        }

        private static bool checkIsExistRecNo(dbEntities db, List<T_CustomerReceipt> lstInsertRec, T_CustomerReceipt dtoRec, bool isExist)
        {
            var recNo = (from rec in db.T_CustomerReceipt
                         where rec.custRec_recpNo.ToUpper().Equals(dtoRec.custRec_recpNo)
                         && rec.custRec_branchName.Equals("")
                         select rec.custRec_recpNo).SingleOrDefault();
            if (recNo != null && !recNo.Trim().Equals(""))
            {
                isExist = true;
            }
            else
            {
                recNo = (from rec in lstInsertRec
                         where rec.custRec_recpNo.ToUpper().Equals(dtoRec.custRec_recpNo)
                        && rec.custRec_branchName.Equals("")
                         select rec.custRec_recpNo).SingleOrDefault();
                if (recNo != null && !recNo.Trim().Equals(""))
                {
                    isExist = true;
                }
            }

            return isExist;
        }

        private static T_CustomerReceipt generatePropForCustRec(UserDetailSession userInfo, T_CustomerReceipt dtoRec, String recImgUrl)
        {
            dtoRec.custRec_branchName = "";
            dtoRec.custRec_recpImgUrl = recImgUrl;
            dtoRec.custRec_createBy = userInfo.loginUserDetail.user_guid;
            dtoRec.custRec_createDate = DateTime.Now;
            dtoRec.custRec_gameResult = ConstValue.Status.gameResult_none;
            dtoRec.custRec_guid = Guid.NewGuid();
            dtoRec.custRec_remark = "";
            dtoRec.custRec_status = ConstValue.Status.init;
            dtoRec.custRec_totalGainPoint = 1;
            dtoRec.custRec_updateBy = userInfo.loginUserDetail.user_guid;
            dtoRec.custRec_updateDate = DateTime.Now;
            return dtoRec;
        }

        private static T_CustomerProduct generateNewCustProd(UserDetailSession userInfo, Guid prodGuid, T_CustomerReceipt dtoRec)
        {
            T_CustomerProduct dtoProd = new T_CustomerProduct();
            dtoProd.custProd_createBy = userInfo.loginUserDetail.user_guid;
            dtoProd.custProd_createDate = DateTime.Now;
            dtoProd.custProd_custRecGuid = dtoRec.custRec_guid;
            dtoProd.custProd_guid = Guid.NewGuid();
            dtoProd.custProd_prodGuid = prodGuid;
            dtoProd.custProd_qty = 1;
            dtoProd.custProd_remark = "";
            dtoProd.custProd_status = ConstValue.Status.init;
            dtoProd.custProd_totalPoint = 0;
            dtoProd.custProd_updateBy = userInfo.loginUserDetail.user_guid;
            dtoProd.custProd_updateDate = DateTime.Now;
            return dtoProd;
        }

        private Guid generateNewCustomer(UserDetailSession userInfo, List<M_Customer> lstInsertCust, List<M_Customer> lstAllCust, String firstname, String lastname, string tel)
        {
            Guid custGuid = (from c in lstAllCust
                             where c.cust_tel.Equals(tel)
                             select c.cust_guid).SingleOrDefault();

            if (custGuid == null || custGuid.Equals(Guid.Empty))
            {
                custGuid = (from c in lstInsertCust
                            where c.cust_tel.Equals(tel)
                            select c.cust_guid).SingleOrDefault();
                if (custGuid == null || custGuid.Equals(Guid.Empty))
                {
                    // new Customer
                    M_Customer dtoCust = new M_Customer();
                    dtoCust.cust_addr1 = "";
                    dtoCust.cust_addr2 = "";
                    dtoCust.cust_createBy = userInfo.loginUserDetail.user_guid;
                    dtoCust.cust_createDate = DateTime.Now;
                    dtoCust.cust_distId = 0;
                    dtoCust.cust_email = "";
                    dtoCust.cust_firstName = firstname;
                    dtoCust.cust_guid = Guid.NewGuid();
                    dtoCust.cust_lastName = lastname;
                    dtoCust.cust_password = "";
                    dtoCust.cust_provId = 0;
                    dtoCust.cust_remark = "";
                    dtoCust.cust_status = ConstValue.Status.init;
                    dtoCust.cust_subId = 0;
                    dtoCust.cust_tel = tel;
                    dtoCust.cust_updateBy = userInfo.loginUserDetail.user_guid;
                    dtoCust.cust_updateDate = DateTime.Now;

                    lstInsertCust.Add(dtoCust);
                    custGuid = dtoCust.cust_guid;
                }  
            }

            return custGuid;
        }

        public List<CouponUploadModel> readCouponFromFile(dbEntities db, HttpPostedFileBase file, out bool isValid)
        {
            try
            {

                isValid = true;
                ExcelUtil excel = new ExcelUtil(file);
                List<System.Data.DataRow> lstRow = excel.GetData(ConstValue.ExcelSheetName.coupon, true);

                List<CouponUploadModel> lstResult = (from row in lstRow
                                                     select new CouponUploadModel()
                                                     {
                                                         campaignId = (row[0] == null ? "" : row[0].ToString().ToString()),
                                                         createTime = (row[1] == null ? "" : row[1].ToString().ToString()),
                                                         tel = (row[2] == null ? "" : row[2].ToString().ToString()),
                                                         firstname = (row[3] == null ? "" : row[3].ToString().ToString()),
                                                         lastname = (row[4] == null ? "" : row[4].ToString().ToString()),
                                                         recNo = (row[5] == null ? "" : row[5].ToString().ToString()),
                                                         prop_errorMsg = "",
                                                         prop_flagstat = true
                                                     }).ToList();




                if (lstResult != null && lstResult.Count() > 0)
                {
                    lstResult = lstResult.OrderByDescending(s => s.prop_flagstat).ThenByDescending(s => s.createTime).ToList();
                }


                return lstResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }


        public void uploadCouponData(dbEntities db, UserDetailSession userInfo, List<CouponUploadModel> lstData)
        {
            try
            {

                List<T_CustomerReceipt> lstInsertRec = new List<T_CustomerReceipt>();
                List<T_CustomerProduct> lstInsertCustProd = new List<T_CustomerProduct>();
                List<M_Customer> lstInsertCust = new List<M_Customer>();
                List<T_CouponUpload> lstInsertCoupon = new List<T_CouponUpload>();

                if (lstData != null && lstData.Count() > 0)
                {
                    Guid storeGuid = getConstantForStore(db, "COUPON");
                    Guid prodGuid = getConstantForProd(db, "COUPON");

                    List<M_Customer> lstAllCust = (from cust in db.M_Customer
                                                   select cust).ToList();

                    Guid uploadKeyGroup = Guid.NewGuid();

                    foreach (var item in lstData)
                    {
                        // generate log
                        T_CouponUpload dtoCoupon = generateCouponUpload(uploadKeyGroup, item);

                        Guid campGuid = getCampGuidByCampCode(db, item.campaignId.Trim().ToUpper());

                        T_CustomerReceipt dtoRec = new T_CustomerReceipt();
                        Guid custGuid = generateNewCustomer(userInfo, lstInsertCust, lstAllCust, item.firstname, item.lastname, item.tel);

                        dtoRec.custRec_recpNo = item.recNo.ToUpper();
                        bool isExist = false;
                        isExist = checkIsExistRecNo(db, lstInsertRec, dtoRec, isExist);

                        if (isExist)
                        {
                            dtoCoupon.uploadMsg = "ไม่นำรายการเข้าระบบ:: เลขที่ใบเสร็จซ้ำ";
                            lstInsertCoupon.Add(dtoCoupon);
                            continue;
                        }

                        dtoRec = generatePropForCustRec(userInfo, dtoRec, "COUPON.png");
                        dtoRec.custRec_campGuid = campGuid;
                        dtoRec.custRec_custGuid = custGuid;
                        dtoRec.custRec_storeGuid = storeGuid;
                        dtoRec.custRec_createDate = convertUploadOnlyDateFromStringToDate(item.createTime);
                        dtoRec.custRec_updateDate = dtoRec.custRec_createDate;

                        lstInsertRec.Add(dtoRec);

                        T_CustomerProduct dtoProd = generateNewCustProd(userInfo, prodGuid, dtoRec);
                        lstInsertCustProd.Add(dtoProd);

                        dtoCoupon.uploadMsg = "Import Success";
                        lstInsertCoupon.Add(dtoCoupon);
                    }
                }

                db.T_CustomerProduct.AddRange(lstInsertCustProd);
                db.T_CustomerReceipt.AddRange(lstInsertRec);
                db.M_Customer.AddRange(lstInsertCust);
                db.T_CouponUpload.AddRange(lstInsertCoupon);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        private static T_CouponUpload generateCouponUpload(Guid uploadKeyGroup, CouponUploadModel item)
        {
            T_CouponUpload dtoCou = new T_CouponUpload();
            dtoCou.campaignID = item.campaignId;
            dtoCou.createDateTime = item.createTime;
            dtoCou.FirstName = item.firstname;
            dtoCou.LastName = item.lastname;
            dtoCou.RecNo = item.recNo;
            dtoCou.Tel = item.tel;
            dtoCou.keyGroup = uploadKeyGroup;
            return dtoCou;
        }

    }
}