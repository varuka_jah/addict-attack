﻿using AutoMapper;
using ManagementSystem.Data;
using ManagementSystem.Models.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagementSystem.Models.Service
{
    public class PremiumService
    {
        public void create(dbEntities db, UserDetailSession userInfo, PremiumModel model)
        {
            try
            {
                var prem = (from s in db.M_Premium
                            where s.prem_code == model.prem_code.Trim().ToUpper()
                            select s).SingleOrDefault();
                if (prem != null)
                {
                    throw new Exception(ConstUtil.MsgConst.PremiumCodeDuplicate);
                }

                prem = (from s in db.M_Premium
                        where s.prem_name == model.prem_name.Trim()
                        select s).SingleOrDefault();
                if (prem != null)
                {
                    throw new Exception(ConstUtil.MsgConst.PremiumNameDuplicate);
                }


                prem = new M_Premium();
                prem.prem_code = model.prem_code.Trim();
                prem.prem_createBy = userInfo.loginUserDetail.user_guid;
                prem.prem_createDate = DateTime.Now;
                prem.prem_guid = Guid.NewGuid();
                prem.prem_imgUrl = model.prem_imgUrl.Trim();
                prem.prem_name = model.prem_name.Trim();
                prem.prem_pricePerUnit = model.prem_pricePerUnit;
                prem.prem_remark = (model.prop_remark == null ? "" : model.prop_remark.Trim());
                prem.prem_status = ConstUtil.ConstValue.Status.active;
                prem.prem_updateBy = userInfo.loginUserDetail.user_guid;
                prem.prem_updateDate = DateTime.Now;
                db.M_Premium.Add(prem);

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public void update(dbEntities db, UserDetailSession userInfo, PremiumModel model)
        {
            try
            {
                var prem = (from s in db.M_Premium
                            where s.prem_guid == model.prem_guid
                            select s).SingleOrDefault();

                if (prem == null)
                {
                    throw new Exception("ไม่พบรายการที่ต้องการแก้ไขค่ะ");
                }

                var checkDup = (from s in db.M_Premium
                                where s.prem_code == model.prem_code.Trim().ToUpper()
                                    && s.prem_guid != model.prem_guid
                                select s).SingleOrDefault();
                if (checkDup != null)
                {
                    throw new Exception(ConstUtil.MsgConst.ProdCodeDuplicate);
                }

                checkDup = (from s in db.M_Premium
                            where s.prem_name == model.prem_name.Trim()
                                && s.prem_guid != model.prem_guid
                            select s).SingleOrDefault();
                if (checkDup != null)
                {
                    throw new Exception(ConstUtil.MsgConst.ProdNameDuplicate);
                }

                prem.prem_code = model.prem_code.Trim();
                prem.prem_imgUrl = model.prem_imgUrl.Trim();
                prem.prem_name = model.prem_name.Trim();
                prem.prem_pricePerUnit = model.prem_pricePerUnit;
                prem.prem_remark = (model.prop_remark == null ? "" : model.prop_remark.Trim());
                prem.prem_status = model.prop_activeStat;
                prem.prem_updateBy = userInfo.loginUserDetail.user_guid;
                prem.prem_updateDate = DateTime.Now;

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<PremiumModel> searchByCriteria(dbEntities db, PremiumSearchModel model)
        {
            try
            {
                var data = (from s in db.M_Premium
                            join create in db.M_User on s.prem_createBy equals create.user_guid
                            join update in db.M_User on s.prem_updateBy equals update.user_guid
                            select new PremiumModel()
                            {
                                prem_code = s.prem_code,
                                prem_guid = s.prem_guid,
                                prem_imgUrl = s.prem_imgUrl,
                                prem_fullPath = ConstUtil.ConstValue.UploadPath.premium + s.prem_imgUrl,
                                prem_name = s.prem_name,
                                prem_pricePerUnit = s.prem_pricePerUnit,
                                prop_activeStat = s.prem_status,
                                prop_create_by = s.prem_createBy,
                                prop_create_by_name = create.user_name,
                                prop_create_date_dt = s.prem_createDate,
                                prop_remark = s.prem_remark,
                                prop_update_by = s.prem_updateBy,
                                prop_update_by_name = update.user_name,
                                prop_update_date_dt = s.prem_updateDate,
                            });
                if (model.prem_code != null && !model.prem_code.Trim().Equals(""))
                {
                    data = data.Where(s => s.prem_code.Contains(model.prem_code.Trim()));
                }

                if (model.prem_name != null && !model.prem_name.Trim().Equals(""))
                {
                    data = data.Where(s => s.prem_name.Contains(model.prem_name.Trim()));
                }

                if (model.prem_guid != null && !model.prem_guid.Equals(Guid.Empty))
                {
                    data = data.Where(s => s.prem_guid.Equals(model.prem_guid));
                }

                #region Find by checkbox => Status
                List<String> lstStat = new List<string>();
                if (model.status_act)
                {
                    lstStat.Add(ConstUtil.ConstValue.Status.active);
                }
                if (model.status_ina)
                {
                    lstStat.Add(ConstUtil.ConstValue.Status.inactive);
                }

                if (lstStat.Count() > 0)
                {
                    data = data.Where(s => lstStat.Contains(s.prop_activeStat));
                }
                #endregion

                List<PremiumModel> lstResult = data.OrderBy(s => s.prem_code).ToList<PremiumModel>();

                lstResult = Mapper.Map<List<PremiumModel>, List<PremiumModel>>(lstResult);

                return lstResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

    }
}