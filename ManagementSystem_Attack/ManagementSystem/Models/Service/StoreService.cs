﻿using AutoMapper;
using ManagementSystem.Data;
using ManagementSystem.Models.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagementSystem.Models.Service
{
    public class StoreService
    {
        public void create(dbEntities db, UserDetailSession userInfo, StoreModel model)
        {
            try
            {

                var store = (from s in db.M_Store
                        where s.store_name == model.store_name.Trim()
                        select s).SingleOrDefault();
                if (store != null)
                {
                    throw new Exception(ConstUtil.MsgConst.StoreNameDuplicate);
                }


                store = new M_Store();
                store.store_createBy = userInfo.loginUserDetail.user_guid;
                store.store_createDate = DateTime.Now;
                store.store_guid = Guid.NewGuid();
                store.store_logoUrl = model.store_imgUrl.Trim();
                store.store_name = model.store_name.Trim();
                store.store_remark = (model.prop_remark == null ? "" : model.prop_remark.Trim());
                store.store_status = ConstUtil.ConstValue.Status.active;
                store.store_updateBy = userInfo.loginUserDetail.user_guid;
                store.store_updateDate = DateTime.Now;
                db.M_Store.Add(store);

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public void update(dbEntities db, UserDetailSession userInfo, StoreModel model)
        {
            try
            {
                var store = (from s in db.M_Store
                            where s.store_guid == model.store_guid
                            select s).SingleOrDefault();

                if (store == null)
                {
                    throw new Exception("ไม่พบรายการที่ต้องการแก้ไขค่ะ");
                }


                var checkDup = (from s in db.M_Store
                            where s.store_name == model.store_name.Trim()
                                && s.store_guid != model.store_guid
                            select s).SingleOrDefault();
                if (checkDup != null)
                {
                    throw new Exception(ConstUtil.MsgConst.StoreNameDuplicate);
                }
                
                store.store_logoUrl = model.store_imgUrl.Trim();
                store.store_name = model.store_name.Trim();
                store.store_remark = (model.prop_remark == null ? "" : model.prop_remark.Trim());
                store.store_status = model.prop_activeStat;
                store.store_updateBy = userInfo.loginUserDetail.user_guid;
                store.store_updateDate = DateTime.Now;

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<StoreModel> searchByCriteria(dbEntities db, StoreSearchModel model)
        {
            try
            {
                var data = (from s in db.M_Store
                            join c in db.M_StoreCategory on s.store_cateGuid equals c.storeCate_guid
                            join create in db.M_User on s.store_createBy equals create.user_guid
                            join update in db.M_User on s.store_updateBy equals update.user_guid
                            select new StoreModel()
                            {
                                store_guid = s.store_guid,
                                store_imgUrl = s.store_logoUrl,
                                store_fullPath = ConstUtil.ConstValue.UploadPath.store + s.store_logoUrl,
                                store_name = s.store_name,
                                prop_activeStat = s.store_status,
                                prop_create_by = s.store_createBy,
                                prop_create_by_name = create.user_name,
                                prop_create_date_dt = (DateTime)s.store_createDate,
                                prop_remark = s.store_remark,
                                prop_update_by = s.store_updateBy,
                                prop_update_by_name = update.user_name,
                                prop_update_date_dt = (DateTime)s.store_updateDate,
                                storeCate_guid = c.storeCate_guid,
                                storeCate_name = c.storeCate_name
                            });
               

                if (model.store_name != null && !model.store_name.Trim().Equals(""))
                {
                    data = data.Where(s => s.store_name.Contains(model.store_name.Trim()));
                }

                if (model.store_guid != null && !model.store_guid.Equals(Guid.Empty))
                {
                    data = data.Where(s => s.store_guid.Equals(model.store_guid));
                }

                #region Find by checkbox => Status
                List<String> lstStat = new List<string>();
                if (model.status_act)
                {
                    lstStat.Add(ConstUtil.ConstValue.Status.active);
                }
                if (model.status_ina)
                {
                    lstStat.Add(ConstUtil.ConstValue.Status.inactive);
                }

                if (lstStat.Count() > 0)
                {
                    data = data.Where(s => lstStat.Contains(s.prop_activeStat));
                }
                #endregion

                List<StoreModel> lstResult = data.OrderBy(s => s.store_name).ToList<StoreModel>();

                lstResult = Mapper.Map<List<StoreModel>, List<StoreModel>>(lstResult);

                return lstResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

    }
}