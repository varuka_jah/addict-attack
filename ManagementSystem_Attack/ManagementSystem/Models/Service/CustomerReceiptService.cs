﻿using ManagementSystem.ConstUtil;
using ManagementSystem.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using static ManagementSystem.ConstUtil.ConstValue;

namespace ManagementSystem.Models.Service
{
    public class CustomerReceiptService
    {
        public List<CustomerReceiptModel> getCustReceiptByCamp(dbEntities db, CampReportSearchModel model, out int filterRecords)
        {
            try
            {
                var data = (from cr in db.T_CustomerReceipt
                            join cust in db.M_Customer on cr.custRec_custGuid equals cust.cust_guid
                                  join cmp in db.T_Campaign on cr.custRec_campGuid equals cmp.camp_guid
                                  join store in db.M_Store on cr.custRec_storeGuid equals store.store_guid
                                  where cr.custRec_campGuid.Equals(model.camp_guid)
                                  && cr.custRec_status.Equals(model.custRec_status)
                                  select new CustomerReceiptModel()
                                  {
                                      cust_guid = cust.cust_guid, 
                                      cust_firstName = cust.cust_firstName, 
                                      cust_lastName = cust.cust_lastName, 
                                      cust_fullName = cust.cust_firstName + " " + cust.cust_lastName, 
                                      cust_tel = cust.cust_tel , 
                                      custRec_guid = cr.custRec_guid,
                                      custRec_branchName = cr.custRec_branchName,
                                      custRec_recpImgUrl = ConstUtil.ConstValue.UploadPath.receipt + cr.custRec_recpImgUrl,
                                      custRec_recpNo = cr.custRec_recpNo,
                                      custRec_totalGainPoint = cr.custRec_totalGainPoint,
                                      custRec_gameResult = cr.custRec_gameResult,
                                      custRec_status = cr.custRec_status,
                                      custRec_remark = cr.custRec_remark,
                                      custRec_createBy = cr.custRec_createBy,
                                      custRec_createDate = cr.custRec_createDate,
                                      custRec_updateBy = cr.custRec_updateBy,
                                      custRec_updateDate = cr.custRec_updateDate,
                                      custRec_campGuid = cr.custRec_campGuid,
                                      camp_code = cmp.camp_code,
                                      camp_desc = cmp.camp_desc,
                                      camp_endDate_dt = cmp.camp_endDate,
                                      camp_guid = cmp.camp_guid,
                                      camp_name = cmp.camp_name,
                                      camp_startDate_dt = cmp.camp_startDate,
                                      custRec_storeGuid = cr.custRec_storeGuid,
                                      store_guid = store.store_guid,
                                      store_name = store.store_name,
                                      custRec_imgUrl = cr.custRec_recpImgUrl,
                                      custRec_custGuid = cr.custRec_custGuid
                                  });//.OrderByDescending(s => s.custRec_createDate).ToList<CustomerReceiptModel>();

                if (model.custRec_gameResult != null && !model.custRec_gameResult.Trim().Equals(""))
                {
                    data = data.Where(s => s.custRec_gameResult.Equals(model.custRec_gameResult.Trim()));
                }
                List<CustomerReceiptModel> lstReceipt = data.OrderByDescending(s => s.custRec_createDate).Skip(model.start).Take(model.length).ToList<CustomerReceiptModel>();
                filterRecords = data.Count();

                if (lstReceipt != null && lstReceipt.Count() > 0)
                {
                    foreach (var item in lstReceipt)
                    {
                        item.custRec_fullPath = ConstUtil.ConstValue.UploadPath.receipt + item.custRec_imgUrl;

                        item.custRec_updateDate_str = item.custRec_updateDate.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        item.custRec_createDate_str = item.custRec_createDate.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        item.custRec_gameResult_disp = (item.custRec_gameResult.Equals(ConstValue.Status.gameResult_win) ? ConstValue.Status.gameResult_win_disp :
                                                        item.custRec_gameResult.Equals(ConstValue.Status.gameResult_loss) ? ConstValue.Status.gameResult_loss_disp : ConstValue.Status.gameResult_none_disp);

                        item.prop_activeStat_disp = (item.custRec_status.Equals(ConstValue.Status.success) ? ConstValue.Status.rec_success_disp :
                            item.custRec_status.Equals(ConstValue.Status.init) ? ConstValue.Status.rec_init_disp :
                            item.custRec_status.Equals(ConstValue.Status.reserved) ? ConstValue.Status.rec_reserv_disp :
                            item.custRec_status.Equals(ConstValue.Status.active) ? ConstValue.Status.rec_act_disp :
                            item.custRec_status.Equals(ConstValue.Status.reject) ? ConstValue.Status.rec_rej_disp : ConstValue.Status.rec_oth_disp);


                        if ((item.custRec_status.Equals(ConstValue.Status.active) || item.custRec_status.Equals(ConstValue.Status.success))
                            && item.custRec_gameResult.Equals(ConstValue.Status.gameResult_win))
                        {
                            var custPrem = (from custP in db.T_CustomerPremium
                                            join pre in db.M_Premium on custP.custPrem_premGuid equals pre.prem_guid
                                            where custP.custPrem_custRecGuid.Equals(item.custRec_guid)
                                            select pre).SingleOrDefault();

                            item.custPrem_fullPath = ConstUtil.ConstValue.UploadPath.premium + custPrem.prem_imgUrl;
                            item.custPrem_name = custPrem.prem_name;
                            item.custPrem_premGuid = custPrem.prem_guid;
                            item.custPrem_textNameDisp = "";

                            CustomerService custServ = new CustomerService();
                            item.dtoCustomer = custServ.getCustomerModelInfoByID(db, item.custRec_custGuid);

                        }
                        else
                        {
                            item.custPrem_fullPath = "";
                            item.custPrem_guid = Guid.Empty;
                            item.custPrem_name = "";
                            item.custPrem_premGuid = Guid.Empty;
                            item.custPrem_textNameDisp = "";
                        }
                    }
                }

                return lstReceipt;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }


        public List<CustomerReceiptModel> getCustomerReceiptHistory(dbEntities db, Guid custGuid)
        {
            try
            {
                var lstReceipt = (from cr in db.T_CustomerReceipt
                                  join cmp in db.T_Campaign on cr.custRec_campGuid equals cmp.camp_guid
                                  join store in db.M_Store on cr.custRec_storeGuid equals store.store_guid
                                  where cr.custRec_custGuid.Equals(custGuid)
                                  select new CustomerReceiptModel()
                                  {
                                      custRec_guid = cr.custRec_guid,
                                      custRec_branchName = cr.custRec_branchName,
                                      custRec_recpImgUrl = ConstUtil.ConstValue.UploadPath.receipt + cr.custRec_recpImgUrl,
                                      custRec_recpNo = cr.custRec_recpNo,
                                      custRec_totalGainPoint = cr.custRec_totalGainPoint,
                                      custRec_gameResult = cr.custRec_gameResult,
                                      custRec_status = cr.custRec_status,
                                      custRec_remark = cr.custRec_remark,
                                      custRec_createBy = cr.custRec_createBy,
                                      custRec_createDate = cr.custRec_createDate,
                                      custRec_updateBy = cr.custRec_updateBy,
                                      custRec_updateDate = cr.custRec_updateDate,
                                      custRec_campGuid = cr.custRec_campGuid,
                                      camp_code = cmp.camp_code,
                                      camp_desc = cmp.camp_desc,
                                      camp_endDate_dt = cmp.camp_endDate,
                                      camp_guid = cmp.camp_guid,
                                      camp_name = cmp.camp_name,
                                      camp_startDate_dt = cmp.camp_startDate,
                                      custRec_storeGuid = cr.custRec_storeGuid,
                                      store_guid = store.store_guid,
                                      store_name = store.store_name,
                                      custRec_imgUrl = cr.custRec_recpImgUrl,
                                      custRec_custGuid = cr.custRec_custGuid
                                  }).OrderByDescending(s => s.custRec_createDate).ToList<CustomerReceiptModel>();

                if (lstReceipt != null && lstReceipt.Count() > 0)
                {
                    foreach (var item in lstReceipt)
                    {
                        item.custRec_fullPath = ConstUtil.ConstValue.UploadPath.receipt + item.custRec_imgUrl;
                      
                        item.custRec_updateDate_str = item.custRec_updateDate.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        item.custRec_createDate_str = item.custRec_createDate.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        item.custRec_gameResult_disp = (item.custRec_gameResult.Equals(ConstValue.Status.gameResult_win) ? ConstValue.Status.gameResult_win_disp :
                                                        item.custRec_gameResult.Equals(ConstValue.Status.gameResult_loss) ? ConstValue.Status.gameResult_loss_disp : ConstValue.Status.gameResult_none_disp);

                        item.prop_activeStat_disp = (item.custRec_status.Equals(ConstValue.Status.success) ? ConstValue.Status.rec_success_disp : 
                            item.custRec_status.Equals(ConstValue.Status.init) ? ConstValue.Status.rec_init_disp :
                            item.custRec_status.Equals(ConstValue.Status.reserved) ? ConstValue.Status.rec_reserv_disp :
                            item.custRec_status.Equals(ConstValue.Status.active) ? ConstValue.Status.rec_act_disp : ConstValue.Status.rec_oth_disp);


                        if ((item.custRec_status.Equals(ConstValue.Status.active) || item.custRec_status.Equals(ConstValue.Status.success))
                            && item.custRec_gameResult.Equals(ConstValue.Status.gameResult_win))
                        {
                            var custPrem = (from custP in db.T_CustomerPremium
                                            join pre in db.M_Premium on custP.custPrem_premGuid equals pre.prem_guid
                                            where custP.custPrem_custRecGuid.Equals(item.custRec_guid)
                                            select pre).SingleOrDefault();

                            item.custPrem_fullPath = ConstUtil.ConstValue.UploadPath.premium + custPrem.prem_imgUrl;
                            item.custPrem_name = custPrem.prem_name;
                            item.custPrem_premGuid = custPrem.prem_guid;
                            item.custPrem_textNameDisp = "";

                            CustomerService custServ = new CustomerService();
                            item.dtoCustomer = custServ.getCustomerModelInfoByID(db, item.custRec_custGuid);

                        }
                        else
                        {
                            item.custPrem_fullPath = "";
                            item.custPrem_guid = Guid.Empty;
                            item.custPrem_name = "";
                            item.custPrem_premGuid = Guid.Empty;
                            item.custPrem_textNameDisp = "";
                        }
                    }
                }

                return lstReceipt;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public void deleteReceipt(dbEntities db, Guid custRecGuid)
        {
            try
            {
                var custRec = (from rec in db.T_CustomerReceipt
                               where rec.custRec_guid.Equals(custRecGuid)
                               select rec).Single();

                if (custRec.custRec_gameResult.Equals(ConstValue.Status.gameResult_win))
                {
                    var custPre = (from pre in db.T_CustomerPremium
                                   where pre.custPrem_custRecGuid.Equals(custRec.custRec_guid)
                                   select pre).SingleOrDefault();

                    var campPre = (from cp in db.T_CampaignPremium
                                   where cp.campPrem_campGuid.Equals(custRec.custRec_campGuid)
                                   && cp.campPrem_premGuid.Equals(custPre.custPrem_premGuid)
                                   select cp).SingleOrDefault();
                    campPre.campPrem_remaining = campPre.campPrem_remaining + 1;
                    db.T_CustomerPremium.Remove(custPre);

                    var dist = (from ds in db.T_DistributePremium
                                where ds.distPrem_campGuid.Equals(custRec.custRec_campGuid)
                                && ds.distPrem_date.CompareTo(DateTime.Today) == 0
                                && ds.distPrem_status.Equals(Status.active)
                                select ds).SingleOrDefault();
                    if (dist != null)
                    {
                        if (dist.distPrem_remaining + 1 <= dist.distPrem_outstanding)
                        {
                            dist.distPrem_remaining = dist.distPrem_remaining + 1;
                            dist.distPrem_updateDate = DateTime.Now;
                        }
                    }
                }
                
                db.T_CustomerProduct.RemoveRange(db.T_CustomerProduct.Where(s => s.custProd_custRecGuid.Equals(custRec.custRec_guid)).ToList());
                db.T_CustomerReceipt.Remove(custRec);
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public Guid updateCustomerReceiptStatus(dbEntities db, Guid custRecGuid, String newStatus, String custRecNo)
        {
            try
            {
                var custRec = (from rec in db.T_CustomerReceipt
                               where rec.custRec_guid.Equals(custRecGuid)
                               select rec).Single();

                custRec.custRec_recpNo = custRecNo.ToUpper().Trim();
                custRec.custRec_status = newStatus;
                db.SaveChanges();

                return custRec.custRec_custGuid;
            } catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
    }
}