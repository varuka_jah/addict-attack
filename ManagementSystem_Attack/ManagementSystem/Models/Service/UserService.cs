﻿using ManagementSystem.ConstUtil;
using ManagementSystem.Data;
using ManagementSystem.Models.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagementSystem.Models.Service
{
    public class UserService
    {
        public UserDetailSession getUserLogin(UserLoginModel dtoUserLogin)
        {

            try
            {
                using (dbEntities ctx = new dbEntities())
                {
                    String passEn = ConstUtil.ConvertUtil.GetMyEncryptPassword(dtoUserLogin.Password.Trim());
                    ctx.Database.Connection.Open();

                    M_User dto = (from c in ctx.M_User
                                  where c.user_userName == dtoUserLogin.UserName
                                           && c.user_password == passEn
                                           && c.user_status == ConstUtil.ConstValue.Status.active
                                         select c).SingleOrDefault<M_User>();
                    if (dto == null)
                    {
                        throw new Exception(MsgConst.userNotFound);
                    }


                    UserDetailSession userInfo = new UserDetailSession(dto, getUserRoleByRoleID(dto.user_roleGuid));
                    return userInfo;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public M_Role getUserRoleByRoleID(Guid userRoleGUID)
        {
            try
            {
                using (dbEntities ctx = new dbEntities())
                {
                    M_Role dto = (from c in ctx.M_Role
                                  where c.roleGuid == userRoleGUID
                                     && c.roleStatus == ConstUtil.ConstValue.Status.active
                                  select c).SingleOrDefault<M_Role>();
                    return dto;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public M_User getUserInfoByID(Guid userGUID)
        {
            try
            {
                using (dbEntities ctx = new dbEntities())
                {
                    M_User dto = (from us in ctx.M_User
                                  join ro in ctx.M_Role on us.user_roleGuid equals ro.roleGuid
                                  where us.user_guid == userGUID
                                  select us).SingleOrDefault();

                    return dto;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
    }
}