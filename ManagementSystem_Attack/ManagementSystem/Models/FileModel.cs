﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagementSystem.Models
{
    public class FileModel
    {
        public class FileAttachModel
        {
            public HttpPostedFileBase ko_file { get; set; }
            public String ko_barHTML { get; set; }
            public String ko_filesprogress { get; set; }
            public String ko_showPrev { get; set; }
            public String ko_desc { get; set; }
            public String ko_fileSize { get; set; }
            public String ko_src { get; set; }
            public String ko_fullDirectory { get; set; }
            public String ko_fileName { get; set; }
        }
    }
}