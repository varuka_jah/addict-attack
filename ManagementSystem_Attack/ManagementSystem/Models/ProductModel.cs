﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ManagementSystem.Models.FileModel;

namespace ManagementSystem.Models
{
    public class ProductSearchModel
    {
        public Guid prod_guid { get; set; }
        public String prod_name { get; set; }
        public String prod_code { get; set; }
        public bool status_act { get; set; }
        public bool status_ina { get; set; }
    }

    public class ProductActionModel
    {
        public ProductSearchModel dtoCriteria { get; set; }
        public ProductModel dtoFocus { get; set; }
        public List<ProductModel> lstResult { get; set; }
    }

    public class ProductModel
    {
        public FileAttachModel prod_img { get; set; }
        public Guid prod_guid { get; set; }
        public String prod_name { get; set; }
        public String prod_code { get; set; }
        public Decimal prod_pricePerUnit { get; set; }
        public String prod_imgUrl { get; set; }
        public String prod_fullPath { get; set; }
        #region Common Property
        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }
      
        public String prop_remark { get; set; }

        public DateTime prop_create_date_dt { get; set; }
        public String prop_create_date_str { get; set; }

        public Guid prop_create_by { get; set; }
        public String prop_create_by_name { get; set; }

        public DateTime prop_update_date_dt { get; set; }
        public String prop_update_date_str { get; set; }

        public Guid prop_update_by { get; set; }
        public String prop_update_by_name { get; set; }

        public String prop_errorMsg { get; set; }
        #endregion
    }
}