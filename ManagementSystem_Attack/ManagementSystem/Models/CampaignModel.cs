﻿using ManagementSystem.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ManagementSystem.Models.FileModel;

namespace ManagementSystem.Models
{
    public class CampaignSearchModel
    {
        public Guid camp_guid { get; set; }
        public String camp_code { get; set; }
        public String camp_name { get; set; }
        public DateTime camp_startDate_dt { get; set; }
        public String camp_startDate_str { get; set; }
        public DateTime camp_endDate_dt { get; set; }
        public String camp_endDate_str { get; set; }

        public bool status_act { get; set; }
        public bool status_ina { get; set; }
    }

    public class CampaignSearchResultModel
    {
        public Guid camp_guid { get; set; }
        public String camp_code { get; set; }
        public String camp_name { get; set; }
        public String camp_desc { get; set; }
        public DateTime camp_startDate_dt { get; set; }
        public String camp_startDate_str { get; set; }
        public DateTime camp_endDate_dt { get; set; }
        public String camp_endDate_str { get; set; }
        public decimal camp_budgetAmt { get; set; }
        public decimal camp_receiptMinAmt { get; set; }
        public String camp_imgUrl { get; set; }
        public String camp_fullPath { get; set; }

        #region Common Property
        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }

        public String prop_remark { get; set; }

        public DateTime prop_create_date_dt { get; set; }
        public String prop_create_date_str { get; set; }

        public Guid prop_create_by { get; set; }
        public String prop_create_by_name { get; set; }

        public DateTime prop_update_date_dt { get; set; }
        public String prop_update_date_str { get; set; }

        public Guid prop_update_by { get; set; }
        public String prop_update_by_name { get; set; }

        public String prop_errorMsg { get; set; }
        #endregion
    }

    public class CampaignActionModel
    {
        public CampaignSearchModel dtoCriteria { get; set; }
        public CampaignSearchResultModel dtoSearchResult { get; set; }
        public List<CampaignSearchResultModel> lstSearchResult { get; set; }

        public CampaignModel dtoFocus { get; set; }
        public List<CampaignModel> lstResult { get; set; }
        public List<SelectListItem> lstSel_Prod { get; set; }
        public List<SelectListItem> lstSel_Prem { get; set; }
    }

    public class CampaignModel
    {
        public FileAttachModel camp_img { get; set; }
        public List<CampaignProductModel> lstProduct { get; set; }
        public List<CampaignPremiumModel> lstPremium { get; set; }
        public CampaignProductModel dtoProdFocus { get; set; }
        public CampaignPremiumModel dtoPremFocus { get; set; }

        public Guid camp_guid { get; set; }
        public String camp_code { get; set; }
        public String camp_name { get; set; }
        public String camp_desc { get; set; }
        public DateTime camp_startDate_dt { get; set; }
        public String camp_startDate_str { get; set; }
        public DateTime camp_endDate_dt { get; set; }
        public String camp_endDate_str { get; set; }
        public decimal camp_budgetAmt { get; set; }
        public decimal camp_receiptMinAmt { get; set; }
        public String camp_imgUrl { get; set; }
        public String camp_fullPath { get; set; }

        #region Common Property
        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }

        public String prop_remark { get; set; }

        public DateTime prop_create_date_dt { get; set; }
        public String prop_create_date_str { get; set; }

        public Guid prop_create_by { get; set; }
        public String prop_create_by_name { get; set; }

        public DateTime prop_update_date_dt { get; set; }
        public String prop_update_date_str { get; set; }

        public Guid prop_update_by { get; set; }
        public String prop_update_by_name { get; set; }

        public String prop_errorMsg { get; set; }
        #endregion
    }

    public class CampaignProductModel
    {
        public bool isUpdate { get; set; }
        public Guid camp_guid { get; set; }
        public Guid campProd_guid { get; set; }
        public FileAttachModel prod_img { get; set; }
        public Guid prod_guid { get; set; }
        public String prod_name { get; set; }
        public String prod_code { get; set; }
        public Decimal prod_pricePerUnit { get; set; }
        public String prod_imgUrl { get; set; }
        public String prod_fullPath { get; set; }
        public double prod_point { get; set; }

        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }
    }
    public class CampaignPremiumModel
    {
        public bool isUpdate { get; set; }
        public Guid camp_guid { get; set; }
        public Guid campPrem_guid { get; set; }
        public FileAttachModel prem_img { get; set; }
        public Guid prem_guid { get; set; }
        public String prem_name { get; set; }
        public String prem_code { get; set; }
        public Decimal prem_pricePerUnit { get; set; }
        public int prem_outstandingQty { get; set; }
        public int prem_remainingQty { get; set; }
        public String prem_imgUrl { get; set; }
        public String prem_fullPath { get; set; }
        public DateTime prem_startDate_dt { get; set; }
        public String prem_startDate_str { get; set; }
        public DateTime prem_endDate_dt { get; set; }
        public String prem_endDate_str { get; set; }

        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }
    }

  
}