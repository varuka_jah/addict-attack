﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ManagementSystem.Models.FileModel;

namespace ManagementSystem.Models
{
    public class StoreSearchModel
    {
        public Guid store_guid { get; set; }
        public String store_name { get; set; }
        public bool status_act { get; set; }
        public bool status_ina { get; set; }
    }

    public class StoreActionModel
    {
        public StoreSearchModel dtoCriteria { get; set; }
        public StoreModel dtoFocus { get; set; }
        public List<StoreModel> lstResult { get; set; }
    }

    public class StoreModel
    {
        public FileAttachModel store_img { get; set; }
        public Guid store_guid { get; set; }
        public String store_name { get; set; }
        public String store_imgUrl { get; set; }
        public String store_fullPath { get; set; }

        public Guid storeCate_guid { get; set; }
        public String storeCate_name { get; set; }

        #region Common Property
        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }

        public String prop_remark { get; set; }

        public DateTime prop_create_date_dt { get; set; }
        public String prop_create_date_str { get; set; }

        public Guid prop_create_by { get; set; }
        public String prop_create_by_name { get; set; }

        public DateTime prop_update_date_dt { get; set; }
        public String prop_update_date_str { get; set; }

        public Guid prop_update_by { get; set; }
        public String prop_update_by_name { get; set; }

        public String prop_errorMsg { get; set; }
        #endregion
    }
}