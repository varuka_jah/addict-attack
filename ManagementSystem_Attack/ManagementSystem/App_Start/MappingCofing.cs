﻿using AutoMapper;
using ManagementSystem.Models;
using System.Globalization;

namespace ManagementSystem.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            
            CreateMap<CampaignPremiumModel, CampaignPremiumModel>()
              .ForMember(dest => dest.prem_startDate_str, opt => opt.MapFrom(src => src.prem_startDate_dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)))
              .ForMember(dest => dest.prem_endDate_str, opt => opt.MapFrom(src => src.prem_endDate_dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)));

            CreateMap<CampaignModel, CampaignModel>()
              .ForMember(dest => dest.camp_startDate_str, opt => opt.MapFrom(src => src.camp_startDate_dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)))
              .ForMember(dest => dest.camp_endDate_str, opt => opt.MapFrom(src => src.camp_endDate_dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)));

            CreateMap<CampaignSearchResultModel, CampaignSearchResultModel>()
              .ForMember(dest => dest.camp_startDate_str, opt => opt.MapFrom(src => src.camp_startDate_dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)))
              .ForMember(dest => dest.camp_endDate_str, opt => opt.MapFrom(src => src.camp_endDate_dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)))
              .ForMember(dest => dest.prop_create_date_str, opt => opt.MapFrom(src => src.prop_create_date_dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)))
              .ForMember(dest => dest.prop_update_date_str, opt => opt.MapFrom(src => src.prop_update_date_dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)))
              .ForMember(dest => dest.prop_activeStat_disp, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.active) ? ConstUtil.ConstValue.Status.active_disp : ConstUtil.ConstValue.Status.inactive_disp))
              .ForMember(dest => dest.prop_activeStat_act, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.active) ? true : false))
              .ForMember(dest => dest.prop_activeStat_ina, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.inactive) ? true : false));





            CreateMap<UserModel, UserModel>()
          .ForMember(dest => dest.prop_create_date_str, opt => opt.MapFrom(src => src.prop_create_date_dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)))
          .ForMember(dest => dest.prop_update_date_str, opt => opt.MapFrom(src => src.prop_update_date_dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)))
          .ForMember(dest => dest.prop_activeStat_disp, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.active) ? ConstUtil.ConstValue.Status.active_disp : ConstUtil.ConstValue.Status.inactive_disp))
          .ForMember(dest => dest.prop_activeStat_act, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.active) ? true : false))
          .ForMember(dest => dest.prop_activeStat_ina, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.inactive) ? true : false));


            CreateMap<RoleModel, RoleModel>()
              .ForMember(dest => dest.prop_create_date_str, opt => opt.MapFrom(src => src.prop_create_date_dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)))
              .ForMember(dest => dest.prop_update_date_str, opt => opt.MapFrom(src => src.prop_update_date_dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)))
              .ForMember(dest => dest.prop_activeStat_disp, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.active) ? ConstUtil.ConstValue.Status.active_disp : ConstUtil.ConstValue.Status.inactive_disp))
              .ForMember(dest => dest.prop_activeStat_act, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.active) ? true : false))
              .ForMember(dest => dest.prop_activeStat_ina, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.inactive) ? true : false));


            CreateMap<CustomerModel, CustomerModel>()
              .ForMember(dest => dest.prop_create_date_str, opt => opt.MapFrom(src => src.prop_create_date_dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)))
              .ForMember(dest => dest.prop_update_date_str, opt => opt.MapFrom(src => src.prop_update_date_dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)))
              .ForMember(dest => dest.prop_activeStat_disp, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.active) ? ConstUtil.ConstValue.Status.active_disp : ConstUtil.ConstValue.Status.inactive_disp))
              .ForMember(dest => dest.prop_activeStat_act, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.active) ? true : false))
              .ForMember(dest => dest.prop_activeStat_ina, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.inactive) ? true : false));


            CreateMap<ProductModel, ProductModel>()
              .ForMember(dest => dest.prop_create_date_str, opt => opt.MapFrom(src => src.prop_create_date_dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)))
              .ForMember(dest => dest.prop_update_date_str, opt => opt.MapFrom(src => src.prop_update_date_dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)))
              .ForMember(dest => dest.prop_activeStat_disp, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.active) ? ConstUtil.ConstValue.Status.active_disp : ConstUtil.ConstValue.Status.inactive_disp))
              .ForMember(dest => dest.prop_activeStat_act, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.active) ? true : false))
              .ForMember(dest => dest.prop_activeStat_ina, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.inactive) ? true : false));

            CreateMap<PremiumModel, PremiumModel>()
            .ForMember(dest => dest.prop_create_date_str, opt => opt.MapFrom(src => src.prop_create_date_dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)))
            .ForMember(dest => dest.prop_update_date_str, opt => opt.MapFrom(src => src.prop_update_date_dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)))
            .ForMember(dest => dest.prop_activeStat_disp, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.active) ? ConstUtil.ConstValue.Status.active_disp : ConstUtil.ConstValue.Status.inactive_disp))
            .ForMember(dest => dest.prop_activeStat_act, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.active) ? true : false))
            .ForMember(dest => dest.prop_activeStat_ina, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.inactive) ? true : false));

            CreateMap<StoreModel, StoreModel>()
           .ForMember(dest => dest.prop_create_date_str, opt => opt.MapFrom(src => src.prop_create_date_dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)))
           .ForMember(dest => dest.prop_update_date_str, opt => opt.MapFrom(src => src.prop_update_date_dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)))
           .ForMember(dest => dest.prop_activeStat_disp, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.active) ? ConstUtil.ConstValue.Status.active_disp : ConstUtil.ConstValue.Status.inactive_disp))
           .ForMember(dest => dest.prop_activeStat_act, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.active) ? true : false))
           .ForMember(dest => dest.prop_activeStat_ina, opt => opt.MapFrom(src => src.prop_activeStat.Equals(ConstUtil.ConstValue.Status.inactive) ? true : false));
        }
    }

}