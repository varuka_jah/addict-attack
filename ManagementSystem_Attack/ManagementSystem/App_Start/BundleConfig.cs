﻿using System.Web;
using System.Web.Optimization;

namespace ManagementSystem
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                      "~/vendor/jquery/dist/jquery.min.js",
                      "~/vendor/slimScroll/jquery.slimscroll.min.js",
                      "~/vendor/bootstrap/dist/js/bootstrap.min.js",
                      "~/vendor/metisMenu/dist/metisMenu.min.js",
                         "~/Scripts/fileinput.min.js",
                      "~/vendor/iCheck/icheck.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/hommer/js").Include(
                      "~/scripts/homer.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/vendor/fontawesome/css/font-awesome.css",
                "~/vendor/metisMenu/dist/metisMenu.css",
                "~/vendor/animate.css/animate.css",
               "~/Content/bootstrap-fileinput/css/fileinput.min.css",
                "~/vendor/bootstrap/dist/css/bootstrap.css",
                "~/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css",
                "~/fonts/pe-icon-7-stroke/css/helper.css",
                "~/Content/Homer/style.css"));

            // Datatable
            bundles.Add(new ScriptBundle("~/bundles/datatable/js").Include(
                     "~/vendor/datatables/media/js/jquery.dataTables.min.js",
                     "~/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js"));

              bundles.Add(new StyleBundle("~/bundles/datatable/css").Include(
                "~/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css"));

            // Sweetalert
            bundles.Add(new ScriptBundle("~/bundles/sweetAlert/js").Include(
                "~/vendor/SweetAlert2/promise.min.js",
                "~/vendor/SweetAlert2/sweetalert2.js"));

            bundles.Add(new StyleBundle("~/bundles/sweetAlert/css").Include(
                "~/vendor/SweetAlert2/sweetalert.scss"));

            // Ladda
            bundles.Add(new ScriptBundle("~/bundles/ladda/js").Include(
                "~/vendor/ladda/dist/spin.min.js",
                "~/vendor/ladda/dist/ladda.min.js",
                "~/vendor/ladda/dist/ladda.jquery.min.js"));

            bundles.Add(new StyleBundle("~/bundles/ladda/css").Include(
            "~/vendor/ladda/dist/ladda-themeless.min.css"));


            // Select2
            bundles.Add(new ScriptBundle("~/bundles/select2/js").Include(
                "~/vendor/select2-3.5.2/select2.min.js"));

            bundles.Add(new StyleBundle("~/bundles/select2/css").Include(
                "~/vendor/select2-3.5.2/select2.css",
                "~/vendor/select2-bootstrap/select2-bootstrap.css"
                
                ));

            // Touch Spin
            bundles.Add(new ScriptBundle("~/bundles/touchSpin/js").Include(
                 "~/vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"));

            bundles.Add(new StyleBundle("~/bundles/touchSpin/css").Include(
                "~/vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css"));

            // date picker
            bundles.Add(new ScriptBundle("~/bundles/datePicker/js").Include(
                "~/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"));

            bundles.Add(new StyleBundle("~/bundles/datePicker/css").Include(
                "~/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css"));

            // awesome checkbox
            bundles.Add(new StyleBundle("~/bundles/checkbox/css").Include(
                "~/vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"));

            // knockout
            bundles.Add(new ScriptBundle("~/bundles/knockout/js").Include(
                "~/Scripts/knockout-3.4.2.js",
                "~/Scripts/knockout.mapping-latest.js",
                "~/Scripts/knockout.validation.js",
                "~/Scripts/Application/ko.binding.valueNumber.js"));

            // file upload
            bundles.Add(new StyleBundle("~/bundles/bluimp/css").Include(
                "~/vendor/blueimp/gallery/css/blueimp-gallery.min.css",
                "~/vendor/blueimp/fileupload/css/jquery.fileupload.css",
                "~/vendor/blueimp/fileupload/css/jquery.fileupload-ui.css"));

            bundles.Add(new ScriptBundle("~/bundles/bluimp/js").Include(
                "~/Scripts/bluimp/vendor/jquery.ui.widget.js",
                "~/Scripts/bluimp/tmpl.js",
                "~/Scripts/bluimp/load-image.js",
                "~/Scripts/bluimp/canvas-to-blob.js",
                "~/Scripts/bluimp/jquery.iframe-transport.js",
                "~/Scripts/bluimp/jquery.fileupload.js",
                "~/Scripts/bluimp/jquery.fileupload-fp.js",
                "~/Scripts/bluimp/jquery.fileupload-ui.js"
                ));

            // number onlny
            bundles.Add(new ScriptBundle("~/bundles/numberOnly/js").Include(
              "~/Scripts/jQuery.numberOnly.js"));

            bundles.Add(new StyleBundle("~/bundles/knockoutfile/css").Include(
                "~/Content/knockout-file-bindings.css"));
            bundles.Add(new ScriptBundle("~/bundles/knockoutfile/js").Include(
                "~/Scripts/knockout-file-bindings.js"));


            bundles.Add(new ScriptBundle("~/bundles/bootbox/js").Include(
                "~/Scripts/bootbox.js"));

            // Touch Spin
            bundles.Add(new ScriptBundle("~/bundles/chartjs/js").Include(
                 "~/vendor/chartjs/Chart.min.js"));
        }
    }
}
