﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagementSystem.ConstUtil
{
    public static class ConstValue
    {
        public static class Status
        {
            public static String success = "SUC";
            public static String rec_success_disp = "ผ่านการอนุมัติ";
            public static String rec_init_disp = "ลงทะเบียน";
            public static String rec_reserv_disp = "ร่วมเล่นเกมส์";
            public static String rec_act_disp = "รอการตรวจสอบ";
            public static String rec_oth_disp = "อื่นๆ";
            public static String rec_rej_disp = "ไม่ผ่านการอนุมัติ";

            public static String init = "INIT";
            public static String active = "ACT";
            public static String reject = "REJ";
            public static String inactive = "INA";
            public static String pending = "PND";
            public static String reserved = "RESV";
            public static String init_disp = "Initial";
            public static String active_disp = "Active";
            public static String inactive_disp = "Inactive";
            public static String pending_disp = "Pending";
            public static String reserved_disp = "Reserved";

            public static String gameResult_win = "win";
            public static String gameResult_win_disp = "ชนะ";
            public static String gameResult_loss = "loss";
            public static String gameResult_loss_disp = "แพ้";
            public static String gameResult_none = "none";
            public static String gameResult_none_disp = "ยังไม่ร่วมเล่นเกมส์";
        }

        public static class MainConfig
        {
            public static String baseUploadFolder = "MC01";
        }

        public static class Flag
        {
            public static String yes = "Y";
            public static String no = "N";
        }

        public static class UploadPath
        {
            public static String product = "/FileUpload/Product/";
            public static String premium = "/FileUpload/Premium/";
            public static String store = "/FileUpload/Store/";
            public static String campaign = "/FileUpload/Campaign/";
            public static String receipt = "http://www.duckthailandcampaign.com/FileUploads/Receipt/";
        }

        public static class ExcelSheetName
        {
            public static String sms = "My Sheet";
            public static String coupon = "My Sheet";
        }

        public static class UploadDefault
        {
            public static String SMS_StoreName = "SMS";
            public static String SMS_ProdCode = "SMS";

            public static String COUPON_StoreName = "COUPON";
            public static String COUPON_ProdCode = "COUPON";
        }
    }
}