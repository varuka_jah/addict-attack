﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using ExcelDataReader;


namespace ManagementSystem.ConstUtil
{
    public class ExcelUtil
    {
        private HttpPostedFileBase _file;

        public ExcelUtil(HttpPostedFileBase file)
        {
            this._file = file;
        }

        private IExcelDataReader GetExcelDataReader()
        {
            try
            {
                string fileName = _file.FileName;
                var extension = Path.GetExtension(_file.FileName);

                IExcelDataReader excelReader;


                if (extension == ".xlsx")
                {
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(_file.InputStream);
                }
                else if (extension == ".xls")
                {
                    excelReader = ExcelReaderFactory.CreateBinaryReader(_file.InputStream);
                }
                else
                {
                    throw new Exception("The file to be processed is not an Excel file");
                }




                return excelReader;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        private DataSet GetExcelDataAsDataSet(bool isFirstRowAsColumnNames)
        {
            return GetExcelDataReader().AsDataSet(new ExcelDataSetConfiguration()
            {
                ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                {
                    UseHeaderRow = isFirstRowAsColumnNames
                }
            });
        }

        private DataTable GetExcelWorkSheet(string workSheetName, bool isFirstRowAsColumnNames)
        {
            DataSet dataSet = GetExcelDataAsDataSet(isFirstRowAsColumnNames);
            DataTable workSheet = dataSet.Tables[workSheetName];

            if (workSheet == null)
            {
                throw new Exception(string.Format("The worksheet {0} does not exist, has an incorrect name, or does not have any data in the worksheet", workSheetName));
            }

            return workSheet;
        }

        public List<DataRow> GetData(string workSheetName, bool isFirstRowAsColumnNames = true)
        {
            DataTable workSheet = GetExcelWorkSheet(workSheetName, isFirstRowAsColumnNames);
            return (from DataRow row in workSheet.Rows
                    select row).ToList();
        }


    }
}