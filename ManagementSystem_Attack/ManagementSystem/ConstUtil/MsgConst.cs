﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagementSystem.ConstUtil
{
    public static class MsgConst
    {
        public static String resetPassNotFound = "Password ไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง";

        public static String userNotFound = "ไม่พบ Username หรือ Password ที่ระบุ";
        public static String notFoundRecord = "ไม่พบรายการที่ต้องการแก้ไขค่ะ";

        public static String ProdCodeDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Product Code นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";
        public static String ProdNameDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Product Name นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";

        public static String PremiumCodeDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Premium Code นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";
        public static String PremiumNameDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Premium Name นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";

        public static String StoreNameDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Store Name นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";

        public static String MsgCodeDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Message Code นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";

        public static String RoleNameDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Role Name นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";

        public static String UserDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Username หรืออีเมลล์ นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";

        public static String CampCodeDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Campaign Code นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";
        public static String CampNameDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Campaign Name นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";
        //public static String BranchCodeDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Branch Code นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";
        //public static String DeptCodeDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Department Code นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";
        //public static String DeptNameDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Department Code นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";

        //public static String CourseCodeDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Course Code นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";

        //public static String TraineeNameDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ ชื่อผู้เรียน นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";

        //public static String userRoleDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Role Name นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";
        //public static String userProfileDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Username นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";

        //public static String sysMenuDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Menu Name นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";
        //public static String sysMenuObjDuplicate = "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากพบ Object Code นี้ในระบบแล้วค่ะ กรุณาตรวจสอบอีกครั้ง";
    }
}