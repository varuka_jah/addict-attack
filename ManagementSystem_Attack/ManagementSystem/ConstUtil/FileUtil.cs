﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ManagementSystem.ConstUtil
{
    public class FileUtil
    {
        private HttpPostedFileBase _file;

        public FileUtil(HttpPostedFileBase file)
        {
            this._file = file;
        }

        public FileUtil()
        {
        }

        public String saveFileToServer(String serverPath)
        {
            try
            {
                if (_file != null && _file.ContentLength > 0)
                {
                    string Serverpath = HttpContext.Current.Server.MapPath(serverPath); //serverPath;////

                    string file = _file.FileName;

                    string fileDirectory = Serverpath;

                    string ext = Path.GetExtension(fileDirectory +  file);
                    if (ext == null || ext.Trim().Equals(""))
                    {
                        ext = ".empty";
                    }
                    file = (Guid.NewGuid().ToString()) + ext; // Creating a unique name for the file 

                    if (File.Exists(fileDirectory + file))
                    {
                        File.Delete(fileDirectory + file);
                    }


                    fileDirectory = Serverpath + file;



                    _file.SaveAs(fileDirectory);

                    return  file;
                }
                else
                {
                    throw new Exception("File not found.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }


        public String updateFileToServer(String fileName, String serverPath)
        {
            try
            {
                if (_file != null && _file.ContentLength > 0)
                {
                    string Serverpath = HttpContext.Current.Server.MapPath(serverPath);

                    string file = _file.FileName;

                    string fileDirectory = Serverpath;
                    if (!Directory.Exists(fileDirectory))
                        Directory.CreateDirectory(fileDirectory);

                    string ext = Path.GetExtension(fileDirectory + file);
                    if (ext == null || ext.Trim().Equals(""))
                    {
                        ext = ".empty";
                    }
                    file = fileName + ext; // Creating a unique name for the file 

                    if (File.Exists(fileDirectory + file))
                    {
                        File.Delete(fileDirectory + file);
                    }


                    fileDirectory = Serverpath + file;

                    _file.SaveAs(fileDirectory);
                    return  file;
                }
                else
                {
                    throw new Exception("File not found.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        
        public void removeFileFromServer(String baseUploadFolder, String subFolder, String fileName)
        {
            try
            {

                string fileDirectory = baseUploadFolder + subFolder;
                if (File.Exists(fileDirectory + "\\" + fileName))
                {
                    File.Delete(fileDirectory + "\\" + fileName);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
    }
}