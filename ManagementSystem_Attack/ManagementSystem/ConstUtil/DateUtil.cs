﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ManagementSystem.ConstUtil
{
    public class DateUtil
    {
        public static bool convertFromString_MDY(String strDate, out DateTime date)
        {
            return DateTime.TryParseExact(strDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
        }
    }
}