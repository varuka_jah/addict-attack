﻿ko.bindingHandlers.valueNumber = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        // This will be called when the binding is first applied to an element
        // Set up any initial state, event handlers, etc. here

        var observable = valueAccessor(),
            properties = allBindingsAccessor();
        var valueUnwrapped = ko.utils.unwrapObservable(observable);

        var interceptor = ko.computed({
            read: function () {
                var format = properties.numberDigit || 2;
                return valueUnwrapped.toLocaleString(undefined, { minimumFractionDigits: format, maximumFractionDigits: format });//Globalize.numberFormat(observable(), format);
            },
            write: function (newValue) {
                var number = Globalize.parseFloat(newValue);
                if (number) {
                    valueUnwrapped = number;
                    //  observable(number);
                }
            }
        });

        if (ko.utils.tagNameLower(element) === 'input') {
            ko.applyBindingsToNode(element, { value: interceptor });
        } else {
            ko.applyBindingsToNode(element, { text: interceptor });
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        // This will be called when the binding is first applied to an element
        // Set up any initial state, event handlers, etc. here

        var observable = valueAccessor(),
            properties = allBindingsAccessor();
        var valueUnwrapped = ko.utils.unwrapObservable(observable);

        var interceptor = ko.computed({
            read: function () {
                var format = properties.numberDigit || 2;
                return valueUnwrapped.toLocaleString(undefined, { minimumFractionDigits: format, maximumFractionDigits: format });//Globalize.numberFormat(observable(), format);
            },
            write: function (newValue) {
                var number = Globalize.parseFloat(newValue);
                if (number) {
                    valueUnwrapped = number;
                    //  observable(number);
                }
            }
        });

        if (ko.utils.tagNameLower(element) === 'input') {
            ko.applyBindingsToNode(element, { value: interceptor });
        } else {
            ko.applyBindingsToNode(element, { text: interceptor });
        }
    }
};

ko.bindingHandlers.preSelect = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var val = ko.utils.unwrapObservable(valueAccessor());
        var newOptions = element.getElementsByTagName("option");
        var updateRequired = false;
        for (var i = 0, j = newOptions.length; i < j; i++) {
            if (ko.utils.unwrapObservable(val.value) == ko.selectExtensions.readValue(newOptions[i])[val.key]) {
                if (!newOptions[i].selected) {
                    ko.utils.setOptionNodeSelectionState(newOptions[i], true);//only sets the selectedindex, object still holds index 0 as selected
                    updateRequired = true;
                }
            }
        }
        if (updateRequired) {
            var options = allBindingsAccessor().options;
            var selected = ko.utils.arrayFirst(options, function (item) {
                return ko.utils.unwrapObservable(val.value) == item[val.key];
            });
            if (ko.isObservable(bindingContext.$data[val.propertyName])) {
                bindingContext.$data[val.propertyName](selected); // here we write the correct object back into the $data
            } else {
                bindingContext.$data[val.propertyName] = selected; // here we write the correct object back into the $data
            }
        }
    }
};

ko.bindingHandlers.ladda = {
    init: function (element, valueAccessor) {
        var l = Ladda.create(element);

        ko.computed({
            read: function () {
                var state = ko.unwrap(valueAccessor());
                if (state)
                    l.start();
                else
                    l.stop();
            },
            disposeWhenNodeIsRemoved: element
        });
    }
};


ko.bindingHandlers.dataTablesForEach = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var binding = ko.utils.unwrapObservable(valueAccessor());

        ko.unwrap(binding.data);

        if (binding.options.paging) {
            binding.data.subscribe(function (changes) {
                var table = $(element).closest('table').DataTable();
                ko.bindingHandlers.dataTablesForEach.page = table.page();
                table.destroy();
            }, null, 'arrayChange');
        }

        var nodes = Array.prototype.slice.call(element.childNodes, 0);
        ko.utils.arrayForEach(nodes, function (node) {
            if (node && node.nodeType !== 1) {
                node.parentNode.removeChild(node);
            }
        });

        return ko.bindingHandlers.foreach.init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var value = ko.unwrap(valueAccessor()),
        key = "DataTablesForEach_Initialized";

        var newValue = function () {
            return {
                data: value.data || value,
                beforeRenderAll: function (el, index, data) {
                    if (ko.utils.domData.get(element, key)) {
                        $(element).closest('table').DataTable().destroy();
                    }
                },
                afterRenderAll: function (el, index, data) {
                    $(element).closest('table').DataTable(value.options);
                }

            };
        };

        ko.bindingHandlers.foreach.update(element, newValue, allBindingsAccessor, viewModel, bindingContext);

        //if we have not previously marked this as initialized and there is currently items in the array, then cache on the element that it has been initialized
        if (!ko.utils.domData.get(element, key) && (value.data || value.length)) {
            ko.utils.domData.set(element, key, true);
        }

        return { controlsDescendantBindings: true };
    }
};
ko.bindingHandlers['datepicker'] = {
    'init': function (element, valueAccessor, allBindingsAccessor) {
        /* Initialize datepicker with some optional options */
        var options = allBindingsAccessor().datePickeroptions || {},
            prop = valueAccessor(),
            $elem = $(element);

        prop($elem.val());

        $elem.datepicker(options);

        /* Handle the field changing */
        ko.utils.registerEventHandler(element, "change", function () {
            prop($elem.datepicker("value")[0].value);
          
        });

        /* Handle disposal (if KO removes by the template binding) */
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $elem.datepicker("destroy");
        });
    },
    'update': function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor()),
            $elem = $(element),
            current = $elem.datepicker("value")[0].value;;

        if (value - current !== 0) {
            $elem.datepicker("setDate", value);
        }
    }
};

 

//ko.bindingHandlers.datepicker = {
//    init: function (element, valueAccessor, allBindingsAccessor) {
//        var options = allBindingsAccessor().datepickerOptions || {};
//        $(element).datepicker(options).on("changeDate", function (ev) {
//            var observable = valueAccessor();
//            observable(ev.date);
//        });
//    },
//    update: function (element, valueAccessor) {
//        var value = ko.utils.unwrapObservable(valueAccessor());
//        $(element).datepicker("setValue", value);
//    }
//}; 