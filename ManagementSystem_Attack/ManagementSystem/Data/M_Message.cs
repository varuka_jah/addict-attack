//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManagementSystem.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_Message
    {
        public System.Guid message_guid { get; set; }
        public string message_code { get; set; }
        public string message_value { get; set; }
        public string message_remark { get; set; }
    }
}
