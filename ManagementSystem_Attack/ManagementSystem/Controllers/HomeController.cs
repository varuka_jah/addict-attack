﻿using ManagementSystem.ConstUtil;
using ManagementSystem.Controllers.Base;
using ManagementSystem.Models;
using ManagementSystem.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManagementSystem.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Dashboard()
        {
            DashboardActionModel model = new DashboardActionModel();
            model.dtoCriteria = new DashboardSearchActionModel();
            model.dtoUserActivity = new ActivityModel();
            model.lstDateAct = new List<DateActivityModel>();
            model.lstByStore = new List<TotalByStoreAndCate>();
            model.lstByStoreCate = new List<TotalByStoreAndCate>();


            CampaignService serv = new CampaignService();
            CampaignSearchModel campModel = new CampaignSearchModel();
            campModel.status_act = true;
            List<CampaignSearchResultModel> lstCamp = serv.searchByCriteria(_db, campModel);
            model.lstSel_Campaign = ConvertUtil.generateDDL_Campaign(lstCamp);
            if (lstCamp != null && lstCamp.Count() == 1)
            {
                model.dtoCriteria.camp_guid = lstCamp[0].camp_guid;

                DashboardService dsServ = new DashboardService();
                model = dsServ.getData(_db, lstCamp[0].camp_guid);
            }

            return View(model);
        }

        public JsonResult Dashboard_Search(Guid campGuid)
        {
            try
            {
                if (campGuid == null || campGuid == null)
                {
                    return Json(new JSonResultService() { status = 500, message = "กรุณาเลือก Campaign ก่อนค่ะ", dataResult = null }, JsonRequestBehavior.DenyGet);
                }

                DashboardService serv = new DashboardService();
                DashboardActionModel model = serv.getData(_db, campGuid);
                
                return Json(new JSonResultService() { status = 200, message = "", dataResult = model }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.DenyGet);
            }
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}