﻿using ManagementSystem.ConstUtil;
using ManagementSystem.Controllers.Base;
using ManagementSystem.Models;
using ManagementSystem.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ApplicationServices;
using System.Web.Mvc;

namespace ManagementSystem.Controllers
{
    public class SecurityController : BaseController
    {
        #region Role
        // GET: Product
        public ActionResult Role()
        {
            RoleActionModel model = new RoleActionModel();
            model.dtoCriteria = new RoleSearchModel();
            model.dtoFocus = new RoleModel();
            model.dtoFocus.role_isAdmin = ConstUtil.ConstValue.Flag.no;
            model.lstResult = new List<RoleModel>();

            return View(model);
        }
        public JsonResult Role_SaveNew(RoleModel model)
        {
            try
            {
                SecurityService serv = new SecurityService();
                serv.roleCreate(_db, _userInfo, model);

                RoleSearchModel dtoCriteria = new RoleSearchModel();
                List<RoleModel> lstResult = serv.role_searchByCriteria(_db, dtoCriteria);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Role_SaveEdit(RoleModel model)
        {
            try
            {
                SecurityService serv = new SecurityService();
                serv.roleUpdate(_db, _userInfo, model);

                RoleSearchModel dtoCriteria = new RoleSearchModel();
                List<RoleModel> lstResult = serv.role_searchByCriteria(_db, dtoCriteria);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Role_Search(RoleSearchModel model)
        {
            try
            {
                SecurityService serv = new SecurityService();
                List<RoleModel> lstResult = serv.role_searchByCriteria(_db, model);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region User
        // GET: Product
        public ActionResult UserProfile()
        {
            UserActionModel model = new UserActionModel();
            model.dtoCriteria = new UserSearchModel();
            model.dtoFocus = new UserModel();
            model.lstResult = new List<UserModel>();

            SecurityService serv = new SecurityService();
            RoleSearchModel roleM = new RoleSearchModel();
            roleM.status_act = true;
            List<RoleModel> lstModel = serv.role_searchByCriteria(_db, roleM);
            model.lstSelRole = ConvertUtil.generateDDL_Role(lstModel);
            return View(model);
        }

        public JsonResult User_SaveNew(UserModel model)
        {
            try
            {
                SecurityService serv = new SecurityService();
                serv.userCreate(_db, _userInfo, model);

                UserSearchModel dtoCriteria = new UserSearchModel();
                List<UserModel> lstResult = serv.user_searchByCriteria(_db, dtoCriteria);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult User_SaveEdit(UserModel model)
        {
            try
            {
                SecurityService serv = new SecurityService();
                serv.userUpdate(_db, _userInfo, model);

                UserSearchModel dtoCriteria = new UserSearchModel();
                List<UserModel> lstResult = serv.user_searchByCriteria(_db, dtoCriteria);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult User_Search(UserSearchModel model)
        {
            try
            {
                SecurityService serv = new SecurityService();
                List<UserModel> lstResult = serv.user_searchByCriteria(_db, model);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}