﻿using ManagementSystem.Models;
using ManagementSystem.Models.Service;
using ManagementSystem.Models.Session;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ManagementSystem.Controllers
{
    public class UserController : Controller
    {
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new UserLoginModel());
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(UserLoginModel model, string returnUrl)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                UserService serv = new UserService();
                UserDetailSession _userInfo = serv.getUserLogin(model);
                Session.Add("UserInfoSession", _userInfo);

                setOWIN(_userInfo);
                return Redirect(GetRedirectUrl(returnUrl));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return View(model);
        }

        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("Dashboard", "Home");
            }

            return returnUrl;
        }

        public ActionResult LogOut()
        {
            Request.GetOwinContext().Authentication.SignOut();
            return Redirect("/");
        }

        public ActionResult LogOff()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;
            authManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Abandon();

            Request.GetOwinContext().Authentication.SignOut();
            var cookieName = FormsAuthentication.FormsCookieName;
            FormsAuthentication.SignOut();
            if (Request.Cookies[cookieName] != null)
            {
                var authCookie = new HttpCookie(cookieName);
                authCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(authCookie);
            }
            //    logger.InfoFormat("User '{0}' logout completed.", User.Identity.Name);
            return RedirectToAction("Login", "User");
        }

        public void setOWIN(UserDetailSession userInfo)
        {
            try
            {
                var identity = new ClaimsIdentity(new[] {
                        new Claim(ClaimTypes.NameIdentifier.ToString(),  userInfo.loginUserDetail.user_guid.ToString()),
                        new Claim(ClaimTypes.Name, userInfo.loginUserDetail.user_name),
                        new Claim(ClaimTypes.Role, userInfo.loginUserRole.roleGuid.ToString())
                    }, DefaultAuthenticationTypes.ApplicationCookie);

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;
                authManager.SignIn(identity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        
        #region Helpers
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}