﻿using ManagementSystem.Controllers.Base;
using ManagementSystem.Models;
using ManagementSystem.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManagementSystem.Controllers
{
    public class PremiumController : BaseController
    {
        // GET: Product
        public ActionResult Index()
        {
            PremiumActionModel model = new PremiumActionModel();
            model.dtoCriteria = new PremiumSearchModel();
            model.dtoFocus = new PremiumModel();
            model.dtoFocus.prem_img = new FileModel.FileAttachModel();
            model.lstResult = new List<PremiumModel>();

            return View(model);
        }

        public JsonResult SaveNew(PremiumModel model)
        {
            try
            {
                PremiumService serv = new PremiumService();
                serv.create(_db, _userInfo, model);

                PremiumSearchModel dtoCriteria = new PremiumSearchModel();
                List<PremiumModel> lstResult = serv.searchByCriteria(_db, dtoCriteria);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UploadFile()
        {
            try
            {
                ConstUtil.FileUtil fUtil;
                String newFileName = "";

                if (Request.Files != null && Request.Files.Count == 1)
                {
                    HttpPostedFileBase file = Request.Files[Request.Files.Keys[0]] as HttpPostedFileBase;

                    if (file != null && file.ContentLength > 0)
                    {
                        fUtil = new ConstUtil.FileUtil(file);
                        newFileName = fUtil.saveFileToServer(ConstUtil.ConstValue.UploadPath.premium);
                    }
                }
                return Json(new JSonResultService() { status = 200, message = "", dataResult = newFileName, dataResult_2 = "" }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UploadFile_Edit()
        {
            try
            {
                ConstUtil.FileUtil fUtil;
                String newFileName = "";

                if (Request.Files != null && Request.Files.Count == 1)
                {
                    HttpPostedFileBase file = Request.Files[Request.Files.Keys[0]] as HttpPostedFileBase;
                    String fileName = Request.Params[Request.Params.Keys[0]];

                    if (file != null && file.ContentLength > 0)
                    {
                        fUtil = new ConstUtil.FileUtil(file);
                        newFileName = fUtil.updateFileToServer(fileName, ConstUtil.ConstValue.UploadPath.premium);
                    }
                }
                return Json(new JSonResultService() { status = 200, message = "", dataResult = newFileName, dataResult_2 = "" }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveEdit(PremiumModel model)
        {
            try
            {
                PremiumService serv = new PremiumService();
                serv.update(_db, _userInfo, model);

                PremiumSearchModel dtoCriteria = new PremiumSearchModel();
                List<PremiumModel> lstResult = serv.searchByCriteria(_db, dtoCriteria);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Search(PremiumSearchModel model)
        {
            try
            {
                PremiumService serv = new PremiumService();
                List<PremiumModel> lstResult = serv.searchByCriteria(_db, model);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getPremByGuid(String premGuid)
        {
            try
            {
                Guid premG;
                if (premGuid == null || !Guid.TryParse(premGuid, out premG))
                {
                    return Json(new JSonResultService() { status = 500, message = "ไม่พบ Premium Guid", dataResult = null }, JsonRequestBehavior.AllowGet);
                }

                PremiumService serv = new PremiumService();
                PremiumSearchModel model = new PremiumSearchModel();
                model.prem_guid = premG;
                List<PremiumModel> lstResult = serv.searchByCriteria(_db, model);
               
                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult[0] }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}