﻿using ManagementSystem.ConstUtil;
using ManagementSystem.Controllers.Base;
using ManagementSystem.Models;
using ManagementSystem.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ManagementSystem.ConstUtil.ConstValue;

namespace ManagementSystem.Controllers
{
    public class ReportController : BaseController
    {
        // GET: Report
        public ActionResult ByCustomer(Guid custGuid)
        {

            if (custGuid == null || custGuid.Equals(Guid.Empty))
            {
                return RedirectToAction("Index", "Customer");
            }

            ReportByCustomer model = new ReportByCustomer();
            CustomerReceiptService serv = new CustomerReceiptService();
            model.lstData = serv.getCustomerReceiptHistory(_db, custGuid);

            CustomerService custServ = new CustomerService();
            model.dtoCustomer = custServ.getCustomerModelInfoByID(_db, custGuid);
            model.lstSel_RecStatus = ConvertUtil.generateDDL_RecStatus();
            model.dtoData = new CustomerReceiptModel();
            return View(model);
        }

        public JsonResult ByCustomer_getData(Guid custGuid)
        {
            CustomerReceiptService serv = new CustomerReceiptService();
            if (custGuid == null || custGuid == null)
            {
                return Json(new JSonResultService() { status = 500, message = "กรุณาเลือกรายการก่อนค่ะ", dataResult = null }, JsonRequestBehavior.DenyGet);
            }


            List<CustomerReceiptModel> lstRecp = serv.getCustomerReceiptHistory(_db, custGuid);
            return Json(new JSonResultService() { status = 200, message = "", dataResult = lstRecp, dataResult_2 = null }, JsonRequestBehavior.DenyGet);
        }
        public JsonResult DeleteReceipt(Guid custRecGuid)
        {
            CustomerReceiptService serv = new CustomerReceiptService();
            if (custRecGuid == null || custRecGuid == null )
            {
                return Json(new JSonResultService() { status = 500, message = "กรุณาเลือกรายการก่อนค่ะ", dataResult = null }, JsonRequestBehavior.DenyGet);
            }


            serv.deleteReceipt(_db, custRecGuid);
            return Json(new JSonResultService() { status = 200, message = "", dataResult = null, dataResult_2 = null }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult UpdateRecStatus(Guid custRecGuid, String newStat, String custRecNo)
        {
            CustomerReceiptService serv = new CustomerReceiptService();
            if (custRecGuid == null || custRecGuid == null || newStat == null || newStat.Equals("") || custRecNo == null || custRecNo.Trim().Equals(""))
            {
                return Json(new JSonResultService() { status = 500, message = "กรุณาเลือกรายการก่อนค่ะ", dataResult = null }, JsonRequestBehavior.DenyGet);
            }


            Guid custGuid = serv.updateCustomerReceiptStatus(_db, custRecGuid, newStat, custRecNo);
           
            return Json(new JSonResultService() { status = 200, message = "", dataResult = null, dataResult_2 = null }, JsonRequestBehavior.DenyGet);
        }

        public ActionResult ByCampaign(Guid campGuid)
        {

            if (campGuid == null || campGuid.Equals(Guid.Empty))
            {
                return RedirectToAction("List", "Campaign");
            }
            ReportByCamp model = new ReportByCamp();
            model.dtoCriteria_act = new CampReportSearchModel();
            model.dtoCriteria_act.camp_guid = campGuid;
            model.dtoCriteria_act.custRec_status = Status.active;

            model.dtoCriteria_int = new CampReportSearchModel();
            model.dtoCriteria_int.camp_guid = campGuid;
            model.dtoCriteria_int.custRec_status = Status.init;

            model.dtoCriteria_resv = new CampReportSearchModel();
            model.dtoCriteria_resv.camp_guid = campGuid;
            model.dtoCriteria_resv.custRec_status = Status.reserved;

            model.dtoCriteria_succ = new CampReportSearchModel();
            model.dtoCriteria_succ.camp_guid = campGuid;
            model.dtoCriteria_succ.custRec_status = Status.success;

            model.dtoData = new CustomerReceiptModel();
            model.lstData_act = new List<CustomerReceiptModel>();
            model.lstData_int = new List<CustomerReceiptModel>();
            model.lstData_resv = new List<CustomerReceiptModel>();
            model.lstData_succ = new List<CustomerReceiptModel>();
            model.lstSel_RecStatus = ConvertUtil.generateDDL_RecStatus();

            CampaignService campServ = new CampaignService();
            model.dtoCamp = campServ.getCampaignByGuId(_db, campGuid);
            model.totalJoin = campServ.getNoByCamp(_db, campGuid);

            return View(model);
            
        }

        public JsonResult SearchByCamp_act(CampReportSearchModel model)
        {
            try
            {
                CustomerReceiptService serv = new CustomerReceiptService();
                int filterRecords = 0;
                model.custRec_status = Status.active;
                List<CustomerReceiptModel> lstResult = serv.getCustReceiptByCamp(_db, model, out filterRecords);

                return Json(new
                {
                    draw = model.draw,
                    recordsTotal = filterRecords,
                    recordsFiltered = filterRecords,
                    data = lstResult

                });
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SearchByCamp_init(CampReportSearchModel model)
        {
            try
            {
                CustomerReceiptService serv = new CustomerReceiptService();
                int filterRecords = 0;
                model.custRec_status = Status.init;
                List<CustomerReceiptModel> lstResult = serv.getCustReceiptByCamp(_db, model, out filterRecords);

                return Json(new
                {
                    draw = model.draw,
                    recordsTotal = filterRecords,
                    recordsFiltered = filterRecords,
                    data = lstResult

                });
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SearchByCamp_resv(CampReportSearchModel model)
        {
            try
            {
                CustomerReceiptService serv = new CustomerReceiptService();
                int filterRecords = 0;
                model.custRec_status = Status.reserved;
                List<CustomerReceiptModel> lstResult = serv.getCustReceiptByCamp(_db, model, out filterRecords);

                return Json(new
                {
                    draw = model.draw,
                    recordsTotal = filterRecords,
                    recordsFiltered = filterRecords,
                    data = lstResult

                });
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SearchByCamp_succ(CampReportSearchModel model)
        {
            try
            {
                CustomerReceiptService serv = new CustomerReceiptService();
                int filterRecords = 0;
                model.custRec_status = Status.success;
                List<CustomerReceiptModel> lstResult = serv.getCustReceiptByCamp(_db, model, out filterRecords);

                return Json(new
                {
                    draw = model.draw,
                    recordsTotal = filterRecords,
                    recordsFiltered = filterRecords,
                    data = lstResult
                });
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SearchByCamp_rej(CampReportSearchModel model)
        {
            try
            {
                CustomerReceiptService serv = new CustomerReceiptService();
                int filterRecords = 0;
                model.custRec_status = Status.reject;
                List<CustomerReceiptModel> lstResult = serv.getCustReceiptByCamp(_db, model, out filterRecords);

                return Json(new
                {
                    draw = model.draw,
                    recordsTotal = filterRecords,
                    recordsFiltered = filterRecords,
                    data = lstResult

                });
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}