﻿using ManagementSystem.Controllers.Base;
using ManagementSystem.Models;
using ManagementSystem.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManagementSystem.Controllers
{
    public class MsgController : BaseController
    {
        // GET: Msg
        public ActionResult Index()
        {
            MsgActionModel model = new MsgActionModel();
            model.dtoCriteria = new MsgSearchModel();
            model.dtoFocus = new MsgModel();
            model.lstResult = new List<MsgModel>();

            return View(model);
        }

        public JsonResult SaveEdit(MsgModel model)
        {
            try
            {
                MsgService serv = new MsgService();
                serv.update(_db, _userInfo, model);

                MsgSearchModel dtoCriteria = new MsgSearchModel();
                List<MsgModel> lstResult = serv.searchByCriteria(_db, dtoCriteria);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Search(MsgSearchModel model)
        {
            try
            {
                MsgService serv = new MsgService();
                List<MsgModel> lstResult = serv.searchByCriteria(_db, model);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}