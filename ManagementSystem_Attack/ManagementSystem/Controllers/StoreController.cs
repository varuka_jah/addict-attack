﻿using ManagementSystem.Controllers.Base;
using ManagementSystem.Models;
using ManagementSystem.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManagementSystem.Controllers
{
    public class StoreController : BaseController
    {
        // GET: Product
        public ActionResult Index()
        {
            StoreActionModel model = new StoreActionModel();
            model.dtoCriteria = new StoreSearchModel();
            model.dtoFocus = new StoreModel();
            model.dtoFocus.store_img = new FileModel.FileAttachModel();
            model.lstResult = new List<StoreModel>();

            return View(model);
        }

        public JsonResult SaveNew(StoreModel model)
        {
            try
            {
                StoreService serv = new StoreService();
                serv.create(_db, _userInfo, model);

                StoreSearchModel dtoCriteria = new StoreSearchModel();
                List<StoreModel> lstResult = serv.searchByCriteria(_db, dtoCriteria);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UploadFile()
        {
            try
            {
                ConstUtil.FileUtil fUtil;
                String newFileName = "";

                if (Request.Files != null && Request.Files.Count == 1)
                {
                    HttpPostedFileBase file = Request.Files[Request.Files.Keys[0]] as HttpPostedFileBase;

                    if (file != null && file.ContentLength > 0)
                    {
                        fUtil = new ConstUtil.FileUtil(file);
                        newFileName = fUtil.saveFileToServer(ConstUtil.ConstValue.UploadPath.store);
                    }
                }
                return Json(new JSonResultService() { status = 200, message = "", dataResult = newFileName, dataResult_2 = "" }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UploadFile_Edit()
        {
            try
            {
                ConstUtil.FileUtil fUtil;
                String newFileName = "";

                if (Request.Files != null && Request.Files.Count == 1)
                {
                    HttpPostedFileBase file = Request.Files[Request.Files.Keys[0]] as HttpPostedFileBase;
                    String fileName = Request.Params[Request.Params.Keys[0]];

                    if (file != null && file.ContentLength > 0)
                    {
                        fUtil = new ConstUtil.FileUtil(file);
                        newFileName = fUtil.updateFileToServer(fileName, ConstUtil.ConstValue.UploadPath.store);
                    }
                }
                return Json(new JSonResultService() { status = 200, message = "", dataResult = newFileName, dataResult_2 = "" }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveEdit(StoreModel model)
        {
            try
            {
                StoreService serv = new StoreService();
                serv.update(_db, _userInfo, model);

                StoreSearchModel dtoCriteria = new StoreSearchModel();
                List<StoreModel> lstResult = serv.searchByCriteria(_db, dtoCriteria);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Search(StoreSearchModel model)
        {
            try
            {
                StoreService serv = new StoreService();
                List<StoreModel> lstResult = serv.searchByCriteria(_db, model);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}