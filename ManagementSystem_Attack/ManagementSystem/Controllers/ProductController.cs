﻿
using ManagementSystem.Controllers.Base;
using ManagementSystem.Models;
using ManagementSystem.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace ManagementSystem.Controllers
{
    public class ProductController : BaseController
    {

        // GET: Product
        public ActionResult Index()
        {
            ProductActionModel model = new ProductActionModel();
            model.dtoCriteria = new ProductSearchModel();
            model.dtoFocus = new ProductModel();
            model.dtoFocus.prod_img = new FileModel.FileAttachModel();
            model.lstResult = new List<ProductModel>();

            return View(model);
        }

        public JsonResult SaveNew(ProductModel model)
        {
            try
            {
                ProductService serv = new ProductService();
                serv.create(_db, _userInfo, model);

                ProductSearchModel dtoCriteria = new ProductSearchModel();
                List<ProductModel> lstResult = serv.searchByCriteria(_db, dtoCriteria);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UploadFile()
        {
            try
            {
                ConstUtil.FileUtil fUtil;
                String newFileName = "";

                if (Request.Files != null && Request.Files.Count == 1)
                {
                    HttpPostedFileBase file = Request.Files[Request.Files.Keys[0]] as HttpPostedFileBase;
                 
                    if (file != null && file.ContentLength > 0)
                    {
                        fUtil = new ConstUtil.FileUtil(file);
                        newFileName = fUtil.saveFileToServer(ConstUtil.ConstValue.UploadPath.product);
                    }
                }
                return Json(new JSonResultService() { status = 200, message = "", dataResult = newFileName, dataResult_2 = "" }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UploadFile_Edit()
        {
            try
            {
                ConstUtil.FileUtil fUtil;
                String newFileName = "";

                if (Request.Files != null && Request.Files.Count == 1)
                {
                    HttpPostedFileBase file = Request.Files[Request.Files.Keys[0]] as HttpPostedFileBase;
                    String fileName = Request.Params[Request.Params.Keys[0]];

                    if (file != null && file.ContentLength > 0)
                    {
                        fUtil = new ConstUtil.FileUtil(file);
                        newFileName = fUtil.updateFileToServer(fileName, ConstUtil.ConstValue.UploadPath.product);
                    }
                }
                return Json(new JSonResultService() { status = 200, message = "", dataResult = newFileName, dataResult_2 = "" }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveEdit(ProductModel model)
        {
            try
            {
                ProductService serv = new ProductService();
                serv.update(_db, _userInfo, model);

                ProductSearchModel dtoCriteria = new ProductSearchModel();
                List<ProductModel> lstResult = serv.searchByCriteria(_db, dtoCriteria);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Search(ProductSearchModel model)
        {
            try
            {
                ProductService serv = new ProductService();
                List<ProductModel> lstResult = serv.searchByCriteria(_db, model);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getProdByGuid(String prodGuid)
        {
            try
            {
                Guid prodG;
                if (prodGuid == null || !Guid.TryParse(prodGuid, out prodG))
                {
                    return Json(new JSonResultService() { status = 500, message = "ไม่พบ Product Guid", dataResult = null }, JsonRequestBehavior.AllowGet);
                }

                ProductService serv = new ProductService();
                ProductSearchModel model = new ProductSearchModel();
                model.prod_guid = prodG;
                List<ProductModel> lstResult = serv.searchByCriteria(_db, model);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult[0] }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}