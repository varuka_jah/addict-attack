﻿using ManagementSystem.ConstUtil;
using ManagementSystem.Controllers.Base;
using ManagementSystem.Models;
using ManagementSystem.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManagementSystem.Controllers
{
    public class CustomerController : BaseController
    {
        // GET: Customer
        // GET: Msg
        //public ActionResult Index()
        //{
        //    CustomerActionModel model = new CustomerActionModel();
        //    model.dtoCriteria = new CustomerSearchModel();
        //    model.dtoFocus = new CustomerModel();
        //    model.lstResult = new List<CustomerModel>();

        //    CustomerService custServ = new CustomerService();
        //    model.lstProvince = custServ.getProvince(_db);
        //    model.lstSel_Province = ConvertUtil.generateDDL_Province(model.lstProvince);

        //    model.lstDistrict = new List<AddrDistrictModel>();
        //    model.lstSel_District = new List<SelectListItem>();

        //    model.lstSubDistrict = new List<AddrDistrictModel>();
        //    model.lstSel_SubDistrict = new List<SelectListItem>();


        //    return View(model);
        //}

        public ActionResult Index(Guid custGuid)
        {
            CustomerActionModel model = new CustomerActionModel();
            model.dtoCriteria = new CustomerSearchModel();
            model.dtoFocus = new CustomerModel();
            model.lstResult = new List<CustomerModel>();

            CustomerService custServ = new CustomerService();
            model.lstProvince = custServ.getProvince(_db);
            model.lstSel_Province = ConvertUtil.generateDDL_Province(model.lstProvince);

            model.lstDistrict = new List<AddrDistrictModel>();
            model.lstSel_District = new List<SelectListItem>();

            model.lstSubDistrict = new List<AddrDistrictModel>();
            model.lstSel_SubDistrict = new List<SelectListItem>();

            if (custGuid != null && !custGuid.Equals(Guid.Empty))
            {
                model.dtoCriteria.cust_guid = custGuid;
                model.lstResult = custServ.searchByCriteria(_db, model.dtoCriteria);
                if (model.lstResult != null && model.lstResult.Count() == 1)
                {
                    model.dtoCriteria.continueProcess = true;
                    model.dtoCriteria.cust_fullname = model.lstResult[0].cust_fullname;
                }
            }
            return View(model);
        }


        public JsonResult Search(CustomerSearchModel model)
        {
            try
            {
                if (model.continueProcess)
                {
                    CustomerService serv = new CustomerService();
                    int filterRecords = 0;
                    List<CustomerModel> lstResult = serv.searchByCriteria(_db, model, out filterRecords);
                    
                    return Json(new
                    {
                        draw = model.draw,
                        recordsTotal = filterRecords,
                        recordsFiltered = filterRecords,
                        data = lstResult
                    });
                }
                else
                {
                    return Json(new
                    {

                        draw = model.draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        data = new LinkedList<CustomerModel>()
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Customer_ViewDetail(Guid custGuid)
        {
            try
            {
                if (custGuid == null || custGuid == null)
                {
                    return Json(new JSonResultService() { status = 500, message = "กรุณาเลือกรายการก่อนค่ะ", dataResult = null }, JsonRequestBehavior.DenyGet);
                }
                CustomerService serv = new CustomerService();
                CustomerSearchModel model = new CustomerSearchModel();
                model.cust_guid = custGuid;

                CustomerModel data = serv.searchByCriteria(_db, model)[0];

                return Json(new JSonResultService() { status = 200, message = "", dataResult = data }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.DenyGet);
            }
        }
        public JsonResult Customer_SaveNew(CustomerModel model)
        {
            try
            {
                CustomerService serv = new CustomerService();
                serv.update(_db, _userInfo, model);

                CustomerSearchModel dtoCriteria = new CustomerSearchModel();
                dtoCriteria.cust_guid = model.cust_guid;
                List<CustomerModel> lstResult = serv.searchByCriteria(_db, dtoCriteria);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.DenyGet);
            }
        }

        public JsonResult getDistrictByProvince(String provId)
        {
            CustomerService serv = new CustomerService();

            if (provId == null || provId.Equals(""))
            {
                return Json(new JSonResultService() { status = 200, message = "", dataResult = new List<SelectListItem>(), dataResult_2 = new List<AddrDistrictModel>() }, JsonRequestBehavior.DenyGet);
            }
            else
            {
                List<AddrDistrictModel> lstData = serv.getDistrictByProv(Convert.ToInt64(provId));
                List<SelectListItem> lstSel = ConvertUtil.generateDDL_District(lstData);
                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstSel, dataResult_2 = lstData }, JsonRequestBehavior.DenyGet);
            }
        }

        public JsonResult getSubDistrictByDistrict(String districtId)
        {
            CustomerService serv = new CustomerService();

            if (districtId == null || districtId.Equals(""))
            {
                return Json(new JSonResultService() { status = 200, message = "", dataResult = new List<SelectListItem>(), dataResult_2 = new List<AddrSubDistrictModel>() }, JsonRequestBehavior.DenyGet);
            }
            else
            {
                List<AddrSubDistrictModel> lstData = serv.getSubDistrictByDistrict(Convert.ToInt64(districtId));
                List<SelectListItem> lstSel = ConvertUtil.generateDDL_SubDistrict(lstData);
                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstSel, dataResult_2 = lstData }, JsonRequestBehavior.DenyGet);
            }
        }
    }
}