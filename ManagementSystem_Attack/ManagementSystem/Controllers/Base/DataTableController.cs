﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManagementSystem.Controllers.Base
{
    public class DataTableController : Controller
    {
        //public JsonResult CustomServerSideSearchAction(DataTableAjaxPostModel model)
        //{
        //    // action inside a standard controller
        //    int filteredResultsCount;
        //    int totalResultsCount;
        //    var res = YourCustomSearchFunc(model, out filteredResultsCount, out totalResultsCount);

        //    var result = new List<YourCustomSearchClass>(res.Count);
        //    foreach (var s in res)
        //    {
        //        // simple remapping adding extra info to found dataset
        //        result.Add(new YourCustomSearchClass
        //        {
        //            EmployerId = User.ClaimsUserId(),
        //            Id = s.Id,
        //            Pin = s.Pin,
        //            Firstname = s.Firstname,
        //            Lastname = s.Lastname,
        //            RegistrationStatusId = DoSomethingToGetIt(s.Id),
        //            Address3 = s.Address3,
        //            Address4 = s.Address4
        //        });
        //    };

        //    return Json(new
        //    {
        //        // this is what datatables wants sending back
        //        draw = model.draw,
        //        recordsTotal = totalResultsCount,
        //        recordsFiltered = filteredResultsCount,
        //        data = result
        //    });
        //}

        //public IList<YourCustomSearchClass> YourCustomSearchFunc(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount)
        //{
        //    var searchBy = (model.search != null) ? model.search.value : null;
        //    var take = model.length;
        //    var skip = model.start;

        //    string sortBy = "";
        //    bool sortDir = true;

        //    if (model.order != null)
        //    {
        //        // in this example we just default sort on the 1st column
        //        sortBy = model.columns[model.order[0].column].data;
        //        sortDir = model.order[0].dir.ToLower() == "asc";
        //    }

        //    // search the dbase taking into consideration table sorting and paging
        //    var result = GetDataFromDbase(searchBy, take, skip, sortBy, sortDir, out filteredResultsCount, out totalResultsCount);
        //    if (result == null)
        //    {
        //        // empty collection...
        //        return new List<YourCustomSearchClass>();
        //    }
        //    return result;
        //}
    }
}