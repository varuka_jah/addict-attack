﻿
using ManagementSystem.Data;
using ManagementSystem.Models;
using ManagementSystem.Models.Service;
using ManagementSystem.Models.Session;
using Microsoft.AspNet.Identity;
using System;
using System.IO;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using WebMatrix.WebData;

namespace ManagementSystem.Controllers.Base
{
    public class BaseController : Controller
    {
        /// <summary>
        /// Gets the current site session.
        /// </summary>
        public UserDetailSession CurrentUserInfoSession
        {
            get
            {
                UserDetailSession userInfoSession = (UserDetailSession)this.Session["UserInfoSession"];
                return userInfoSession;
            }
        }
        /// <summary>
        /// The data context.
        /// </summary>
        protected dbEntities _db = new dbEntities();
        protected UserDetailSession _userInfo;
        protected MainConfigModel _mainConig;

        /// <summary>
        /// Disable the Async support.
        /// </summary>
        /// <remarks>
        /// Must be diable, otherwise ExecuteCore() will not be invoked in MVC4 like was in MVC3!
        /// </remarks>
        protected override bool DisableAsyncSupport
        {
            get { return true; }
        }

        /// <summary>
        /// Dispose the used resource.
        /// </summary>
        /// <param name="disposing">The disposing flag.</param>
        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);


        }

        /// <summary>
        /// Manage the internationalization before to invokes the action in the current controller context.
        /// </summary>
        protected override void ExecuteCore()
        {
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                UserDetailSession userInfoSession = (UserDetailSession)this.Session["UserInfoSession"];
                Guid userID = Guid.Empty;
                bool isExist = false;


                ClaimsPrincipal icp = Thread.CurrentPrincipal as ClaimsPrincipal;
                // Access IClaimsIdentity which contains claims
                ClaimsIdentity claimsIdentity = (ClaimsIdentity)icp.Identity;
                bool isAuthenticated = icp.Identity.IsAuthenticated;

                if (userInfoSession != null)
                {
                    userID = userInfoSession.UserID;
                    isExist = true;
                }
                else
                {
                    logoff();
                }

                if (isExist)
                {
                    //EmployeeService empServ = new EmployeeService();
                    //SecurityService secServ = new SecurityService();
                    UserService serv = new UserService();
                    M_User prof = serv.getUserInfoByID(userID);

                    MainConfigService mcServ = new MainConfigService();
                    _mainConig = mcServ.getMainConfig(_db);

                    if (prof != null)
                    {
                        //EmpSearchModel emp = new EmpSearchModel();
                        //emp.dto_focus = new EmpActionModel();
                        //emp.dto_focus.emp_code = prof.user_username;
                        //emp.dto_focus.prop_activeStat_act = true;
                        //List<EmpActionModel> lstEmp = empServ.searchByCriteria(_db, emp);
                        //if (lstEmp != null && lstEmp.Count() > 0)
                        //{
                        //    #region Account Role Mapping
                        //    AccRoleMappingService accRMServ = new AccRoleMappingService();
                        //    AccRoleMappingSearchModel accRMModel = new AccRoleMappingSearchModel();
                        //    accRMModel.dto_focus = new AccRoleMappingActionModel();
                        //    accRMModel.dto_focus.role_guid = prof.user_roleguid;
                        //    accRMModel.dto_focus.prop_activeStat_act = true;
                        //    List<AccRoleMappingActionModel> lstAccRM = accRMServ.searchByCriteria(_db, accRMModel);
                        //    #endregion

                            userInfoSession = new UserDetailSession(prof, serv.getUserRoleByRoleID(prof.user_roleGuid));
                           
                            _userInfo = userInfoSession;
                            Session.Add("UserInfoSession", userInfoSession);

                            setOWIN(userInfoSession);


                        //}
                        //else
                        //{
                        //    logoff();
                        //}

                    }
                    else
                    {
                        logoff();
                    }


                }
                base.ExecuteCore();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message.ToString());
            }
        }

        public JsonResult SerializeControl(string controlPath, object model)
        {
            ViewPage page = new ViewPage();
            ViewUserControl ctl = (ViewUserControl)page.LoadControl(controlPath);

            page.Controls.Add(ctl);
            page.ViewData.Model = model;
            page.ViewContext = new ViewContext();
            System.IO.StringWriter writer = new System.IO.StringWriter();
            System.Web.HttpContext.Current.Server.Execute(page, writer, false);
            string outputToReturn = writer.ToString();
            writer.Close();
            return this.Json(outputToReturn.Trim());
        }

        /// <summary>
        /// Called when an unhandled exception occurs in the action.
        /// </summary>
        /// <param name="filterContext">Information about the current request and action.</param>
        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception is UnauthorizedAccessException)
            {
                //
                // Manage the Unauthorized Access exceptions
                // by redirecting the user to Home page.
                //
                filterContext.ExceptionHandled = true;
                filterContext.Result = RedirectToAction("Home", "Index");
            }
            //
            base.OnException(filterContext);
        }

        public void logoff()
        {
            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            authenticationManager.SignOut();
            FormsAuthentication.SignOut();
            HttpContext.Session.Abandon();
            ViewBag.ReturnUrl = "";

            FormsAuthentication.RedirectToLoginPage();
        }

        public void setOWIN(UserDetailSession userInfo)
        {
            try
            {
                var identity = new ClaimsIdentity(new[] {
                        new Claim(ClaimTypes.NameIdentifier.ToString(),  userInfo.loginUserDetail.user_guid.ToString()),
                        new Claim(ClaimTypes.Name, userInfo.loginUserDetail.user_name),
                        new Claim(ClaimTypes.Role, userInfo.loginUserRole.roleGuid.ToString())
                    }, DefaultAuthenticationTypes.ApplicationCookie);

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;
                authManager.SignIn(identity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        #region rendor
        protected string RenderPartialViewToString()
        {
            return RenderPartialViewToString(null, null);
        }

        protected string RenderPartialViewToString(string viewName)
        {
            return RenderPartialViewToString(viewName, null);
        }

        protected string RenderPartialViewToString(object model)
        {
            return RenderPartialViewToString(null, model);
        }

        protected string RenderPartialViewToString(string viewName, object model = null)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            }

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        protected string RenderViewToString(string viewName, object model = null)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            }

            var viewData = new ViewDataDictionary(model);

            using (StringWriter sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindView(ControllerContext, viewName, null);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, viewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        protected string RenderViewToString<T>(string viewName, T model)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            }

            var viewData = new ViewDataDictionary<T>(model);

            using (StringWriter sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindView(ControllerContext, viewName, null);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, viewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }


        #endregion


    }

}
