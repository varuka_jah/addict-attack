﻿using ManagementSystem.Controllers.Base;
using ManagementSystem.Models;
using ManagementSystem.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManagementSystem.Controllers
{
    public class UploadFileController : BaseController
    {
        public ActionResult SMS_Upload()
        {
            Session.Remove("SMS_UPLOAD");
            UploadModel model = new UploadModel();
            //  SMSUpload serv = new SMSUpload();

            model.dtoData = new SMSUploadModel();
            model.valid = false;
            model.notValid = false;
            model.alreadyUpload = false;
            model.continueProcess = false;
            model.resultSuccess = false;
            model.resultError = false;
            model.message = "";


            return View(model);
        }

        [HttpPost]
        public JsonResult SMS_PreviewData(UploadModel model)
        {
            try
            {
                foreach (string item in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[item] as HttpPostedFileBase;
                    SMSUpload serv = new SMSUpload();
                   
                    int filterRecords = 0;
                    bool isValid = true;
                    List<SMSUploadModel> result = serv.readSmsFromFile(_db, file, out isValid);
                    Session.Add("SMS_UPLOAD", result);
                    filterRecords = (result == null ? 0 : result.Count());

                    model.lstData = new List<SMSUploadModel>();
                    model.isValid = isValid;
                    model.alreadyUpload = true;
                    model.valid = model.isValid;
                    model.notValid = !model.valid;
                    model.totalRecord = filterRecords;
                    return Json(new JSonResultService() { status = 200, message = "", dataResult = model }, JsonRequestBehavior.DenyGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.DenyGet);
            }

            return Json(new JSonResultService() { status = 500, message = "Internal Error", dataResult = model }, JsonRequestBehavior.DenyGet);
        }


        public JsonResult SMS_PreviewData_Paging(UploadPagingModel model)
        {
            try
            {
                if (model.continueProcess)
                {
                    List<SMSUploadModel> result = new List<SMSUploadModel>();
                    if (Session["SMS_UPLOAD"] != null)
                    {
                        result = (List<SMSUploadModel>)Session["SMS_UPLOAD"];
                    }

                    model.resultSuccess = false;
                    model.resultError = false;
                    model.message = "";
                    model.alreadyUpload = true;


                    if (result != null && result.Count() > 0)
                    {
                        return Json(new
                        {
                            draw = model.draw,
                            recordsTotal = result.Count(),
                            recordsFiltered = result.Count(),
                            data = result.OrderByDescending(s => s.prop_flagstat).ThenByDescending(s => s.createTime).Skip(model.start).Take(model.length)
                        });
                    }
                    else
                    {
                        return Json(new
                        {

                            draw = model.draw,
                            recordsTotal = 0,
                            recordsFiltered = 0,
                            data = new LinkedList<SMSUploadModel>()
                        });
                    }
                }
                else
                {
                    return Json(new
                    {
                        draw = model.draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        data = new LinkedList<SMSUploadModel>()
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.DenyGet);
            }
        }


        [HttpPost]
        public JsonResult SMS_saveUploadData(UploadModel model)
        {
            try
            {
                List<SMSUploadModel> sessionModel = new List<SMSUploadModel>();
                if (Session["SMS_UPLOAD"] == null)
                {
                    return Json(new JSonResultService() { status = 500, message = "ไม่สามารถบันทึกข้อมูลได้ กรุณาทำรายการอีกครั้ง", dataResult = null }, JsonRequestBehavior.DenyGet);
                }
                sessionModel = (List<SMSUploadModel>)Session["SMS_UPLOAD"];

                if (sessionModel == null)
                {
                    return Json(new JSonResultService() { status = 500, message = "Session model is null!!!", dataResult = null }, JsonRequestBehavior.DenyGet);
                }


                SMSUpload serv = new SMSUpload();
             
                serv.uploadSMSData(_db, _userInfo, sessionModel);
                Session.Remove("SMS_UPLOAD");

                model.resultSuccess = true;
                model.resultError = false;
                model.message = "นำข้อมูลเข้าเรียบร้อยค่ะ";
                model.alreadyUpload = false;

                return Json(new JSonResultService() { status = 200, message = "นำข้อมูลเข้าเรียบร้อยค่ะ", dataResult = model }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {
                model.resultSuccess = false;
                model.resultError = true;
                model.message = ex.Message.ToString();
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = model }, JsonRequestBehavior.DenyGet);
            }

        }


        // ==========================================
        public ActionResult COUPON_Upload()
        {
            Session.Remove("COUPON_UPLOAD");
            UploadModel model = new UploadModel();
           
            model.valid = false;
            model.notValid = false;
            model.alreadyUpload = false;
            model.continueProcess = false;
            model.resultSuccess = false;
            model.resultError = false;
            model.message = "";


            return View(model);
        }

        [HttpPost]
        public JsonResult COUPON_PreviewData(UploadModel model)
        {
            try
            {
                foreach (string item in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[item] as HttpPostedFileBase;
                    SMSUpload serv = new SMSUpload();

                    int filterRecords = 0;
                    bool isValid = true;
                    List<CouponUploadModel> result = serv.readCouponFromFile(_db, file, out isValid);
                    Session.Add("COUPON_UPLOAD", result);
                    filterRecords = (result == null ? 0 : result.Count());

                    model.lstCouponData = new List<CouponUploadModel>();
                    model.isValid = isValid;
                    model.alreadyUpload = true;
                    model.valid = model.isValid;
                    model.notValid = !model.valid;
                    model.totalRecord = filterRecords;
                    return Json(new JSonResultService() { status = 200, message = "", dataResult = model }, JsonRequestBehavior.DenyGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.DenyGet);
            }

            return Json(new JSonResultService() { status = 500, message = "Internal Error", dataResult = model }, JsonRequestBehavior.DenyGet);
        }


        public JsonResult COUPON_PreviewData_Paging(UploadPagingModel model)
        {
            try
            {
                if (model.continueProcess)
                {
                    List<CouponUploadModel> result = new List<CouponUploadModel>();
                    if (Session["COUPON_UPLOAD"] != null)
                    {
                        result = (List<CouponUploadModel>)Session["COUPON_UPLOAD"];
                    }

                    model.resultSuccess = false;
                    model.resultError = false;
                    model.message = "";
                    model.alreadyUpload = true;


                    if (result != null && result.Count() > 0)
                    {
                        return Json(new
                        {
                            draw = model.draw,
                            recordsTotal = result.Count(),
                            recordsFiltered = result.Count(),
                            data = result.OrderByDescending(s => s.prop_flagstat).ThenByDescending(s => s.createTime).Skip(model.start).Take(model.length)
                        });
                    }
                    else
                    {
                        return Json(new
                        {

                            draw = model.draw,
                            recordsTotal = 0,
                            recordsFiltered = 0,
                            data = new LinkedList<CouponUploadModel>()
                        });
                    }
                }
                else
                {
                    return Json(new
                    {
                        draw = model.draw,
                        recordsTotal = 0,
                        recordsFiltered = 0,
                        data = new LinkedList<CouponUploadModel>()
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.DenyGet);
            }
        }


        [HttpPost]
        public JsonResult COUPON_saveUploadData(UploadModel model)
        {
            try
            {
                List<CouponUploadModel> sessionModel = new List<CouponUploadModel>();
                if (Session["COUPON_UPLOAD"] == null)
                {
                    return Json(new JSonResultService() { status = 500, message = "ไม่สามารถบันทึกข้อมูลได้ กรุณาทำรายการอีกครั้ง", dataResult = null }, JsonRequestBehavior.DenyGet);
                }
                sessionModel = (List<CouponUploadModel>)Session["COUPON_UPLOAD"];

                if (sessionModel == null)
                {
                    return Json(new JSonResultService() { status = 500, message = "Session model is null!!!", dataResult = null }, JsonRequestBehavior.DenyGet);
                }


                SMSUpload serv = new SMSUpload();

                serv.uploadCouponData(_db, _userInfo, sessionModel);
                Session.Remove("COUPON_UPLOAD");

                model.resultSuccess = true;
                model.resultError = false;
                model.message = "นำข้อมูลเข้าเรียบร้อยค่ะ";
                model.alreadyUpload = false;

                return Json(new JSonResultService() { status = 200, message = "นำข้อมูลเข้าเรียบร้อยค่ะ", dataResult = model }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {
                model.resultSuccess = false;
                model.resultError = true;
                model.message = ex.Message.ToString();
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = model }, JsonRequestBehavior.DenyGet);
            }

        }

    }
}