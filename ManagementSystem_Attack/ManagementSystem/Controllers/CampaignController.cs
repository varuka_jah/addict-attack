﻿using ManagementSystem.Controllers.Base;
using ManagementSystem.Models;
using ManagementSystem.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManagementSystem.Controllers
{
    public class CampaignController : BaseController
    {
        public ActionResult List()
        {
            CampaignActionModel model = new CampaignActionModel();
            model.dtoCriteria = new CampaignSearchModel();
            model.dtoSearchResult = new CampaignSearchResultModel();
            model.lstSearchResult = new List<CampaignSearchResultModel>();

            return View(model);
        }

        public JsonResult Search(CampaignSearchModel model)
        {
            try
            {
                if (model == null)
                {
                    return Json(new JSonResultService() { status = 500, message = "ไม่พบเงื่อนไขการค้นหาข้อมูล", dataResult = null }, JsonRequestBehavior.AllowGet);
                }
                CampaignService serv = new CampaignService();
                List<CampaignSearchResultModel> lstResult = serv.searchByCriteria(_db, model);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstResult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }


        // GET: Campaign
        public ActionResult Create()
        {
            CampaignActionModel model = new CampaignActionModel();
            model.dtoCriteria = new CampaignSearchModel();
            model.dtoFocus = new CampaignModel();
            model.lstResult = new List<CampaignModel>();
            model.dtoFocus.dtoPremFocus = new CampaignPremiumModel();
            model.dtoFocus.dtoPremFocus.prem_img = new FileModel.FileAttachModel();

            model.dtoFocus.lstPremium = new List<CampaignPremiumModel>();
            model.dtoFocus.lstProduct = new List<CampaignProductModel>();

            model.dtoFocus.dtoProdFocus = new CampaignProductModel();
            model.dtoFocus.dtoProdFocus.prod_img = new FileModel.FileAttachModel();

            model.dtoFocus.camp_img = new FileModel.FileAttachModel();
            #region Product
            ProductService prodServ = new ProductService();
            ProductSearchModel prodModel = new ProductSearchModel();
            prodModel.status_act = true;
            List<ProductModel> lstProdResult = prodServ.searchByCriteria(_db, prodModel);
            model.lstSel_Prod = ConstUtil.ConvertUtil.generateDDL_Product(lstProdResult);
            #endregion

            #region Premium
            PremiumService premServ = new PremiumService();
            PremiumSearchModel premModel = new PremiumSearchModel();
            premModel.status_act = true;
            List<PremiumModel> lstPremResult = premServ.searchByCriteria(_db, premModel);
            model.lstSel_Prem = ConstUtil.ConvertUtil.generateDDL_Premium(lstPremResult);
            #endregion

            return View(model);
        }

        public JsonResult UploadFile()
        {
            try
            {
                ConstUtil.FileUtil fUtil;
                String newFileName = "";

                if (Request.Files != null && Request.Files.Count == 1)
                {
                    HttpPostedFileBase file = Request.Files[Request.Files.Keys[0]] as HttpPostedFileBase;

                    if (file != null && file.ContentLength > 0)
                    {
                        fUtil = new ConstUtil.FileUtil(file);
                        newFileName = fUtil.saveFileToServer(ConstUtil.ConstValue.UploadPath.campaign);
                    }
                }
                return Json(new JSonResultService() { status = 200, message = "", dataResult = newFileName, dataResult_2 = "" }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveNew(CampaignModel model)
        {
            try
            {
                CampaignService serv = new CampaignService();
                serv.create(_db, _userInfo, model);

                return Json(new JSonResultService() { status = 200, message = Url.Action("Detail", "Campaign", new { campGuid = model.camp_guid }), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Detail(String campGuid)
        {
            Guid campG;
            if (campGuid == null || !Guid.TryParse(campGuid, out campG))
            {
                return RedirectToAction("List");
            }

            CampaignService serv = new CampaignService();
            CampaignModel model = serv.getCampaignByGuId(_db, campG);
           
            return View(model);
        }

        public ActionResult Edit(String campGuid)
        {
            Guid campG;
            if (campGuid == null || !Guid.TryParse(campGuid, out campG))
            {
                return RedirectToAction("List");
            }

            CampaignService serv = new CampaignService();
            CampaignActionModel model = new CampaignActionModel();
            model.dtoCriteria = new CampaignSearchModel();
            model.dtoFocus = serv.getCampaignByGuId(_db, campG);
            model.lstResult = new List<CampaignModel>();
            model.dtoFocus.dtoPremFocus = new CampaignPremiumModel();
            model.dtoFocus.dtoPremFocus.prem_img = new FileModel.FileAttachModel();
           
            model.dtoFocus.dtoProdFocus = new CampaignProductModel();
            model.dtoFocus.dtoProdFocus.prod_img = new FileModel.FileAttachModel();

            model.dtoFocus.camp_img = new FileModel.FileAttachModel();
            model.dtoFocus.camp_img.ko_src = model.dtoFocus.camp_fullPath;

            #region Product
            ProductService prodServ = new ProductService();
            ProductSearchModel prodModel = new ProductSearchModel();
            prodModel.status_act = true;
            List<ProductModel> lstProdResult = prodServ.searchByCriteria(_db, prodModel);
            model.lstSel_Prod = ConstUtil.ConvertUtil.generateDDL_Product(lstProdResult);
            #endregion

            #region Premium
            PremiumService premServ = new PremiumService();
            PremiumSearchModel premModel = new PremiumSearchModel();
            premModel.status_act = true;
            List<PremiumModel> lstPremResult = premServ.searchByCriteria(_db, premModel);
            model.lstSel_Prem = ConstUtil.ConvertUtil.generateDDL_Premium(lstPremResult);
            #endregion

            return View(model);
        }

        public JsonResult SaveUpdate(CampaignModel model)
        {
            try
            {
                CampaignService serv = new CampaignService();
                serv.update(_db, _userInfo, model);

                return Json(new JSonResultService() { status = 200, message = Url.Action("Detail", "Campaign", new { campGuid = model.camp_guid }), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}