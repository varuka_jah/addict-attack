﻿using System.Web;
using System.Web.Optimization;

namespace AttackCampaign
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/theme/js/jquery.min.js",
                         "~/theme/js/bootstrap.bundle.min.js",
                          "~/Scripts/jquery.redirect.js",
                          "~/theme/js/moment.js"));

            bundles.Add(new ScriptBundle("~/bundles/theme/js").Include(
                       "~/theme/js/main.js"));
            
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/theme/css/bootstrap.min.css",
                      "~/theme/fonts/style.css",
                      "~/theme/css/main.css"));

            // knockout
            bundles.Add(new ScriptBundle("~/bundles/knockout/js").Include(
                "~/Scripts/knockout-3.4.2.js",
                "~/Scripts/knockout.mapping-latest.js",
                "~/Scripts/knockout.validation.js",
                "~/Scripts/Application/ko.binding.valueNumber.js"));

            // number onlny
            bundles.Add(new ScriptBundle("~/bundles/numberOnly/js").Include(
              "~/Scripts/jQuery.numberOnly.js"));

            // Select 2
            bundles.Add(new ScriptBundle("~/bundles/select2/js").Include(
              "~/theme/vendor/select2-4.0.7/dist/js/select2.js"));

            bundles.Add(new StyleBundle("~/bundles/select2/css").Include(
               "~/theme/vendor/select2-4.0.7/dist/css/select2.css"));

            // Ladda
            bundles.Add(new ScriptBundle("~/bundles/ladda/js").Include(
              "~/Scripts/spin.min.js",
              "~/Scripts/ladda.min.js",
              "~/Scripts/ladda.jquery.js"));

            bundles.Add(new StyleBundle("~/bundles/ladda/css").Include(
                     "~/Content/ladda.css"));

            // Toster
            bundles.Add(new ScriptBundle("~/bundles/toaster/js").Include(
              "~/theme/vendor/Toaster/js/jquery.notify.min.js"));

            bundles.Add(new StyleBundle("~/bundles/toaster/css").Include(
                     "~/theme/vendor/Toaster/css/jquery.notify.css"));

            // File Input
            bundles.Add(new ScriptBundle("~/bundles/fileInput/js").Include(
              "~/theme/vendor/CustomFileInputs/js/jquery.custom-file-input.js"));

            bundles.Add(new StyleBundle("~/bundles/fileInput/css").Include(
                     "~/theme/vendor/CustomFileInputs/css/normalize.css",
                     "~/theme/vendor/CustomFileInputs/css/demo.css",
                     "~/theme/vendor/CustomFileInputs/css/component.css"));
        }
    }
}
