﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttackCampaign.Models
{
    public class CustomerReceiptHistoryModel
    {
        public CustomerReceiptModel dtoData { get; set; }
        public List<CustomerReceiptModel> lstData { get; set; }
        public List<CustRecHisSummaryModel> lstSummaryData { get; set; }

    }

    public class CustRecHisSummaryModel
    {
        public String sumDesc { get; set; }
        public int totalQty { get; set; }
    }

    public class CustomerReceiptModel
    {
        public CustomerModel dtoCustomer { get; set; }
        public String custRec_pointAnnounce { get; set; }

        public System.Guid custRec_guid { get; set; }
        public System.Guid custRec_campGuid { get; set; }
        public System.Guid custRec_custGuid { get; set; }
     
        public double custRec_totalGainPoint { get; set; }
        public string custRec_status { get; set; }
        public string custRec_remark { get; set; }
        public System.Guid custRec_createBy { get; set; }
        public System.DateTime custRec_createDate { get; set; }
        public System.Guid custRec_updateBy { get; set; }
        public System.DateTime custRec_updateDate { get; set; }
        public String custRec_updateDate_str { get; set; }

        #region Campaign
        public Guid camp_guid { get; set; }
        public String camp_code { get; set; }
        public String camp_name { get; set; }
        public String camp_desc { get; set; }
        public DateTime camp_startDate_dt { get; set; }
        public String camp_startDate_str { get; set; }
        public DateTime camp_endDate_dt { get; set; }
        public String camp_endDate_str { get; set; }
        #endregion

        #region Customer
        public Guid cust_guid { get; set; }
        public String cust_fullname { get; set; }
        public String cust_firstname { get; set; }
        public String cust_lastname { get; set; }
        public String cust_email { get; set; }
        public String cust_tel { get; set; }
        public String cust_password { get; set; }
        public String cust_addr1 { get; set; }
        public String cust_addr2 { get; set; }


        #region Province
        public long provinceId { get; set; }
        public String provinceCode { get; set; }
        public String provinceNameTh { get; set; }
        public String provinceNameEn { get; set; }
        #endregion

        #region District
        public long distId { get; set; }
        public String distCode { get; set; }
        public String distNameTh { get; set; }
        public String distNameEn { get; set; }
        #endregion

        #region Sub District
        public long subDistId { get; set; }
        public String subDistNameTh { get; set; }
        public String subDistNameEn { get; set; }
        public String subDistZipcode { get; set; }
        #endregion
        #endregion


        #region Common Property
        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }

        public String prop_remark { get; set; }

        public DateTime prop_create_date_dt { get; set; }
        public String prop_create_date_str { get; set; }

        public Guid prop_create_by { get; set; }
        public String prop_create_by_name { get; set; }

        public DateTime prop_update_date_dt { get; set; }
        public String prop_update_date_str { get; set; }

        public Guid prop_update_by { get; set; }
        public String prop_update_by_name { get; set; }

        public String prop_errorMsg { get; set; }
        #endregion
    }

    public class AddrProviceModel
    {
        public long provinceId { get; set; }
        public String provinceCode { get; set; }
        public String provinceNameTh { get; set; }
        public String provinceNameEn { get; set; }
    }
    public class AddrDistrictModel
    {
        #region Province
        public long provinceId { get; set; }
        public String provinceCode { get; set; }
        public String provinceNameTh { get; set; }
        public String provinceNameEn { get; set; }
        #endregion

        public long distId { get; set; }
        public String distCode { get; set; }
        public String distNameTh { get; set; }
        public String distNameEn { get; set; }
    }
    public class AddrSubDistrictModel
    {
        #region Province
        public long provinceId { get; set; }
        public String provinceCode { get; set; }
        public String provinceNameTh { get; set; }
        public String provinceNameEn { get; set; }
        #endregion

        #region District
        public long distId { get; set; }
        public String distCode { get; set; }
        public String distNameTh { get; set; }
        public String distNameEn { get; set; }
        #endregion

        public long subDistId { get; set; }
        public String subDistNameTh { get; set; }
        public String subDistNameEn { get; set; }
        public String subDistZipcode { get; set; }
    }

}