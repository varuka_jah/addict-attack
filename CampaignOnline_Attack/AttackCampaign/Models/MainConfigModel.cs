﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttackCampaign.Models
{
    public class MainConfigModel
    {
        public bool isUseTel { get; set; }
        public int campaignGameRatio { get; set; }
        public String urlGameWin { get; set; }
        public String urlGameLoss { get; set; }
    }
}