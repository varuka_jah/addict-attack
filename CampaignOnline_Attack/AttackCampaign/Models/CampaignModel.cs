﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static AttackCampaign.Models.FileModel;

namespace AttackCampaign.Models
{
    public class CampaignSearchModel
    {
        public Guid camp_guid { get; set; }
        public String camp_code { get; set; }
        public String camp_name { get; set; }
        public DateTime camp_startDate_dt { get; set; }
        public String camp_startDate_str { get; set; }
        public DateTime camp_endDate_dt { get; set; }
        public String camp_endDate_str { get; set; }
        public DateTime camp_find_dt { get; set; }
        public String camp_find_str { get; set; }
    }

    public class CampaignActionModel
    {
        public Guid chooseCampaign { get; set; }
        public List<SelectListItem> lstSel_Campaign { get; set; }
        public List<CampaignSearchResultModel> lstSearchResult { get; set; }
    }

    public class CampaignSearchResultModel
    {
        public Guid camp_guid { get; set; }
        public String camp_code { get; set; }
        public String camp_name { get; set; }
        public String camp_desc { get; set; }
        public DateTime camp_startDate_dt { get; set; }
        public String camp_startDate_str { get; set; }
        public DateTime camp_endDate_dt { get; set; }
        public String camp_endDate_str { get; set; }
        public decimal camp_budgetAmt { get; set; }
        public decimal camp_receiptMinAmt { get; set; }
        public String camp_imgUrl { get; set; }
        public String camp_fullPath { get; set; }

    }

    public class CampaignProductModel
    {
        public bool isUpdate { get; set; }
        public Guid camp_guid { get; set; }
        public Guid campProd_guid { get; set; }
        public FileAttachModel prod_img { get; set; }
        public Guid prod_guid { get; set; }
        public String prod_name { get; set; }
        public String prod_code { get; set; }
        public Decimal prod_pricePerUnit { get; set; }
        public String prod_imgUrl { get; set; }
        public String prod_fullPath { get; set; }
        public double prod_point { get; set; }
        public bool prod_check { get; set; }


        public int quantity { get; set; }

        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }
    }
    public class CampaignPremiumModel
    {
        public bool isUpdate { get; set; }
        public Guid camp_guid { get; set; }
        public Guid campPrem_guid { get; set; }
        public FileAttachModel prem_img { get; set; }
        public Guid prem_guid { get; set; }
        public String prem_name { get; set; }
        public String prem_code { get; set; }
        public Decimal prem_pricePerUnit { get; set; }
        public int prem_outstandingQty { get; set; }
        public int prem_remainingQty { get; set; }
        public String prem_imgUrl { get; set; }
        public String prem_fullPath { get; set; }
        public DateTime prem_startDate_dt { get; set; }
        public String prem_startDate_str { get; set; }
        public DateTime prem_endDate_dt { get; set; }
        public String prem_endDate_str { get; set; }
        public PremiumModel prem_model { get; set; }
        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }

    }

    public class CampaignModel
    {
        public FileAttachModel camp_img { get; set; }

        public Guid camp_guid { get; set; }
        public String camp_code { get; set; }
        public String camp_name { get; set; }
        public String camp_desc { get; set; }
        public DateTime camp_startDate_dt { get; set; }
        public String camp_startDate_str { get; set; }
        public DateTime camp_endDate_dt { get; set; }
        public String camp_endDate_str { get; set; }
        public decimal camp_budgetAmt { get; set; }
        public decimal camp_receiptMinAmt { get; set; }
        public String camp_imgUrl { get; set; }
        public String camp_fullPath { get; set; }

        #region Common Property
        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }

        public String prop_remark { get; set; }

        public DateTime prop_create_date_dt { get; set; }
        public String prop_create_date_str { get; set; }

        public Guid prop_create_by { get; set; }
        public String prop_create_by_name { get; set; }

        public DateTime prop_update_date_dt { get; set; }
        public String prop_update_date_str { get; set; }

        public Guid prop_update_by { get; set; }
        public String prop_update_by_name { get; set; }

        public String prop_errorMsg { get; set; }
        #endregion
    }
    public class CampaignUse
    {
        public String receipt_imgUrl_1 { get; set; }
        public String receiptNo_1 { get; set; }
        public String receipt_imgUrl_2 { get; set; }
        public String receiptNo_2 { get; set; }
        public String receipt_imgUrl_3 { get; set; }
        public String receiptNo_3 { get; set; }
        public String receipt_imgUrl_4 { get; set; }
        public String receiptNo_4 { get; set; }
        public String receipt_imgUrl_5 { get; set; }
        public String receiptNo_5 { get; set; }
        public String branchNo_1 { get; set; }
        public String branchNo_2 { get; set; }
        public String branchNo_3 { get; set; }
        public String branchNo_4 { get; set; }
        public String branchNo_5 { get; set; }
        public CampaignModel dtoCamp { get; set; }
        public Guid chooseCampaign { get; set; }
    }
    public class CampaignUpload
    {
        public CampaignUse dtoData { get; set; }
        public bool ko_isEnableNext { get; set; }
        public bool isCheckTerm { get; set; }
        public Guid customerGrGuid { get; set; }

        public String imgSrcPreview { get; set; }
        public String branchNo_1 { get; set; }
        public String branchNo_2 { get; set; }
        public String branchNo_3 { get; set; }
        public String branchNo_4 { get; set; }
        public String branchNo_5 { get; set; }

        public FileAttachModel receipt_img_1 { get; set; }
        public String receipt_imgUrl_1 { get; set; }
        public String receipt_fullPath_1 { get; set; }
        public String receiptNo_1 { get; set; }

        public FileAttachModel receipt_img_2 { get; set; }
        public String receipt_imgUrl_2 { get; set; }
        public String receipt_fullPath_2 { get; set; }
        public String receiptNo_2 { get; set; }

        public FileAttachModel receipt_img_3 { get; set; }
        public String receipt_imgUrl_3 { get; set; }
        public String receipt_fullPath_3 { get; set; }
        public String receiptNo_3 { get; set; }

        public FileAttachModel receipt_img_4 { get; set; }
        public String receipt_imgUrl_4 { get; set; }
        public String receipt_fullPath_4 { get; set; }
        public String receiptNo_4 { get; set; }

        public FileAttachModel receipt_img_5 { get; set; }
        public String receipt_imgUrl_5 { get; set; }
        public String receipt_fullPath_5 { get; set; }
        public String receiptNo_5 { get; set; }


        public Guid chooseCampaign { get; set; }
        public String chooseCampaignName { get; set; }
        public CampaignModel dtoCamp { get; set; }

        public CustomerReceiptModel dtoCustRecp { get; set; }

        public List<AddrProviceModel> lstProvince { get; set; }
        public List<SelectListItem> lstSel_Province { get; set; }

        public List<AddrDistrictModel> lstDistrict { get; set; }
        public List<SelectListItem> lstSel_District { get; set; }

        public List<AddrDistrictModel> lstSubDistrict { get; set; }
        public List<SelectListItem> lstSel_SubDistrict { get; set; }

    }

}