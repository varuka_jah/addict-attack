﻿using AttackCampaign.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AttackCampaign.Models
{
    public class CustomerModel
    {
        public CustomerModel()
        {
            cust_customer = new M_Customer();
            cust_province = new M_Province();
            cust_district = new M_District();
            cust_subDistrict = new M_SubDistrict();
        }
        public String custRec_pointAnnounce { get; set; }

        public M_Customer cust_customer { get; set; }
        public M_Province cust_province { get; set; }
        public M_District cust_district { get; set; }
        public M_SubDistrict cust_subDistrict { get; set; }

        public List<AddrProviceModel> lstProvince { get; set; }
        public List<SelectListItem> lstSel_Province { get; set; }

        public List<AddrDistrictModel> lstDistrict { get; set; }
        public List<SelectListItem> lstSel_District { get; set; }

        public List<AddrDistrictModel> lstSubDistrict { get; set; }
        public List<SelectListItem> lstSel_SubDistrict { get; set; }

        #region Common Property
        public String prop_activeStat { get; set; }
        public String prop_activeStat_disp { get; set; }
        public bool prop_activeStat_act { get; set; }
        public bool prop_activeStat_ina { get; set; }

        public String prop_remark { get; set; }

        public DateTime prop_create_date_dt { get; set; }
        public String prop_create_date_str { get; set; }

        public Guid prop_create_by { get; set; }
        public String prop_create_by_name { get; set; }

        public DateTime prop_update_date_dt { get; set; }
        public String prop_update_date_str { get; set; }

        public Guid prop_update_by { get; set; }
        public String prop_update_by_name { get; set; }

        public String prop_errorMsg { get; set; }
        #endregion
    }

}