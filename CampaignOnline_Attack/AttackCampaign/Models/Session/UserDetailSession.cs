﻿using AttackCampaign.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace AttackCampaign.Models.Session
{
    public class UserDetailSession
    {
        public M_Customer loginUserDetail { get; set; }

        public UserDetailSession(M_Customer user)
        {
            this.loginUserDetail = user;
        }

        /// <summary>
        /// Log off the current user.
        /// </summary>
        /// <param name="httpSession">The current HTTP session.</param>
        public static void LogOff(HttpSessionStateBase httpSession)
        {
            UserDetailSession userInfoSession = (httpSession["UserInfoSession"] == null ? null : (UserDetailSession)httpSession["UserInfoSession"]);

            FormsAuthentication.SignOut();
            httpSession["userInfoSession"] = null;
        }

    }
}