﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttackCampaign.Models
{
    public class LoginActionModel
    {
        public LoginModel dtoLogin { get; set; }
        public RegisterModel dtoReg { get; set; }
    }
    public class LoginModel
    {
        public String userName { get; set; }
        public String password { get; set; }
        public String tel { get; set; }
    }
    public class RegisterModel
    {
        public String name { get; set; }
        public String username { get; set; }
        public String lastName { get; set; }
        public String tel { get; set; }

    }
}