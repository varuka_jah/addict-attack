﻿using AttackCampaign.ConstUtil;
using AttackCampaign.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttackCampaign.Models.Service
{
    public class MainConfigService
    {
        public MainConfigModel getMainConfig(Entities db)
        {
            try
            {
                MainConfigModel dtoMC = new MainConfigModel();

                var lstMainConfig = (from m in db.M_MainConfig
                                     select m).ToList();


                String useFlag = (from m in lstMainConfig
                                  where m.configCode.Equals(ConstValue.MainConfig.isUseTel)
                                  select m.configValue).SingleOrDefault();
                dtoMC.isUseTel = (useFlag.Equals(ConstValue.Flag.yes) ? true : false);

                String campaignRatio = (from m in lstMainConfig
                                        where m.configCode.Equals(ConstValue.MainConfig.campaignGameRatio)
                                        select m.configValue).SingleOrDefault();
                dtoMC.campaignGameRatio = int.Parse(campaignRatio);


                dtoMC.urlGameWin = (from m in lstMainConfig
                                    where m.configCode.Equals(ConstValue.MainConfig.urlGameWin)
                                    select m.configValue).SingleOrDefault();

                dtoMC.urlGameLoss = (from m in lstMainConfig
                                     where m.configCode.Equals(ConstValue.MainConfig.urlGameLoss)
                                     select m.configValue).SingleOrDefault();

                return dtoMC;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

    }
}