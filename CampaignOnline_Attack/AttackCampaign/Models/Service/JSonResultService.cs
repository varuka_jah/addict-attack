﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace AttackCampaign.Models.Service
{
    public class JSonResultService : JsonResult
    {
        const string JsonRequest_GetNotAllowed = "This request has been blocked because sensitive information could be disclosed to third party web sites when this is used in a GET request. To allow GET requests, set JsonRequestBehavior to AllowGet.";

        public JSonResultService()
        {
            MaxJsonLength = Int32.MaxValue;
            RecursionLimit = 100;
        }

        public String fileName1 { get; set; }
        public String fileName2 { get; set; }
        public String fileName3 { get; set; }
        public String fileName4 { get; set; }
        public String fileName5 { get; set; }

        public object dataResult { get; set; }
        public object dataResult_2 { get; set; }

        public object dataResult_text { get; set; }

        public object dataResult_value { get; set; }
        public int status { get; set; }
        public String message { get; set; }
        public bool isValid { get; set; }

        public int MaxJsonLength1 { get; set; }
        public int RecursionLimit1 { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            if (JsonRequestBehavior == JsonRequestBehavior.DenyGet &&
                String.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException(JsonRequest_GetNotAllowed);
            }

            HttpResponseBase response = context.HttpContext.Response;

            if (!String.IsNullOrEmpty(ContentType))
            {
                response.ContentType = ContentType;
            }
            else
            {
                response.ContentType = "application/json";
            }
            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }
            if (Data != null)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer() { MaxJsonLength = MaxJsonLength1, RecursionLimit = RecursionLimit1 };
                response.Write(serializer.Serialize(Data));
            }
        }
    }

    public static class CoreWebHelper
    {
        public static ContentResult GetJsonResult(Object obj)
        {
            ContentResult result = new ContentResult();
            result.ContentEncoding = Encoding.UTF8;
            result.ContentType = "application/json";
            result.Content = JsonConvert.SerializeObject(obj);
            return result;
        }
    }
}