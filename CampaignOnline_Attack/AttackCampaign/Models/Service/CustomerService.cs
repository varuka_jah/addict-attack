﻿using AttackCampaign.ConstUtil;
using AttackCampaign.Data;
using AttackCampaign.Models.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttackCampaign.Models.Service
{
    public class CustomerService
    {
        public String register(RegisterModel model)
        {
            try
            {
                using (Entities ctx = new Entities())
                {
                    ctx.Database.Connection.Open();

                    M_Customer dto = (from c in ctx.M_Customer
                                      where c.cust_tel == model.tel
                                      && c.cust_status == ConstUtil.ConstValue.Status.active
                                      select c).SingleOrDefault<M_Customer>();

                    if (dto != null)
                    {
                        throw new Exception(MsgConst.existTelNo);
                    }

                    dto = new M_Customer();
                    dto.cust_guid = Guid.NewGuid();
                    dto.cust_addr1 = "";
                    dto.cust_addr2 = "";
                    dto.cust_createBy = dto.cust_guid;
                    dto.cust_createDate = DateTime.Now;
                    dto.cust_distId = 0;
                    dto.cust_email = "";
                    dto.cust_firstName = model.name.Trim();
                    dto.cust_lastName = model.lastName.Trim();
                    dto.cust_password = "";
                    dto.cust_provId = 0;
                    dto.cust_remark = "";
                    dto.cust_status = ConstValue.Status.active;
                    dto.cust_subId = 0;
                    dto.cust_tel = model.tel.Trim();
                    dto.cust_updateBy = dto.cust_guid;
                    dto.cust_updateDate = DateTime.Now;

                    ctx.M_Customer.Add(dto);
                    ctx.SaveChanges();

                    return dto.cust_tel;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        public UserDetailSession getLogin(LoginModel model)
        {
            try
            {
                using (Entities ctx = new Entities())
                {
                    ctx.Database.Connection.Open();

                    MainConfigService mcServ = new MainConfigService();
                    MainConfigModel mcModel = mcServ.getMainConfig(ctx);
                    if (mcModel.isUseTel)
                    {
                        M_Customer dto = (from c in ctx.M_Customer
                                          where c.cust_tel == model.tel
                                          && c.cust_status == ConstUtil.ConstValue.Status.active
                                          select c).SingleOrDefault<M_Customer>();
                        if (dto == null)
                        {
                            throw new Exception(MsgConst.userNotFound);
                        }

                        UserDetailSession userInfo = new UserDetailSession(dto);
                        return userInfo;
                    }
                    else
                    {
                        String passEn = ConstUtil.ConvertUtil.GetMyEncryptPassword(model.password.Trim());
                        M_Customer dto = (from c in ctx.M_Customer
                                          where c.cust_tel == model.tel
                                                   && c.cust_password == passEn
                                                   && c.cust_status == ConstUtil.ConstValue.Status.active
                                          select c).SingleOrDefault<M_Customer>();
                        if (dto == null)
                        {
                            throw new Exception(MsgConst.userNotFound);
                        }

                        UserDetailSession userInfo = new UserDetailSession(dto);
                        return userInfo;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        public M_Customer getCustomerInfoByID(Guid custGuid)
        {
            try
            {
                using (Entities ctx = new Entities())
                {
                    M_Customer dto = (from us in ctx.M_Customer
                                      where us.cust_guid == custGuid
                                      select us).SingleOrDefault();

                    return dto;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public CustomerModel getCustomerModelInfoByID(Entities ctx, Guid custGuid)
        {
            try
            {
                CustomerModel custModel = new CustomerModel();

                custModel.cust_customer = (from us in ctx.M_Customer
                                           where us.cust_guid == custGuid
                                           select us).SingleOrDefault();

                if (custModel.cust_customer.cust_provId > 0)
                {
                    custModel.cust_province = (from us in ctx.M_Province
                                               where us.provId == custModel.cust_customer.cust_provId
                                               select us).SingleOrDefault();


                }
                else
                {
                    custModel.cust_province = new M_Province();
                }

                if (custModel.cust_customer.cust_distId > 0)
                {
                    custModel.cust_district = (from us in ctx.M_District
                                               where us.distId == custModel.cust_customer.cust_distId
                                               select us).SingleOrDefault();
                }
                else
                {
                    custModel.cust_district = new M_District();
                }

                if (custModel.cust_customer.cust_subId > 0)
                {
                    custModel.cust_subDistrict = (from us in ctx.M_SubDistrict
                                                  where us.subDistId == custModel.cust_customer.cust_subId
                                                  select us).SingleOrDefault();
                }
                else
                {
                    custModel.cust_subDistrict = new M_SubDistrict();
                }

                return custModel;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<AddrProviceModel> getProvince(Entities db)
        {
            try
            {
                var prov = (from m in db.M_Province
                            select new AddrProviceModel()
                            {
                                provinceCode = m.provCode,
                                provinceId = m.provId,
                                provinceNameEn = m.provNameEn,
                                provinceNameTh = m.provNameTh
                            });


                List<AddrProviceModel> lstResult = prov.OrderBy(s => s.provinceNameTh).ToList<AddrProviceModel>();
                return lstResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<AddrDistrictModel> getDistrictByProv(long provinceId)
        {
            try
            {
                using (Entities ctx = new Entities())
                {
                    var prov = (from a in ctx.M_District
                                join m in ctx.M_Province on a.province_id equals m.provId
                                where m.provId == provinceId
                                select new AddrDistrictModel()
                                {
                                    distCode = a.distCode,
                                    distId = a.distId,
                                    distNameEn = a.name_en,
                                    distNameTh = a.name_th,
                                    provinceCode = m.provCode,
                                    provinceId = m.provId,
                                    provinceNameEn = m.provNameEn,
                                    provinceNameTh = m.provNameTh
                                });
                    List<AddrDistrictModel> lstResult = prov.OrderBy(s => s.distNameTh).ToList<AddrDistrictModel>();
                    return lstResult;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public List<AddrSubDistrictModel> getSubDistrictByDistrict(long districtId)
        {
            try
            {
                using (Entities ctx = new Entities())
                {
                    var prov = (from b in ctx.M_SubDistrict
                                join a in ctx.M_District on b.subDistDistId equals a.distId
                                join m in ctx.M_Province on a.province_id equals m.provId
                                where b.subDistDistId == districtId
                                select new AddrSubDistrictModel()
                                {
                                    subDistId = b.subDistId,
                                    subDistNameEn = b.subDistNameEn,
                                    subDistNameTh = b.subDistNameTh,
                                    subDistZipcode = b.subDistZipCode,
                                    distCode = a.distCode,
                                    distId = a.distId,
                                    distNameEn = a.name_en,
                                    distNameTh = a.name_th,
                                    provinceCode = m.provCode,
                                    provinceId = m.provId,
                                    provinceNameEn = m.provNameEn,
                                    provinceNameTh = m.provNameTh
                                });


                    List<AddrSubDistrictModel> lstResult = prov.OrderBy(s => s.subDistNameTh).ToList<AddrSubDistrictModel>();
                    return lstResult;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public void updateCustomerAddress(Entities ctx, M_Customer customer)
        {
            try
            {

                var m_customer = ctx.M_Customer.Find(customer.cust_guid);
                if (m_customer == null)
                {
                    throw new Exception("ไม่พบข้อมูลลูกค้าค่ะ");
                }

                m_customer.cust_firstName = customer.cust_firstName.Trim();
                m_customer.cust_lastName = customer.cust_lastName.Trim();
                m_customer.cust_tel = customer.cust_tel.Trim();
                m_customer.cust_addr1 = customer.cust_addr1;
                m_customer.cust_addr2 = customer.cust_addr2;
                m_customer.cust_provId = customer.cust_provId;
                m_customer.cust_distId = customer.cust_distId;
                m_customer.cust_subId = customer.cust_subId;
                m_customer.cust_updateBy = customer.cust_guid;
                m_customer.cust_updateDate = DateTime.Now;

                ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

    }
}