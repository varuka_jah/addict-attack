﻿using AttackCampaign.ConstUtil;
using AttackCampaign.Data;
using AttackCampaign.Models.Session;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using static AttackCampaign.ConstUtil.ConstValue;

namespace AttackCampaign.Models.Service
{
    public class CampaignService
    {
        public List<CampaignSearchResultModel> searchByCriteria(Entities db, CampaignSearchModel model)
        {
            try
            {
                var data = (from ca in db.T_Campaign
                            join create in db.M_User on ca.camp_createBy equals create.user_guid
                            join update in db.M_User on ca.camp_updateBy equals update.user_guid
                            where ca.camp_status.Equals(ConstUtil.ConstValue.Status.active)
                            select new CampaignSearchResultModel()
                            {
                                camp_budgetAmt = ca.camp_budgetAmt,
                                camp_code = ca.camp_code,
                                camp_desc = ca.camp_desc,
                                camp_endDate_dt = ca.camp_endDate,
                                camp_fullPath = ConstUtil.ConstValue.UploadPath.campaign + ca.camp_imgUrl,
                                camp_guid = ca.camp_guid,
                                camp_imgUrl = ca.camp_imgUrl,
                                camp_name = ca.camp_name,
                                camp_receiptMinAmt = ca.camp_receiptMinAmt,
                                camp_startDate_dt = ca.camp_startDate
                            });

                if (model.camp_code != null && !model.camp_code.Trim().Equals(""))
                {
                    data = data.Where(s => s.camp_code.Contains(model.camp_code.Trim()));
                }

                if (model.camp_name != null && !model.camp_name.Trim().Equals(""))
                {
                    data = data.Where(s => s.camp_name.Contains(model.camp_name.Trim()));
                }

                if (model.camp_guid != null && !model.camp_guid.Equals(Guid.Empty))
                {
                    data = data.Where(s => s.camp_guid.Equals(model.camp_guid));
                }

                if (model.camp_find_str != null && !model.camp_find_str.Trim().Equals(""))
                {
                    DateTime dt;
                    DateUtil.convertFromString_MDY(model.camp_find_str, out dt);
                    data = data.Where(s => s.camp_startDate_dt.CompareTo(dt) <= 0);
                    data = data.Where(s => s.camp_endDate_dt.CompareTo(dt) >= 0);
                }

                if (model.camp_find_dt != null && model.camp_find_dt.Year > 2000)
                {
                    data = data.Where(s => s.camp_startDate_dt.CompareTo(model.camp_find_dt) <= 0);
                    data = data.Where(s => s.camp_endDate_dt.CompareTo(model.camp_find_dt) >= 0);
                }

                List<CampaignSearchResultModel> lstResult = data.OrderByDescending(s => s.camp_startDate_dt).ToList<CampaignSearchResultModel>();
                return lstResult;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public CampaignModel getCampaignByGuId(Entities db, Guid campGuid)
        {
            try
            {
                var data = (from ca in db.T_Campaign
                            join create in db.M_User on ca.camp_createBy equals create.user_guid
                            join update in db.M_User on ca.camp_updateBy equals update.user_guid
                            where ca.camp_guid.Equals(campGuid)
                            && ca.camp_status.Equals(ConstValue.Status.active)
                            select new CampaignModel()
                            {
                                prop_activeStat = ca.camp_status,
                                camp_budgetAmt = ca.camp_budgetAmt,
                                camp_code = ca.camp_code,
                                camp_desc = ca.camp_desc,
                                camp_endDate_dt = ca.camp_endDate,
                                camp_fullPath = ConstUtil.ConstValue.UploadPath.campaign + ca.camp_imgUrl,
                                camp_guid = ca.camp_guid,
                                camp_imgUrl = ca.camp_imgUrl,
                                camp_name = ca.camp_name,
                                camp_receiptMinAmt = ca.camp_receiptMinAmt,
                                camp_startDate_dt = ca.camp_startDate,
                                prop_remark = ca.camp_remark
                            }).SingleOrDefault();

                return data;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public Guid insertReceipt(Entities db, CampaignUse model, UserDetailSession userInfo)
        {
            try
            {
                String isExist_1 = checkRecExist(db, model.receiptNo_1, model.branchNo_1);
                String isExist_2 = checkRecExist(db, model.receiptNo_2, model.branchNo_2);
                String isExist_3 = checkRecExist(db, model.receiptNo_3, model.branchNo_3);
                String isExist_4 = checkRecExist(db, model.receiptNo_4, model.branchNo_4);
                String isExist_5 = checkRecExist(db, model.receiptNo_5, model.branchNo_5);

                if (isExist_1.Equals("YES") || isExist_2.Equals("YES") || isExist_3.Equals("YES") || isExist_4.Equals("YES") || isExist_5.Equals("YES"))
                {
                    throw new Exception("ขออภัยค่ะ ไม่สามารถทำรายการได้ เนื่องจากพบเลขที่ใบเสร็จในระบบ");
                }
                if (isExist_1.Equals("EMP") && isExist_2.Equals("EMP") && isExist_3.Equals("EMP") && isExist_4.Equals("EMP") && isExist_5.Equals("EMP"))
                {
                    throw new Exception("ขออภัยค่ะ ไม่สามารถทำรายการได้ กรุณาระบุเลขที่ใบเสร็จที่ต้องการอัพโหลด");
                }
                if (model.receipt_imgUrl_1.Equals("") && model.receipt_imgUrl_2.Equals("") && model.receipt_imgUrl_3.Equals("") && model.receipt_imgUrl_4.Equals("") && model.receipt_imgUrl_5.Equals(""))
                {
                    throw new Exception("ขออภัยค่ะ ไม่สามารถทำรายการได้ กรุณาอัพโหลดใบเสร็จ");
                }

                T_CustomerReceiptGroup custGr = new T_CustomerReceiptGroup();
                custGr.custRecGr_campGuid = model.chooseCampaign;
                custGr.custRecGr_createBy = userInfo.loginUserDetail.cust_guid;
                custGr.custRecGr_createDate = DateTime.Now;
                custGr.custRecGr_custGuid = userInfo.loginUserDetail.cust_guid;
                custGr.custRecGr_guid = Guid.NewGuid();
                custGr.custRecGr_remark = "";
                custGr.custRecGr_status = ConstValue.Status.pending;
                custGr.custRecGr_updateBy = userInfo.loginUserDetail.cust_guid;
                custGr.custRecGr_updateDate = DateTime.Now;
                db.T_CustomerReceiptGroup.Add(custGr);

                T_CustomerRec custRec1 = prepareCustRec( userInfo, isExist_1, custGr.custRecGr_guid, model.receipt_imgUrl_1, model.receiptNo_1, model.branchNo_1);
                T_CustomerRec custRec2 = prepareCustRec(userInfo, isExist_2, custGr.custRecGr_guid, model.receipt_imgUrl_2, model.receiptNo_2, model.branchNo_2);
                T_CustomerRec custRec3 = prepareCustRec(userInfo, isExist_3, custGr.custRecGr_guid, model.receipt_imgUrl_3, model.receiptNo_3, model.branchNo_3);
                T_CustomerRec custRec4 = prepareCustRec(userInfo, isExist_4, custGr.custRecGr_guid, model.receipt_imgUrl_4, model.receiptNo_4, model.branchNo_4);
                T_CustomerRec custRec5 = prepareCustRec(userInfo, isExist_5, custGr.custRecGr_guid, model.receipt_imgUrl_5, model.receiptNo_5, model.branchNo_5);

                if (custRec1 == null && custRec2 == null && custRec3 == null && custRec4 == null && custRec5 == null)
                {
                    throw new Exception("กรุณาระบุเลขที่ใบเสร็จและอัพโหลดรูปภาพ");
                }
                if (custRec1 != null)
                {
                    db.T_CustomerRec.Add(custRec1);
                }
                if (custRec2 != null)
                {
                    db.T_CustomerRec.Add(custRec2);
                }
                if (custRec3 != null)
                {
                    db.T_CustomerRec.Add(custRec3);
                }
                if (custRec4 != null)
                {
                    db.T_CustomerRec.Add(custRec4);
                }
                if (custRec5 != null)
                {
                    db.T_CustomerRec.Add(custRec5);
                }
                
                db.SaveChanges();

                return custGr.custRecGr_guid;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public Guid updateStatusCustGr(Entities db, Guid custGrGuid, String newStat, String oldStat, UserDetailSession userInfo)
        {
            try
            {
                var custGr = (from gr in db.T_CustomerReceiptGroup
                              where gr.custRecGr_guid.Equals(custGrGuid)
                              && gr.custRecGr_status.Equals(oldStat)
                              select gr).SingleOrDefault();

                if (custGr == null)
                {
                    throw new Exception("ไม่พบรายการที่ท่านทำรายการ กรุณาอัพโหลดใบเสร็จใหม่อีกครั้งค่ะ");
                }
                custGr.custRecGr_status = newStat;
                custGr.custRecGr_updateDate = DateTime.Now;
                custGr.custRecGr_updateBy = userInfo.loginUserDetail.cust_guid;
                db.SaveChanges();

                return custGr.custRecGr_custGuid;

            } catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        private static T_CustomerRec prepareCustRec(UserDetailSession userInfo, string isExist, Guid custGrGuid, String receipt_imgUrl, String receiptNo, String branchName)
        {
            if (isExist.Equals("NO") && !receipt_imgUrl.Equals(""))
            {
                T_CustomerRec custRec = new T_CustomerRec();
                custRec.custRec_createBy = userInfo.loginUserDetail.cust_guid;
                custRec.custRec_createDate = DateTime.Now;
                custRec.custRec_branchName = branchName.Trim().ToUpper();
                custRec.custRec_grGuid = custGrGuid;
                custRec.custRec_guid = Guid.NewGuid();
                custRec.custRec_imgUrl = receipt_imgUrl;
                custRec.custRec_recpNo = receiptNo.Trim().ToUpper();
                custRec.custRec_remark = "";
                custRec.custRec_status = ConstValue.Status.init;
                custRec.custRec_updateBy = userInfo.loginUserDetail.cust_guid;
                custRec.custRec_updateDate = DateTime.Now;
                return custRec;
            }

            return null;
        }

        private String checkRecExist(Entities db, String recNo, String branchNo)
        {
            String isExist = "";
            if (recNo != null && !recNo.Trim().Equals(""))
            {
                bool result = checkExistReceiptNo(db, recNo.Trim().ToUpper(), branchNo);
                if (result)
                {
                    isExist = "YES";
                } else
                {
                    isExist = "NO";
                }
            } else
            {
                isExist = "EMP";
            }
            return isExist;
        }

        public bool checkExistReceiptNo(Entities db, String receiptNo,  String branchNo)
        {
            try
            {
                branchNo = (branchNo == null ? "" : branchNo.Trim());

                var result = (from gr in db.T_CustomerReceiptGroup
                              join rec in db.T_CustomerRec on gr.custRecGr_guid equals rec.custRec_grGuid
                              where rec.custRec_recpNo.Equals(receiptNo.Trim().ToUpper())
                              && rec.custRec_branchName.Equals(branchNo.Trim().ToUpper())
                              select rec).SingleOrDefault();

                if (result == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        // public CustomerReceiptModel getCustomerReceiptByGuid(Entities db, Guid custReceipt_guid)
        // {
        //     try
        //     {
        //         var data = (from cr in db.T_CustomerReceipt
        //                     join cust in db.M_Customer on cr.custRec_custGuid equals cust.cust_guid
        //                     where cr.custRec_guid.Equals(custReceipt_guid)
        //                     select new CustomerReceiptModel()
        //                     {
        //                         cust_guid = cr.custRec_custGuid,
        //                         custRec_guid = cr.custRec_guid,
        //                         custRec_campGuid = cr.custRec_campGuid,
        //                         custRec_storeGuid = cr.custRec_storeGuid,
        //                         custRec_custGuid = cr.custRec_custGuid,
        //                         custRec_branchName = cr.custRec_branchName,
        //                         custRec_recpImgUrl = ConstUtil.ConstValue.UploadPath.premium + cr.custRec_recpImgUrl,
        //                         custRec_recpNo = cr.custRec_recpNo,
        //                         custRec_totalGainPoint = cr.custRec_totalGainPoint,
        //                         custRec_gameResult = cr.custRec_gameResult,
        //                         custRec_status = cr.custRec_status,
        //                         custRec_remark = cr.custRec_remark,
        //                         custRec_createBy = cr.custRec_createBy,
        //                         custRec_createDate = cr.custRec_createDate,
        //                         custRec_updateBy = cr.custRec_updateBy,
        //                         custRec_updateDate = cr.custRec_updateDate,
        //                         cust_addr1 = cust.cust_addr1, 
        //                         cust_addr2 = cust.cust_addr2, 
        //                         cust_email = cust.cust_email,
        //                         cust_firstname = cust.cust_firstName, 
        //                         cust_fullname = cust.cust_firstName +  " " + cust.cust_lastName,
        //                         cust_lastname = cust.cust_lastName,
        //                         cust_tel = cust.cust_tel
        //                     }).SingleOrDefault();

        //         data.prop_create_date_str =  data.custRec_createDate.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        //         data.prop_update_date_str = data.custRec_updateDate.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        //         return data;
        //     }
        //     catch (Exception ex)
        //     {
        //         throw new Exception(ex.Message.ToString());
        //     }
        // }

        // public void update_Status_CustomerReceiptByGuid(Entities db, Guid custReceipt_guid, String gameResult, String status, Guid customerGuid, bool saveChange)
        // {
        //     try
        //     {
        //         var data = db.T_CustomerReceipt.Find(custReceipt_guid);

        //         if (!String.IsNullOrEmpty(gameResult))
        //         {
        //             data.custRec_gameResult = gameResult;
        //         }

        //         data.custRec_status = status;
        //         data.custRec_updateBy = customerGuid;
        //         data.custRec_updateDate = DateTime.Now;
        //         if (saveChange)
        //         {
        //             db.SaveChanges();
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         throw new Exception(ex.Message.ToString());
        //     }
        // }

        // public void updateDecreasedRemainingQtyCampaignPremium(Entities db, Guid cust_Guid, Guid campaignPremium_Guid, bool saveChange)
        // {
        //     try
        //     {
        //         var campPrem = (from s in db.T_CampaignPremium
        //                         where s.campPrem_guid == campaignPremium_Guid
        //                         select s).SingleOrDefault();

        //         if (campPrem == null)
        //         {
        //             throw new Exception("not found campaign premium data for update");
        //         }

        //         campPrem.campPrem_remaining = campPrem.campPrem_remaining - 1;
        //         campPrem.campPrem_updateBy = cust_Guid;
        //         campPrem.campPrem_updateDate = DateTime.Now;

        //         if (saveChange)
        //         {
        //             db.SaveChanges();
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         throw new Exception(ex.Message.ToString());
        //     }
        // }

        public CustomerReceiptHistoryModel getCustomerReceiptHistory(Entities db, Guid custGuid)
        {
            try
            {
                DateTime today = DateTime.Today, today_end = DateTime.Today.AddDays(-10);
                CustomerReceiptHistoryModel model = new CustomerReceiptHistoryModel();

                var lstReceipt = (from cr in db.T_CustomerReceiptGroup
                                  join cmp in db.T_Campaign on cr.custRecGr_campGuid equals cmp.camp_guid
                                  where cr.custRecGr_custGuid.Equals(custGuid)
                                  && cmp.camp_startDate.CompareTo(today) <= 0
                                  && cmp.camp_endDate.CompareTo(today_end) >= 0
                                  select new CustomerReceiptModel()
                                  {
                                      custRec_guid = cr.custRecGr_guid,
                                    //  custRec_totalGainPoint = cr.custRec_totalGainPoint,
                                      custRec_status = cr.custRecGr_status,
                                      custRec_remark = cr.custRecGr_remark,
                                      custRec_createBy = cr.custRecGr_createBy,
                                      custRec_createDate = cr.custRecGr_createDate,
                                      custRec_updateBy = cr.custRecGr_updateBy,
                                      custRec_updateDate = cr.custRecGr_updateDate,
                                      custRec_campGuid = cr.custRecGr_campGuid,
                                      camp_code = cmp.camp_code,
                                      camp_desc = cmp.camp_desc,
                                      camp_endDate_dt = cmp.camp_endDate,
                                      camp_guid = cmp.camp_guid,
                                      camp_name = cmp.camp_name,
                                      camp_startDate_dt = cmp.camp_startDate,
                                      cust_guid = cr.custRecGr_custGuid
                                  }).OrderByDescending(s => s.custRec_updateDate).ToList<CustomerReceiptModel>();

                CustRecHisSummaryModel dtoPass = new CustRecHisSummaryModel(),
                    dtoWait = new CustRecHisSummaryModel(),
                    dtoRej = new CustRecHisSummaryModel();

                dtoWait.sumDesc = "จำนวนสิทธิที่รอการตรวจสอบ";
                dtoPass.sumDesc = "จำนวนสิทธิรวมที่ผ่านการอนุมัติ";
                dtoRej.sumDesc = "จำนวนสิทธิที่ไม่ผ่านการอนุมัติ";

                if (lstReceipt != null && lstReceipt.Count() > 0)
                {
                    foreach (var item in lstReceipt)
                    {
                      
                        item.cust_fullname = item.cust_firstname + " " + item.cust_lastname;
                        item.custRec_updateDate_str = item.custRec_updateDate.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                        var allRec = (from rec in db.T_CustomerRec
                                      where rec.custRec_grGuid.Equals(item.custRec_guid)
                                      select rec).ToList();

                        item.custRec_totalGainPoint = (allRec == null ? 0 : allRec.Count());

                        item.prop_activeStat_disp = (item.custRec_status.Equals(ConstValue.Status.success) ? ConstValue.Status.success_disp :
                            item.custRec_status.Equals(ConstValue.Status.init) ? ConstValue.Status.init_disp :
                            item.custRec_status.Equals(ConstValue.Status.reserved) ? ConstValue.Status.reserv_disp :
                            item.custRec_status.Equals(ConstValue.Status.active) ? ConstValue.Status.act_disp :
                            item.custRec_status.Equals(ConstValue.Status.reject) ? ConstValue.Status.rej_disp : ConstValue.Status.oth_disp);

                        getHistorySummary(item, dtoPass, dtoWait, dtoRej);
                      
                    }
                }
                else
                {
                    lstReceipt = new List<CustomerReceiptModel>();
                }


                model.lstData = lstReceipt;
                model.lstSummaryData = new List<CustRecHisSummaryModel>();
                model.lstSummaryData.Add(dtoPass);
                model.lstSummaryData.Add(dtoWait);
                model.lstSummaryData.Add(dtoRej);
                return model;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        private void getHistorySummary(CustomerReceiptModel data, CustRecHisSummaryModel dtoPass,
                    CustRecHisSummaryModel dtoWait, CustRecHisSummaryModel dtoRej)
        {
            try
            {
                if (data.custRec_status.Equals(Status.reject))
                {
                    dtoRej.totalQty = dtoRej.totalQty + Convert.ToInt32(data.custRec_totalGainPoint);
                }
                else
                {
                    if (data.custRec_status.Equals(Status.success))
                    {
                        dtoPass.totalQty = dtoPass.totalQty + Convert.ToInt32(data.custRec_totalGainPoint);
                    }
                    else
                    {
                        dtoWait.totalQty = dtoWait.totalQty + Convert.ToInt32(data.custRec_totalGainPoint);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
    }
}