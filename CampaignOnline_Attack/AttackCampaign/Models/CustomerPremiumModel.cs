﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttackCampaign.Models
{
    public class CustomerPremiumModel
    {
        public String custPrem_name { get; set; }
        public String custPrem_fullPath { get; set; }
        public String custPrem_textNameDisp { get; set; }

        public Guid custPrem_guid { get; set; }
        public Guid custPrem_custRecGuid { get; set; }
        public Guid custPrem_premGuid { get; set; }
        public String custPrem_status { get; set; }
        public String custPrem_remark { get; set; }
        public DateTime custPrem_createDate { get; set; }
        public String custPrem_createDate_str { get; set; }
        public Guid custPrem_createBy { get; set; }
        public DateTime custPrem_updateDate { get; set; }
        public String custPrem_updateDate_str { get; set; }
        public Guid custPrem_updateBy { get; set; }

        public CustomerReceiptModel customerReceiptModel {get;set;}

    }
}