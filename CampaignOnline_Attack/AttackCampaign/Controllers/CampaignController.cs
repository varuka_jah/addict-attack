﻿using AttackCampaign.ConstUtil;
using AttackCampaign.Controllers.Base;
using AttackCampaign.Data;
using AttackCampaign.Models;
using AttackCampaign.Models.Service;
using AttackCampaign.Models.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace AttackCampaign.Controllers
{
    public class CampaignController : BaseController
    {
        public ActionResult ChooseCampaign()
        {
            CampaignActionModel model = new CampaignActionModel();

            CampaignService serv = new CampaignService();
            CampaignSearchModel dtoCri = new CampaignSearchModel();
            dtoCri.camp_find_dt = DateTime.Today;
            model.lstSearchResult = serv.searchByCriteria(_db, dtoCri);

            if (model.lstSearchResult != null && model.lstSearchResult.Count() == 1)
            {
                return RedirectToAction("UploadReceipt", new RouteValueDictionary(
                    new { controller = "Campaign", action = "UploadReceipt", campGuid = model.lstSearchResult[0].camp_guid }));
            }
            else
            {
                model.lstSel_Campaign = ConvertUtil.generateDDL_Campaign(model.lstSearchResult);
                model.lstSearchResult = new List<CampaignSearchResultModel>();
                return View(model);
            }
        }

        public ActionResult UploadReceipt(Guid campGuid)
        {
            if (campGuid == null || campGuid.Equals(Guid.Empty))
            {
                return RedirectToAction("Login", "User");
            }

            CampaignUpload model = new CampaignUpload();
            model.chooseCampaign = campGuid;
            model.receiptNo_1 = "";
            model.receiptNo_2 = "";
            model.receiptNo_3 = "";
            model.receiptNo_4 = "";
            model.receiptNo_5 = "";
            model.branchNo_1 = "";
            model.branchNo_2 = "";
            model.branchNo_3 = "";
            model.branchNo_4 = "";
            model.branchNo_5 = "";
            model.receipt_img_1 = new FileModel.FileAttachModel();
            model.receipt_img_2 = new FileModel.FileAttachModel();
            model.receipt_img_3 = new FileModel.FileAttachModel();
            model.receipt_img_4 = new FileModel.FileAttachModel();
            model.receipt_img_5 = new FileModel.FileAttachModel();
            model.dtoData = new CampaignUse();

            CampaignService campServ = new CampaignService();
            model.dtoCamp = campServ.getCampaignByGuId(_db, model.chooseCampaign);
            model.dtoData.dtoCamp = model.dtoCamp;
            model.dtoData.chooseCampaign = model.chooseCampaign;
            return View(model);
        }

        public JsonResult CheckExistRecepitNo(String receiptNo_1, String receiptNo_2, String receiptNo_3, String receiptNo_4, String receiptNo_5, String branchNo_1, String branchNo_2, String branchNo_3, String branchNo_4, String branchNo_5)
        {
            try
            {
                CampaignService serv = new CampaignService();
                String message = "ขออภัยค่ะ ไม่สามารถทำรายการได้ เนื่องจากพบเลขที่ใบเสร็จนี้ในระบบ";
                bool isDuplicate = false;
                if (receiptNo_1 != null && !receiptNo_1.Trim().Equals(""))
                {
                    bool isExist = serv.checkExistReceiptNo(_db, receiptNo_1,  branchNo_1);
                    if (isExist)
                    {
                        isDuplicate = true;
                        message = message + " :: ใบที่ 1";
                    }
                }
                if (receiptNo_2 != null && !receiptNo_2.Trim().Equals(""))
                {
                    bool isExist = serv.checkExistReceiptNo(_db, receiptNo_2, branchNo_2);
                    if (isExist)
                    {
                        isDuplicate = true;
                        message = message + " :: ใบที่ 2";
                    }
                }
                if (receiptNo_3 != null && !receiptNo_3.Trim().Equals(""))
                {
                    bool isExist = serv.checkExistReceiptNo(_db, receiptNo_3, branchNo_3);
                    if (isExist)
                    {
                        isDuplicate = true;
                        message = message + " :: ใบที่ 3";
                    }
                }
                if (receiptNo_4 != null && !receiptNo_4.Trim().Equals(""))
                {
                    bool isExist = serv.checkExistReceiptNo(_db, receiptNo_4, branchNo_4);
                    if (isExist)
                    {
                        isDuplicate = true;
                        message = message + " :: ใบที่ 4";
                    }
                }
                if (receiptNo_5 != null && !receiptNo_5.Trim().Equals(""))
                {
                    bool isExist = serv.checkExistReceiptNo(_db, receiptNo_5, branchNo_5);
                    if (isExist)
                    {
                        isDuplicate = true;
                        message = message + " :: ใบที่ 5";
                    }
                }


                return Json(new JSonResultService() { status = 200, message = message, dataResult = isDuplicate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UploadFile(String imageData)
        {
            try
            {
                if (imageData == null || imageData.Equals("null"))
                {
                    return Json(new JSonResultService() { status = 200, message = "", dataResult = "", dataResult_2 = "" }, JsonRequestBehavior.DenyGet);
                } else
                {
                    ConstUtil.FileUtil fUtil;
                    String newFileName = "";
                    fUtil = new ConstUtil.FileUtil();
                    newFileName = fUtil.saveImageDataToServer(ConstUtil.ConstValue.UploadPath.receipt, imageData);

                    return Json(new JSonResultService() { status = 200, message = "", dataResult = newFileName, dataResult_2 = "" }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult SaveReceipt(CampaignUse model)
        {
            try
            {
                CampaignService serv = new CampaignService();
                Guid custGrGuid = serv.insertReceipt(_db, model, _userInfo);

                return Json(new JSonResultService() { status = 200, message = Url.Action("TermAndCondition", "Campaign", new { custGrGuid = custGrGuid }), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult TermAndCondition(Guid custGrGuid)
        {
            if (custGrGuid == null || custGrGuid.Equals(Guid.Empty))
            {
                return RedirectToAction("Login", "User");
            }

            CampaignUpload model = new CampaignUpload();
            model.customerGrGuid = custGrGuid;
            model.isCheckTerm = false;
            model.ko_isEnableNext = false;

            return View(model);
        }
       


        public ActionResult Confirm(Guid custGrGuid)
        {
            if (custGrGuid == null || custGrGuid.Equals(Guid.Empty))
            {
                return RedirectToAction("Login", "User");
            }
            
            CampaignUpload model = new CampaignUpload();
            model.customerGrGuid = custGrGuid;

            return View(model);
        }

        public JsonResult ConfirmCondition(Guid custGrGuid)
        {
            try
            {
                CampaignService serv = new CampaignService();
                Guid custGuid = serv.updateStatusCustGr(_db, custGrGuid, ConstValue.Status.init, ConstValue.Status.pending, _userInfo);
                return Json(new JSonResultService() { status = 200, message = Url.Action("History", "Campaign", new { custGuid = custGuid }), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SampleSlip()
        {
            return View();
        }

        public ActionResult CustomerAddress(Guid custGuid)
        {
            if (custGuid == null || custGuid.Equals(Guid.Empty))
            {
                return RedirectToAction("Login", "User");
            }

            CustomerModel model = new CustomerModel();
            CustomerService custServ = new CustomerService();
            model = custServ.getCustomerModelInfoByID(_db, custGuid);

            model.lstProvince = custServ.getProvince(_db);
            model.lstSel_Province = ConvertUtil.generateDDL_Province(model.lstProvince);

            model.lstDistrict = new List<AddrDistrictModel>();
            model.lstSel_District = new List<SelectListItem>();

            model.lstSubDistrict = new List<AddrDistrictModel>();
            model.lstSel_SubDistrict = new List<SelectListItem>();


            return View(model);
        }

        public JsonResult UpdateCustomerAddress(M_Customer model)
        {
            try
            {
                CustomerService serv = new CustomerService();
                serv.updateCustomerAddress(_db, model);

                LoginModel lgModel = new LoginModel();
                lgModel.tel = model.cust_tel.Trim();
                UserDetailSession _userInfo = serv.getLogin(lgModel);
                Session.Add("UserInfoSession", _userInfo);

                return Json(new JSonResultService() { status = 200, message = "", dataResult = null }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult History()
        {
            //  CustomerReceiptHistoryModel model = new CustomerReceiptHistoryModel();
            CampaignService serv = new CampaignService();
            CustomerReceiptHistoryModel model = serv.getCustomerReceiptHistory(_db, _userInfo.loginUserDetail.cust_guid);
            model.dtoData = new CustomerReceiptModel();
            return View(model);
        }

    }
}