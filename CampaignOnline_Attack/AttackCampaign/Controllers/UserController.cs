﻿using AttackCampaign.ConstUtil;
using AttackCampaign.Models;
using AttackCampaign.Models.Service;
using AttackCampaign.Models.Session;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AttackCampaign.Controllers
{
    public class UserController : Controller
    {
     
        [AllowAnonymous]
        public ActionResult Login()
        {
            LoginActionModel model = new LoginActionModel();
            model.dtoLogin = new LoginModel();
            model.dtoReg = new RegisterModel();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult Login(LoginModel model, string returnUrl)
        {
            try
            {
                CustomerService serv = new CustomerService();
                UserDetailSession _userInfo = serv.getLogin(model);
                Session.Add("UserInfoSession", _userInfo);

                setOWIN(_userInfo);
                return Json(new JSonResultService() { status = 200, message = "", dataResult = null }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult Register(RegisterModel model)
        {
            try
            {
                CustomerService serv = new CustomerService();
                String custTel = serv.register(model);

                LoginModel loginModel = new LoginModel();
                loginModel.tel = custTel;
                UserDetailSession _userInfo = serv.getLogin(loginModel);
                Session.Add("UserInfoSession", _userInfo);

                setOWIN(_userInfo);
                return Json(new JSonResultService() { status = 200, message = "", dataResult = null }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JSonResultService() { status = 500, message = ex.Message.ToString(), dataResult = null }, JsonRequestBehavior.AllowGet);
            }
        }

        public void setOWIN(UserDetailSession userInfo)
        {
            try
            {
                var identity = new ClaimsIdentity(new[] {
                        new Claim(ClaimTypes.NameIdentifier.ToString(),  userInfo.loginUserDetail.cust_guid.ToString()),
                        new Claim(ClaimTypes.Name, userInfo.loginUserDetail.cust_firstName)
                    }, DefaultAuthenticationTypes.ApplicationCookie);
       
                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;
                authManager.SignIn(identity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }


        public JsonResult getDistrictByProvince(String provId)
        {
            CustomerService serv = new CustomerService();

            if (provId == null || provId.Equals(""))
            {
                return Json(new JSonResultService() { status = 200, message = "", dataResult = new List<SelectListItem>(), dataResult_2 = new List<AddrDistrictModel>() }, JsonRequestBehavior.DenyGet);
            }
            else
            {
                List<AddrDistrictModel> lstData = serv.getDistrictByProv(Convert.ToInt64(provId));
                List<SelectListItem> lstSel = ConvertUtil.generateDDL_District(lstData);
                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstSel, dataResult_2 = lstData }, JsonRequestBehavior.DenyGet);
            }
        }

        public JsonResult getSubDistrictByDistrict(String districtId)
        {
            CustomerService serv = new CustomerService();

            if (districtId == null || districtId.Equals(""))
            {
                return Json(new JSonResultService() { status = 200, message = "", dataResult = new List<SelectListItem>(), dataResult_2 = new List<AddrSubDistrictModel>() }, JsonRequestBehavior.DenyGet);
            }
            else
            {
                List<AddrSubDistrictModel> lstData = serv.getSubDistrictByDistrict(Convert.ToInt64(districtId));
                List<SelectListItem> lstSel = ConvertUtil.generateDDL_SubDistrict(lstData);
                return Json(new JSonResultService() { status = 200, message = "", dataResult = lstSel, dataResult_2 = lstData }, JsonRequestBehavior.DenyGet);
            }
        }

        public ActionResult LogOff()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;
            authManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Abandon();

            Request.GetOwinContext().Authentication.SignOut();
            var cookieName = FormsAuthentication.FormsCookieName;
            FormsAuthentication.SignOut();
            if (Request.Cookies[cookieName] != null)
            {
                var authCookie = new HttpCookie(cookieName);
                authCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(authCookie);
            }
            //    logger.InfoFormat("User '{0}' logout completed.", User.Identity.Name);
            return RedirectToAction("Login", "User");
        }
    }
}