﻿using AttackCampaign.Data;
using AttackCampaign.Models;
using AttackCampaign.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AttackCampaign.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            CampaignService serv = new CampaignService();
            CampaignSearchModel dtoCri = new CampaignSearchModel();

            Entities db = new Entities();

            CampaignActionModel model = new CampaignActionModel();
            dtoCri.camp_find_dt = DateTime.Today;
       //     model.lstSearchResult = serv.searchByCriteria(db, dtoCri);

            return View(model);
        }

        public ActionResult TermAndCondition()
        {
            return View();
        }
    }
}