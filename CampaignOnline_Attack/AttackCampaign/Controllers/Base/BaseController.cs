﻿using AttackCampaign.Data;
using AttackCampaign.Models;
using AttackCampaign.Models.Service;
using AttackCampaign.Models.Session;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace AttackCampaign.Controllers.Base
{
    public class BaseController : Controller
    {
        public UserDetailSession CurrentUserInfoSession
        {
            get
            {
                UserDetailSession userInfoSession = (UserDetailSession)this.Session["UserInfoSession"];
                return userInfoSession;
            }
        }

        protected Entities _db = new Entities();
        protected UserDetailSession _userInfo;
        protected MainConfigModel _mainConig;


        protected override bool DisableAsyncSupport
        {
            get { return true; }
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);


        }

        protected override void ExecuteCore()
        {
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                UserDetailSession userInfoSession = (UserDetailSession)this.Session["UserInfoSession"];
                Guid custGuid = Guid.Empty;
                bool isExist = false;

                ClaimsPrincipal icp = Thread.CurrentPrincipal as ClaimsPrincipal;

                ClaimsIdentity claimsIdentity = (ClaimsIdentity)icp.Identity;
                bool isAuthenticated = icp.Identity.IsAuthenticated;

                if (userInfoSession != null)
                {
                    custGuid = userInfoSession.loginUserDetail.cust_guid;
                    isExist = true;
                }
                else
                {
                    logoff();
                }

                if (isExist)
                {
                    CustomerService serv = new CustomerService();
                    M_Customer prof = serv.getCustomerInfoByID(custGuid);

                    MainConfigService mcServ = new MainConfigService();
                    _mainConig = mcServ.getMainConfig(_db);

                    if (prof != null)
                    {
                        userInfoSession = new UserDetailSession(prof);

                        _userInfo = userInfoSession;
                        Session.Add("UserInfoSession", userInfoSession);

                        setOWIN(userInfoSession);
                    }
                    else
                    {
                        logoff();
                    }


                }
                base.ExecuteCore();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message.ToString());
            }
        }

        public JsonResult SerializeControl(string controlPath, object model)
        {
            ViewPage page = new ViewPage();
            ViewUserControl ctl = (ViewUserControl)page.LoadControl(controlPath);

            page.Controls.Add(ctl);
            page.ViewData.Model = model;
            page.ViewContext = new ViewContext();
            System.IO.StringWriter writer = new System.IO.StringWriter();
            System.Web.HttpContext.Current.Server.Execute(page, writer, false);
            string outputToReturn = writer.ToString();
            writer.Close();
            return this.Json(outputToReturn.Trim());
        }

        /// <summary>
        /// Called when an unhandled exception occurs in the action.
        /// </summary>
        /// <param name="filterContext">Information about the current request and action.</param>
        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception is UnauthorizedAccessException)
            {
                //
                // Manage the Unauthorized Access exceptions
                // by redirecting the user to Home page.
                //
                filterContext.ExceptionHandled = true;
                filterContext.Result = RedirectToAction("Login", "User");
            }
            //
            base.OnException(filterContext);
        }

        public void logoff()
        {
            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            authenticationManager.SignOut();
            FormsAuthentication.SignOut();
            HttpContext.Session.Abandon();
            //WebSecurity.Logout();
            ViewBag.ReturnUrl = "";
            FormsAuthentication.RedirectToLoginPage();
        }

        public void setOWIN(UserDetailSession userInfo)
        {
            try
            {
                var identity = new ClaimsIdentity(new[] {
                        new Claim(ClaimTypes.NameIdentifier.ToString(),  userInfo.loginUserDetail.cust_guid.ToString()),
                        new Claim(ClaimTypes.Name, userInfo.loginUserDetail.cust_firstName)
                    }, DefaultAuthenticationTypes.ApplicationCookie);

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;
                authManager.SignIn(identity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        #region rendor
        protected string RenderPartialViewToString()
        {
            return RenderPartialViewToString(null, null);
        }

        protected string RenderPartialViewToString(string viewName)
        {
            return RenderPartialViewToString(viewName, null);
        }

        protected string RenderPartialViewToString(object model)
        {
            return RenderPartialViewToString(null, model);
        }

        protected string RenderPartialViewToString(string viewName, object model = null)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            }

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        protected string RenderViewToString(string viewName, object model = null)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            }

            var viewData = new ViewDataDictionary(model);

            using (StringWriter sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindView(ControllerContext, viewName, null);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, viewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        protected string RenderViewToString<T>(string viewName, T model)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            }

            var viewData = new ViewDataDictionary<T>(model);

            using (StringWriter sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindView(ControllerContext, viewName, null);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, viewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }


        #endregion


    }
}