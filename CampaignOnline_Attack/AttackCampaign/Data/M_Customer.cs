//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AttackCampaign.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_Customer
    {
        public System.Guid cust_guid { get; set; }
        public string cust_firstName { get; set; }
        public string cust_lastName { get; set; }
        public string cust_email { get; set; }
        public string cust_tel { get; set; }
        public string cust_password { get; set; }
        public string cust_addr1 { get; set; }
        public string cust_addr2 { get; set; }
        public long cust_provId { get; set; }
        public long cust_distId { get; set; }
        public long cust_subId { get; set; }
        public string cust_status { get; set; }
        public string cust_remark { get; set; }
        public System.DateTime cust_createDate { get; set; }
        public System.Guid cust_createBy { get; set; }
        public System.DateTime cust_updateDate { get; set; }
        public System.Guid cust_updateBy { get; set; }
    }
}
