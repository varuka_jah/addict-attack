﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AttackCampaign.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Entities : DbContext
    {
        public Entities()
            : base("name=Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<M_District> M_District { get; set; }
        public virtual DbSet<M_MainConfig> M_MainConfig { get; set; }
        public virtual DbSet<M_Province> M_Province { get; set; }
        public virtual DbSet<M_Role> M_Role { get; set; }
        public virtual DbSet<M_User> M_User { get; set; }
        public virtual DbSet<M_Customer> M_Customer { get; set; }
        public virtual DbSet<T_Campaign> T_Campaign { get; set; }
        public virtual DbSet<M_SubDistrict> M_SubDistrict { get; set; }
        public virtual DbSet<T_CustomerRec> T_CustomerRec { get; set; }
        public virtual DbSet<T_CustomerReceiptGroup> T_CustomerReceiptGroup { get; set; }
    }
}
