﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttackCampaign.ConstUtil
{
    public class ConstValue
    {
        public static class Status
        {
            public static String success_disp = "ผ่านการอนุมัติ";
            public static String init_disp = "รอการตรวจสอบ";
            public static String reserv_disp = "รอการตรวจสอบ";
            public static String act_disp = "รอการตรวจสอบ";
            public static String oth_disp = "อื่นๆ";
            public static String rej_disp = "ไม่ผ่านการอนุมัติ";


            public static String success = "SUC";
            public static String init = "INIT";
            public static String active = "ACT";
            public static String inactive = "INA";
            public static String pending = "PND";
            public static String reserved = "RESV";
            public static String reject = "REJ";

            //public static String init_disp = "Initial";
            //public static String active_disp = "Active";
            //public static String inactive_disp = "Inactive";
            //public static String pending_disp = "Pending";
            //public static String reserved_disp = "Reserved";

            public static String gameResult_win = "win";
            public static String gameResult_win_disp = "ชนะ";
            public static String gameResult_loss = "loss";
            public static String gameResult_loss_disp = "แพ้";
            public static String gameResult_none = "none";
            public static String gameResult_none_disp = "none";
        }
        public static class MainConfig
        {
            public static String isUseTel = "FE01";
            public static String campaignGameRatio = "CG01";
            public static String urlGameWin = "FE02";
            public static String urlGameLoss = "FE03";
        }

        public static class Flag
        {
            public static String yes = "Y";
            public static String no = "N";
        }

        public static class UploadPath
        {
            public static String product = "http://info.duckthailandcampaign.com/FileUpload/Product/";
            public static String premium = "http://info.duckthailandcampaign.com/FileUpload/Premium/";
            public static String store = "http://info.duckthailandcampaign.com/FileUpload/Store/";
            public static String campaign = "http://info.duckthailandcampaign.com/FileUpload/Campaign/";
            public static String receipt = "/FileUploads/Receipt/";

        }
    }
}