﻿using AttackCampaign.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AttackCampaign.ConstUtil
{
    public static class ConvertUtil
    {
        public static List<SelectListItem> generateDDL_Campaign(List<CampaignSearchResultModel> lstData)
        {
            List<SelectListItem> result = new List<SelectListItem>();

            for (int i = 0; lstData != null && i < lstData.Count(); i++)
            {
                result.Add(new SelectListItem() { Text = lstData[i].camp_name, Value = lstData[i].camp_guid.ToString() });
            }
            return result;
        }

        public static List<SelectListItem> generateDDL_Province(List<AddrProviceModel> lstData)
        {
            List<SelectListItem> result = new List<SelectListItem>();

            for (int i = 0; lstData != null && i < lstData.Count(); i++)
            {
                result.Add(new SelectListItem() { Text = lstData[i].provinceNameTh, Value = lstData[i].provinceId.ToString() });
            }
            return result;
        }

        public static List<SelectListItem> generateDDL_SubDistrict(List<AddrSubDistrictModel> lstData)
        {
            List<SelectListItem> result = new List<SelectListItem>();

            for (int i = 0; lstData != null && i < lstData.Count(); i++)
            {
                result.Add(new SelectListItem() { Text = lstData[i].subDistNameTh, Value = lstData[i].subDistId.ToString() });
            }
            return result;
        }

        public static List<SelectListItem> generateDDL_District(List<AddrDistrictModel> lstData)
        {
            List<SelectListItem> result = new List<SelectListItem>();

            for (int i = 0; lstData != null && i < lstData.Count(); i++)
            {
                result.Add(new SelectListItem() { Text = lstData[i].distNameTh, Value = lstData[i].distId.ToString() });
            }
            return result;
        }


        private static byte[] GetPassword(string inputString)
        {
            //HashAlgorithm algorithm = MD5.Create(); 
            HashAlgorithm algorithm = SHA1.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        private static string GetEncryptPassword(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetPassword(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public static string GetMyEncryptPassword(string inputString)
        {
            string hash = GetEncryptPassword(inputString + ",ADDICT2019MKT!");
            // SHA MAX Lenght is 40
            string modHash = hash.Substring(15, 10);
            Console.WriteLine("hash = {0}\nmod = {1}", hash, modHash);

            return modHash;
        }

        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }

    }
}