﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttackCampaign.ConstUtil
{
    public static class MsgConst
    {
        public static String resetPassNotFound = "Password ไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง";

        public static String userNotFound = "ไม่พบเบอร์โทรศัพท์ที่ระบุ กรุณาลงทะเบียนก่อนค่ะ";
        public static String notFoundRecord = "ไม่พบรายการที่ต้องการแก้ไขค่ะ";
        public static String existTelNo = "พบเบอร์โทรศัพท์นี้แล้วในระบบค่ะ สามารถทำการ Login ด้วยเบอร์โทรศัพท์ที่ระบุ";
    }
}